/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.talkingstatues.data.prefs;

import com.talkingstatues.model.city.CityDetails;
import com.talkingstatues.model.login.LoginDetails;
import com.talkingstatues.model.statueList.StatueDetails;
import com.talkingstatues.model.statueList.StatueMaster;

/**
 * Created by Paras Andani on 19/09/2018.
 */

public interface PreferencesHelper {

    String getDeviceToken();
    void setDeviceToken(String deviceToken);

    LoginDetails getLoginModel();
    void setLoginDetails(String loginDetails);

    boolean getIsStarted();
    void setIsStarted(boolean isStarted);

    String getProgramId();
    void setProgramId(String programId);

    String getCurrentLatitude();
    void setCurrentLatitude(String latitude);

    String getCurrentLongitude();
    void setCurrentLongitude(String longitude);

    String getLanguageCode();
    void setLanguageCode(String languageCode);

    String getAuthToken();
    void setAuthToken(String authToken);

    boolean getIsFirstTimeLaunch();
    void setIsFirstTimeLaunch(boolean isFirstTimeLaunch);

    void setActivityRunning(boolean b);
    boolean getIsAcitivtyRunning();

    void setStatueJson(String toJson);
    StatueMaster getStatueModel();

    void setDefaultCity(String toJson);
    String getStoredCity();
}
