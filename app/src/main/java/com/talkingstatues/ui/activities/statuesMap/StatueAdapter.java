package com.talkingstatues.ui.activities.statuesMap;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.talkingstatues.R;
import com.talkingstatues.databinding.LayoutPlacesItemsBinding;
import com.talkingstatues.databinding.LayoutStatueListBinding;
import com.talkingstatues.model.statueList.StatueDetails;
import com.talkingstatues.networking.APIUrl;
import com.talkingstatues.ui.base.BaseViewHolder;
import com.talkingstatues.utilities.AppConstants;

import java.util.ArrayList;
import java.util.List;

public class StatueAdapter extends RecyclerView.Adapter<BaseViewHolder> {


    private List<StatueDetails> mList = new ArrayList<>();
    private LayoutInflater layoutInflater;
    OnItemClickListener onItemClickListener;
    Context context;
    private String languageCode;




    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
            context = parent.getContext();
        }
        LayoutStatueListBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.layout_statue_list, parent, false);
        return new ViewHolder(binding);

    }


    @Override
    public int getItemCount() {
        return mList.size();

    }

    public void addItems(List<StatueDetails> mList) {
        this.mList.addAll(mList);
        notifyDataSetChanged();
    }

    public void clearList() {
        if (this.mList != null && this.mList.size() > 0) {
            this.mList.clear();
        }
    }

    public void setCallback(OnItemClickListener clickListener) {
        this.onItemClickListener = clickListener;
    }

    public void setItem(int listIndex, StatueDetails statueDetails) {
        mList.set(listIndex, statueDetails);
        notifyItemChanged(listIndex);
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public class ViewHolder extends BaseViewHolder {
        LayoutStatueListBinding binding;

        public ViewHolder(View itemView) {
            super(itemView);
        }

        public ViewHolder(final LayoutStatueListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        @Override
        protected void clear() {

        }

        public void onBind(final int position) {
            super.onBind(position);
            StatueDetails statueDetails = mList.get(position);
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.drawable.ic_place_holder);
            requestOptions.error(R.drawable.ic_place_holder);
            Glide.with(binding.getRoot().getContext())
                    .setDefaultRequestOptions(requestOptions)
                    .load(APIUrl.IMAGE_URL + statueDetails.getImage())
                    .into(binding.ivBanner);
            if (languageCode.equalsIgnoreCase(AppConstants.LANGUAGE_SPANISH)) {
                if (statueDetails.getSpanishStatueName() != null) {
                    binding.staueName.setText(statueDetails.getSpanishStatueName());
                } else {
                    binding.staueName.setText(statueDetails.getStatueName());
                }
            } else {
                binding.staueName.setText(statueDetails.getStatueName());
            }
            if (statueDetails.getInZone() == null) {
                binding.linMain.setBackgroundResource(R.color.transparent);
            } else if (statueDetails.getInZone().equals("yes")) {
                binding.linMain.setBackgroundResource(R.drawable.bg_green_square);
            } else if (statueDetails.getInZone().equals("no")) {
                binding.linMain.setBackgroundResource(R.drawable.bg_red_square);
            } else {
                binding.linMain.setBackgroundResource(R.drawable.bg_white_rectangle);
            }

            binding.getRoot().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.onChatClick(mList.get(position));
                }
            });


        }
    }


    public interface OnItemClickListener {
        void onChatClick(StatueDetails statueDetails);
    }
}
