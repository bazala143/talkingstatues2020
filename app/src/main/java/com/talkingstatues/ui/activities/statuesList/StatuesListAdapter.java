package com.talkingstatues.ui.activities.statuesList;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.talkingstatues.R;
import com.talkingstatues.databinding.LayoutStatuesListBinding;
import com.talkingstatues.model.statueList.StatueDetails;
import com.talkingstatues.networking.APIUrl;
import com.talkingstatues.ui.activities.statueDetails.StatueDetailsActivity;
import com.talkingstatues.ui.activities.statuesMap.StatuesMapActivity;
import com.talkingstatues.ui.base.BaseViewHolder;
import com.talkingstatues.utilities.AppConstants;

import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

public class StatuesListAdapter extends RecyclerView.Adapter<BaseViewHolder> implements View.OnClickListener, Filterable {
    private List<StatueDetails> mList;
    private List<StatueDetails> filteredStatueList;
    private LayoutInflater layoutInflater;
    private Activity activity;
    String languageCode = AppConstants.LANGUAGE_ENGLISH;
    OnItemClickCallBack onItemClickCallBack;
    String country = "";

    public StatuesListAdapter(Activity activity, List<StatueDetails> mList) {
        this.activity = activity;
        this.mList = mList;
        this.filteredStatueList = mList;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        LayoutStatuesListBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.layout_statues_list, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public int getItemCount() {
        return filteredStatueList.size();
    }

    @Override
    public void onClick(View v) {

    }

    public void clear() {
        filteredStatueList.clear();
        notifyDataSetChanged();
    }

    public class ViewHolder extends BaseViewHolder {
        LayoutStatuesListBinding binding;

        public ViewHolder(View itemView) {
            super(itemView);
        }

        public ViewHolder(final LayoutStatuesListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        @Override
        protected void clear() {

        }

        public void onBind(final int position) {
            super.onBind(position);
            final StatueDetails statuesDetails = filteredStatueList.get(position);
            binding.setData(statuesDetails);
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.drawable.ic_place_holder);
           /* startAnimation(binding.cardInfo);
            startAnimation(binding.cardMap);*/
            if (languageCode.equalsIgnoreCase(AppConstants.LANGUAGE_SPANISH)) {
                if (statuesDetails.getSpanishStatueName() != null) {
                    binding.tvStatuesName.setText(statuesDetails.getSpanishStatueName());
                } else {
                    binding.tvStatuesName.setText(statuesDetails.getStatueName());
                }
            } else {
                binding.tvStatuesName.setText(statuesDetails.getStatueName());
            }
            Glide.with(binding.getRoot().getContext())
                    .applyDefaultRequestOptions(requestOptions)
                    .load(APIUrl.IMAGE_URL + statuesDetails.getImage())
                    .into(binding.ivImage);

            if (statuesDetails.getIsVisited()) {
                Glide.with(binding.getRoot().getContext())
                        .applyDefaultRequestOptions(requestOptions)
                        .load(R.drawable.ic_tick)
                        .into(binding.ivImageDistance);
            } else {
                Glide.with(binding.getRoot().getContext())
                        .applyDefaultRequestOptions(requestOptions)
                        .load(R.drawable.ic_gps)
                        .into(binding.ivImageDistance);
            }

            binding.ivImageDistance.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickCallBack.onMapCallBack(filteredStatueList.get(position), position);
                }
            });

            binding.ivImageInfo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickCallBack.onInfoCallBack(statuesDetails, position);
                }
            });
            if (statuesDetails.getDistanceElement() != null) {
                if (statuesDetails.getDistanceElement().getDistance() != null) {
                    if (statuesDetails.getDistanceElement().getDistance().getValue() != null) {
                        if (country != null && country.equals("Usa")) {
                            binding.tvDistanceName.setText(String.valueOf(statuesDetails.getDistanceElement().getDistance().getValue() * 0.000621371192).substring(0, 4) + " Miles");
                        } else {
                            binding.tvDistanceName.setText(statuesDetails.getDistanceElement().getDistance().getText());
                        }
                    }
                    ;
                }
            }


        }


    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    filteredStatueList = mList;
                } else {
                    List<StatueDetails> filteredList = new ArrayList<>();
                    for (StatueDetails row : mList) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        //   Log.e("CCCCC",row.getSpanishStatueName()+" =>"+charString.toLowerCase());
                        if (row.getStatueName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        } else if (row.getSpanishStatueName() != null && (row.getSpanishStatueName().toLowerCase().contains(charString.toLowerCase()))) {
                            filteredList.add(row);
                        } else if (row.getKeywords() != null && row.getKeywords().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        } else if (row.getSpanishKeywords() != null && row.getSpanishKeywords().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    filteredStatueList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredStatueList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filteredStatueList = (ArrayList<StatueDetails>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public interface OnItemClickCallBack {
        void onInfoCallBack(StatueDetails statueDetails, int position);

        void onMapCallBack(StatueDetails statueDetails, int position);

    }

    public void setonItemClickListnere(OnItemClickCallBack onItemClickCallBack) {
        this.onItemClickCallBack = onItemClickCallBack;
    }

    public void addAll(List<StatueDetails> list) {
        Log.e("SSSSS", list.size() + "");
        mList.addAll(list);
        notifyDataSetChanged();
    }

    public void clearAll() {
        mList.clear();
        notifyDataSetChanged();
    }


}