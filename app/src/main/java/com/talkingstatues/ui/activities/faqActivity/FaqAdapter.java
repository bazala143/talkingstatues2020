package com.talkingstatues.ui.activities.faqActivity;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.talkingstatues.R;
import com.talkingstatues.databinding.LayoutFaqBinding;

import com.talkingstatues.model.faqList.FaqDetails;
import com.talkingstatues.ui.base.BaseViewHolder;
import com.talkingstatues.utilities.AppConstants;

import java.util.List;

public class FaqAdapter extends RecyclerView.Adapter<BaseViewHolder> implements View.OnClickListener {
    private List<FaqDetails> mList;
    LayoutInflater layoutInflater;
    Activity activity;
    String languageCode = AppConstants.LANGUAGE_ENGLISH;

    public FaqAdapter(Activity activity, List<FaqDetails> mList) {
        this.activity = activity;
        this.mList = mList;
    }

    public void setLangugeCode(String languageCode) {
        this.languageCode = languageCode;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        LayoutFaqBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.layout_faq, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public void onClick(View v) {

    }

    public void clear() {
        mList.clear();
        notifyDataSetChanged();
    }

    public class ViewHolder extends BaseViewHolder {
        LayoutFaqBinding binding;

        public ViewHolder(View itemView) {
            super(itemView);
        }

        public ViewHolder(final LayoutFaqBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        @Override
        protected void clear() {

        }

        public void onBind(final int position) {
            super.onBind(position);
            final FaqDetails statuesDetails = mList.get(position);
            if (languageCode.equalsIgnoreCase(AppConstants.LANGUAGE_SPANISH)) {
                if (statuesDetails.getSpanishQuestion() != null && statuesDetails.getSpanishAnswer() != null) {
                    binding.tvQuestion.setText(statuesDetails.getSpanishQuestion());
                    binding.tvAnswer.setText(statuesDetails.getSpanishAnswer());
                } else {
                    binding.tvQuestion.setText(statuesDetails.getQuestion());
                    binding.tvAnswer.setText(statuesDetails.getAnswer());
                }

            } else {
                binding.tvQuestion.setText(statuesDetails.getQuestion());
                binding.tvAnswer.setText(statuesDetails.getAnswer());
            }


        }


    }


}