package com.talkingstatues.ui.activities.rewardsActivity;

import com.talkingstatues.di.PerActivity;
import com.talkingstatues.model.login.LoginDetails;
import com.talkingstatues.ui.base.MvpPresenter;

@PerActivity
public interface RewardsMvpPresenter<V extends RewardsMvpView> extends MvpPresenter<V> {


    LoginDetails getLoginDetails();
    void  redeemReward(String cityId);

    void  getStatueList(String cityId);
}
