package com.talkingstatues.ui.activities.main;

import android.app.Activity;
import android.widget.EditText;

import com.google.gson.JsonObject;
import com.harmis.imagepicker.CameraUtils.CameraIntentHelper;
import com.talkingstatues.di.PerActivity;
import com.talkingstatues.model.DrawerItem;
import com.talkingstatues.model.city.CityDetails;
import com.talkingstatues.model.login.LoginDetails;
import com.talkingstatues.model.statueList.StatueDetails;
import com.talkingstatues.ui.base.MvpPresenter;

import java.util.List;

@PerActivity
public interface MainMvpPresenter<V extends MainMvpView> extends MvpPresenter<V> {
    boolean isValid(EditText edUsername, EditText edPassword);

    void storeLocation(String latitude, String longitude);
    void  setDefaultCity(CityDetails cityDetails);
    LoginDetails getLoginDetails();
    List<DrawerItem>  getDrawerList(Activity activity);
    CameraIntentHelper getmCameraIntentHelper(int whichImage, Activity editProfileActivity);
    void  redirectToPlayStore(Activity activity);
    void  shareApp(Activity activity);
    void  getCityList();
    void  setSelfieApi(String image, int userId);
    void  getSponserLogos(String cityId,Activity activity);
    void  getAllStatueList();
    void  visitStatueApi(StatueDetails statueDetails);
    void setLanguageCode(String language);
    String getLanguageCode();

    void  getStatueDetailsApi(String qrCode);
    String  getDefaultCity(double cLat,double cLong,Activity activity);
    void getStatueDetailsFromPrefs(String qrCode);
    void setLoginDetails(String s);
}
