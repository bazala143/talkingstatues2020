package com.talkingstatues.ui.activities.main;

import com.talkingstatues.model.city.CityDetails;
import com.talkingstatues.model.logoDetails.LogoDetails;
import com.talkingstatues.model.statueList.StatueDetails;
import com.talkingstatues.ui.base.MvpView;

import java.util.List;

public interface MainMvpView extends MvpView {

    void  onCityListLoaded(List<CityDetails>  cityList);

    void onLogosLoaded(List<LogoDetails> json);


    void  onStatueVisited(StatueDetails statueDetails);

    void shareSelfie(String mImage);

    void onDefaultCityFound(String city);

    void onDefaualtCityNotFound();

    void openStatueLInk(String qrCode);
}
