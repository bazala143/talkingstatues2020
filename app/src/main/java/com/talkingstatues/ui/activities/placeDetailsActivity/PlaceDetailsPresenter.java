package com.talkingstatues.ui.activities.placeDetailsActivity;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.EditText;

import androidx.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.talkingstatues.R;
import com.talkingstatues.data.prefs.AppPreferencesHelper;
import com.talkingstatues.model.distance.DistanceDetails;
import com.talkingstatues.model.distance.DistanceDuration;
import com.talkingstatues.model.distance.DistanceElement;
import com.talkingstatues.model.distance.DistanceRow;
import com.talkingstatues.model.login.LoginDetails;
import com.talkingstatues.model.login.LoginMaster;
import com.talkingstatues.model.placeDetails.PlaceInfoMaster;
import com.talkingstatues.model.statueList.StatueDetails;
import com.talkingstatues.model.statueList.StatueMaster;
import com.talkingstatues.networking.APIUrl;
import com.talkingstatues.networking.API_Params;
import com.talkingstatues.networking.NetworkError;
import com.talkingstatues.networking.NetworkService;
import com.talkingstatues.ui.activities.login.LoginMvpPresenter;
import com.talkingstatues.ui.activities.login.LoginMvpView;
import com.talkingstatues.ui.base.BasePresenter;
import com.talkingstatues.utilities.AppConstants;
import com.talkingstatues.utilities.CommonUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

public class PlaceDetailsPresenter<V extends PlaceDetailsMvpView> extends BasePresenter<V>
        implements PlaceDetailsMvpPresenter<V> {
    @Inject
    NetworkService service;
    private static final String TAG = PlaceDetailsPresenter.class.getSimpleName();
    GetStatyeDistanceAsync getStatyeDistanceAsync;


    @Inject
    public PlaceDetailsPresenter(AppPreferencesHelper preferencesHelper, CompositeSubscription mSubscription) {
        super(preferencesHelper, mSubscription);
    }


    @Override
    public void getPlaceInfoApi(String placeId) {
        if (getMvpView().isNetworkConnected()) {
            getMvpView().showLoading();
            Subscription subscription = service.getPlaceInfo(placeInfoParas(placeId), new NetworkService.GetPlaceInfoCallback() {
                @Override
                public void onSuccess(PlaceInfoMaster loginMaster) {
                    getMvpView().hideLoading();
                    if (loginMaster != null) {
                        CommonUtils.pLog(TAG, "Response : " + new Gson().toJson(loginMaster).toString());
                        if (loginMaster.getCode().equals("0")) {
                            getMvpView().onPlaceDetailsLoaded(loginMaster);
                         getStatyeDistanceAsync =   new GetStatyeDistanceAsync(loginMaster);
                         getStatyeDistanceAsync.execute();
                        } else {
                            getMvpView().showMessage(loginMaster.getMessage());
                        }

                    }
                }

                @Override
                public void onError(NetworkError networkError) {
                    getMvpView().hideLoading();
                    getMvpView().onError(networkError.getMessage() + "");
                }
            });
            getSubscription().add(subscription);
        } else {
            getMvpView().onError(R.string.utils__no_connection);
        }
    }

    @Override
    public String getLangugeCode() {
       return getPreferencesHelper().getLanguageCode();
    }


    private JsonObject placeInfoParas(String placeId) {
        JsonObject gsonObject = new JsonObject();
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(API_Params.place_id, placeId);
            JsonParser parser = new JsonParser();
            gsonObject = (JsonObject) parser.parse(jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("loginParams", gsonObject.toString());
        return gsonObject;
    }


    @Override
    public void onDetach() {
        if (getSubscription() != null && getSubscription().hasSubscriptions()) {
            getSubscription().clear();
        }
    }


    @SuppressLint("StaticFieldLeak")
    public class GetStatyeDistanceAsync extends AsyncTask<String, String, DistanceElement> {

        PlaceInfoMaster statueMaster;

        public GetStatyeDistanceAsync(PlaceInfoMaster statueMaster) {
            this.statueMaster = statueMaster;
        }

        final List<StatueDetails> sttaueList = new ArrayList<>();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected DistanceElement doInBackground(String... strings) {
                String latitude = statueMaster.getPlaceInfoDetails().getLatitude() + "";
                String longitude = statueMaster.getPlaceInfoDetails().getLongitude() + "";
                try {
                    DistanceElement distanceElement = null;
                    HttpURLConnection connection = null;
                    StringBuilder jsonResults = new StringBuilder();
                    try {
                        //String latitude = objects[0];
                        //String longitude = objects[1];
                        CommonUtils.pLog(TAG, getDistanceUrl(latitude, longitude));
                        URL url = new URL(getDistanceUrl(latitude, longitude));
                        connection = (HttpURLConnection) url.openConnection();

                        CommonUtils.pLog(TAG, "Response code : " + connection.getResponseCode());
                        InputStreamReader in = new InputStreamReader(connection.getInputStream());

                        // Load the results into a StringBuilder
                        int read;
                        char[] buff = new char[1024];
                        while ((read = in.read(buff)) != -1) {
                            jsonResults.append(buff, 0, read);
                        }

                        try {
                            JSONObject jsonMain = new JSONObject(jsonResults.toString());
                            CommonUtils.pLog(TAG, "json : " + jsonMain.toString());
                            String status = jsonMain.optString(API_Params.status);
                            if (!CommonUtils.isEmpty(status) && CommonUtils.equals(status, AppConstants.STATUS_OK)) {
                                List<String> destinationAddresses = new ArrayList<>();
                                JSONArray destinationAddressArray = jsonMain.getJSONArray(API_Params.destination_addresses);
                                if (destinationAddressArray != null && destinationAddressArray.length() > 0) {
                                    destinationAddresses.add(destinationAddressArray.getString(0));
                                }

                                List<String> originAddresses = new ArrayList<>();
                                JSONArray originAddressArray = jsonMain.getJSONArray(API_Params.origin_addresses);
                                if (originAddressArray != null && originAddressArray.length() > 0) {
                                    originAddresses.add(originAddressArray.getString(0));
                                }

                                List<DistanceRow> distanceRows = new ArrayList<>();
                                JSONArray rowsArray = jsonMain.getJSONArray(API_Params.rows);
                                if (rowsArray != null && rowsArray.length() > 0) {
                                    List<DistanceElement> distanceElements = new ArrayList<>();
                                    JSONObject jsonObject = rowsArray.getJSONObject(0);
                                    JSONArray elementsArray = jsonObject.getJSONArray(API_Params.elements);
                                    if (elementsArray != null && elementsArray.length() > 0) {
                                        JSONObject elementObject = elementsArray.getJSONObject(0);
                                        if (elementObject != null && CommonUtils.equals(elementObject.optString(API_Params.status), AppConstants.STATUS_OK)) {
                                            distanceElement = new DistanceElement();
                                            JSONObject distanceObject = elementObject.getJSONObject(API_Params.distance);
                                            if (distanceObject != null) {
                                                DistanceDetails distanceDetails = new DistanceDetails();
                                                distanceDetails.setText(distanceObject.optString(API_Params.text));
                                                distanceDetails.setValue(distanceObject.optInt(API_Params.value));
                                                distanceElement.setDistance(distanceDetails);
                                            }

                                            JSONObject durationObject = elementObject.getJSONObject(API_Params.duration);
                                            if (durationObject != null) {
                                                DistanceDuration duration = new DistanceDuration();
                                                duration.setText(durationObject.optString(API_Params.text));
                                                duration.setValue(durationObject.optInt(API_Params.value));
                                                distanceElement.setDuration(duration);
                                            }

                                        }
                                    }
                                }
                            }
                        } catch (JSONException e) {
                            CommonUtils.pLog(TAG, "Error : " + e.getMessage());
                            //CommonUtils.printLog(LOG_TAG, "Cannot process JSON results", e);
                        }
                    } catch (Exception e) {
                        CommonUtils.pLog(TAG, "Error line 235: " + e.getMessage());
                    } finally {
                        if (connection != null) {
                            connection.disconnect();
                        }
                    }
                    CommonUtils.pLog(TAG, new Gson().toJson(distanceElement));
                    if (distanceElement != null) {
                        return  distanceElement;
                    }
                } catch (Exception e) {
                    CommonUtils.pLog(TAG, "Distance matrix error: " + e.getMessage());
                    e.printStackTrace();
                }

            return null;
        }

        @Override
        protected void onPostExecute(DistanceElement s) {
            super.onPostExecute(s);
            getMvpView().onFetchingDistance(s);
        }
    }

    private String getDistanceUrl(String destLat, String destLong) {
        String url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" +
                getPreferencesHelper().getCurrentLatitude() + "," + getPreferencesHelper().getCurrentLongitude() +
                "&destinations=" + destLat + "," + destLong +
                "&key=" + APIUrl.googleApiKey;
        return url;
    }



}
