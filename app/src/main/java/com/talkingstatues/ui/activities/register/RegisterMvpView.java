package com.talkingstatues.ui.activities.register;

import com.talkingstatues.model.login.LoginMaster;
import com.talkingstatues.ui.base.MvpView;

public interface RegisterMvpView extends MvpView {

    void onGetLogs(String whichApi, String errorMsg);
    void  onRegisterSuccess(LoginMaster master);

}
