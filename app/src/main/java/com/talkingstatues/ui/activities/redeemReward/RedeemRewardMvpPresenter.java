package com.talkingstatues.ui.activities.redeemReward;

import android.app.Activity;

import com.talkingstatues.di.PerActivity;
import com.talkingstatues.ui.base.MvpPresenter;

@PerActivity
public interface RedeemRewardMvpPresenter<V extends RedeemRewardMvpView> extends MvpPresenter<V> {
   void  redeemReward(String cityId);
   void  showConformationDialog(Activity activity,String cityId);

    void getRewardDetails(String cityId);
}
