package com.talkingstatues.ui.activities.statuesMap;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.talkingstatues.R;
import com.talkingstatues.utilities.CommonUtils;

public class InfoWidnowAdapter implements GoogleMap.InfoWindowAdapter {
    private final View myContentsView;
    private Context context;
    private String imageUrl;

    InfoWidnowAdapter(Context context) {
        this.context = context;
        myContentsView = LayoutInflater.from(context).inflate(R.layout.custom_info_window, null);
    }



    @Override
    public View getInfoContents(Marker marker) {
        TextView tvTitle = ((TextView) myContentsView.findViewById(R.id.staueName));
        ImageView imageView = (ImageView) myContentsView.findViewById(R.id.ivBanner);
        if(marker.getTag() != null){
            imageView.setImageBitmap((Bitmap)marker.getTag());
        }else {
            imageView.setImageResource(R.drawable.rewards_image);
        }
        return myContentsView;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }
}
