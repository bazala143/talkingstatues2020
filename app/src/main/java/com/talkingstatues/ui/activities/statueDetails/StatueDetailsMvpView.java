package com.talkingstatues.ui.activities.statueDetails;

import com.talkingstatues.ui.base.MvpView;

public interface StatueDetailsMvpView extends MvpView {
    void onGetLogs(String whichApi, String errorMsg);

}
