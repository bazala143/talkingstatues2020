
package com.talkingstatues.ui.activities.aboutUs;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.databinding.DataBindingUtil;

import com.talkingstatues.R;
import com.talkingstatues.databinding.ActivityAboutUsBinding;
import com.talkingstatues.model.aboutUs.AboutUsDetails;
import com.talkingstatues.ui.activities.main.MainActvity;
import com.talkingstatues.ui.base.BaseActivity;
import com.talkingstatues.utilities.AppConstants;
import com.talkingstatues.utilities.RequestCodes;

import java.util.List;

import javax.inject.Inject;

public class AboutUsActivity extends BaseActivity implements View.OnClickListener, AboutUsMvpView {

    ActivityAboutUsBinding binding;
    @Inject
    AboutUsMvpPresenter<AboutUsMvpView> mPresenter;
    private static final String TAG = AboutUsActivity.class.getSimpleName();
    String languageCode = AppConstants.LANGUAGE_ENGLISH;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivityComponent().inject(this);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_about_us);
        binding.setClickListener(this);
        mPresenter.onAttach(this);
        binding.toolbar.setClickListener(this);
        binding.toolbar.ivback.setVisibility(View.VISIBLE);
        binding.toolbar.tvTitle.setVisibility(View.VISIBLE);
        binding.toolbar.tvTitle.setText(R.string.text_about_us);
        languageCode = mPresenter.getLanguageCode();
        mPresenter.getAbousUsApi();


    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        if (v == binding.toolbar.ivback) {
            finish();
        }
    }


    @Override
    public void onAbousLoaded(List<AboutUsDetails> json) {
        if(languageCode.equalsIgnoreCase(AppConstants.LANGUAGE_SPANISH)){
            binding.tvAbousUs.setText(json.get(0).getSpanishDetails());
        }else{
            binding.tvAbousUs.setText(json.get(0).getDetails());

        }
    }


    @Override
    public void onGetLogs(String whichApi, String errorMsg) {

    }


}
