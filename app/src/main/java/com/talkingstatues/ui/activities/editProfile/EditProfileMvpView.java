package com.talkingstatues.ui.activities.editProfile;



import com.talkingstatues.model.login.LoginDetails;
import com.talkingstatues.ui.base.MvpView;

public interface EditProfileMvpView extends MvpView {

    void getLoginDetails(LoginDetails loginDetails);
}
