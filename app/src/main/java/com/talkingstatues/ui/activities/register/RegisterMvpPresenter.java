package com.talkingstatues.ui.activities.register;

import android.widget.EditText;

import com.talkingstatues.databinding.ActivityRegisterBinding;
import com.talkingstatues.di.PerActivity;
import com.talkingstatues.ui.base.MvpPresenter;

@PerActivity
public interface RegisterMvpPresenter<V extends RegisterMvpView> extends MvpPresenter<V> {


    boolean isValid(ActivityRegisterBinding binding);

    void registerApi(ActivityRegisterBinding binding);
}
