
package com.talkingstatues.ui.activities.placeDetailsActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.talkingstatues.R;
import com.talkingstatues.databinding.ActivityPlaceDetailsBinding;
import com.talkingstatues.model.banner.BannerDetails;
import com.talkingstatues.model.distance.DistanceElement;
import com.talkingstatues.model.placeDetails.PlaceInfoDetails;
import com.talkingstatues.model.placeDetails.PlaceInfoMaster;
import com.talkingstatues.model.placeList.PlaceDetails;
import com.talkingstatues.networking.APIUrl;
import com.talkingstatues.ui.activities.register.RegisterActivity;
import com.talkingstatues.ui.activities.main.MainActvity;
import com.talkingstatues.ui.activities.webviewActivity.WebViewActivity;
import com.talkingstatues.ui.base.BaseActivity;
import com.talkingstatues.utilities.AppConstants;
import com.talkingstatues.utilities.RequestCodes;

import java.util.Calendar;

import javax.inject.Inject;

public class PlaceDetailsActivity extends BaseActivity implements View.OnClickListener, PlaceDetailsMvpView {

    ActivityPlaceDetailsBinding binding;
    @Inject
    PlaceDetailsMvpPresenter<PlaceDetailsMvpView> mPresenter;
    private static final String TAG = PlaceDetailsActivity.class.getSimpleName();
    private PlaceDetails placeDetails;
    private BannerDetails bannerDetails;
    PlaceInfoMaster placeInfoMaster;
    String placeId = "";
    String languageCode = AppConstants.LANGUAGE_ENGLISH;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivityComponent().inject(this);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_place_details);
        binding.setClickListener(this);
        mPresenter.onAttach(this);
        binding.toolbar.setClickListener(this);
        binding.toolbar.ivback.setVisibility(View.VISIBLE);
        binding.toolbar.tvTitle.setVisibility(View.VISIBLE);
        binding.toolbar.tvTitle.setText(R.string.app_name);
        Intent i = getIntent();
        if (i.hasExtra(AppConstants.BannerDetails)) {
            bannerDetails = (BannerDetails) i.getExtras().get(AppConstants.BannerDetails);
            placeId = bannerDetails.getPlaceId();
        } else {
            placeDetails = (PlaceDetails) i.getExtras().get(AppConstants.Object);
            placeId = placeDetails.getPlaceId();
        }
      languageCode = mPresenter.getLangugeCode();
        mPresenter.getPlaceInfoApi(placeId);
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        if (v == binding.tvWebsite) {
            if (placeInfoMaster.getPlaceInfoDetails().getPlaceWebUrl() != null) {
                Intent i = new Intent(PlaceDetailsActivity.this, WebViewActivity.class);
                i.putExtra("url", placeInfoMaster.getPlaceInfoDetails().getPlaceWebUrl());
                i.putExtra("toolbarName", getString(R.string.app_name));
                startActivity(i);
            } else {
                Toast.makeText(this, "No website found", Toast.LENGTH_SHORT).show();
            }

        } else if (v == binding.toolbar.ivback) {
            finish();
        }
    }


    @Override
    public void onPlaceDetailsLoaded(PlaceInfoMaster loginMaster) {
        placeInfoMaster = loginMaster;
        PlaceInfoDetails placeInfoDetails = loginMaster.getPlaceInfoDetails();
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.ic_place_holder);
        Glide.with(binding.getRoot().getContext())
                .applyDefaultRequestOptions(requestOptions)
                .load(APIUrl.IMAGE_URL + placeInfoDetails.getPlaceImage())
                .into(binding.placeImage);
        if(languageCode.equalsIgnoreCase(AppConstants.LANGUAGE_SPANISH)){
            if(placeInfoDetails.getSpanishPlaceName() != null){
                binding.tvPlaceName.setText(placeInfoDetails.getSpanishPlaceName());
            }else{
                binding.tvPlaceName.setText(placeInfoDetails.getPlaceName());
            }
            if(placeInfoDetails.getSpanishDetails() != null){
                binding.tvPlaceDesc.setText(placeInfoDetails.getSpanishDetails());
            }else{
                binding.tvPlaceDesc.setText(placeInfoDetails.getDetails());
            }
        }else{
            binding.tvPlaceAddress.setText(placeInfoDetails.getPlaceAddress());
            binding.tvPlaceDesc.setText(placeInfoDetails.getDetails());
            binding.tvPlaceName.setText(placeInfoDetails.getPlaceName());
        }



        Calendar c = Calendar.getInstance();
        int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
        String[] weekDays = placeInfoDetails.getOpeningWeekdays().split(",");
        String time = weekDays[dayOfWeek-1];

        if (time.equals("-")) {
            binding.linTime.setVisibility(View.VISIBLE);
            binding.tvOpenTime.setText("NA");
            binding.tvOpenToday.setText(getResources().getString(R.string.text_closed));
            binding.tvOpenToday.setTextColor(Color.RED);
        } else {
            binding.linTime.setVisibility(View.VISIBLE);
            binding.tvOpenTime.setText(time);
            binding.tvOpenToday.setText(getResources().getString(R.string.text_open_today));
            binding.tvOpenToday.setTextColor(Color.GREEN);
        }
    }

    @Override
    public void onFetchingDistance(DistanceElement s) {
        if(s != null && s.getDistance() != null){
            binding.tvDistanceName.setText(String.valueOf(s.getDistance().getValue() * 0.000621371192).substring(0, 4) + " Miles");
        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RequestCodes.REQUEST_MAIN_ACTIVITY && resultCode == RESULT_OK) {
            setResult(RESULT_OK);
            finish();
        }
    }
}
