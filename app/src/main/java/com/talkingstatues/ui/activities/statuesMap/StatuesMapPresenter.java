package com.talkingstatues.ui.activities.statuesMap;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AlertDialog;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.talkingstatues.R;
import com.talkingstatues.data.prefs.AppPreferencesHelper;
import com.talkingstatues.model.directionApi.DirectionResults;
import com.talkingstatues.model.directionApi.LocationBean;
import com.talkingstatues.model.directionApi.Route;
import com.talkingstatues.model.directionApi.Steps;
import com.talkingstatues.model.distance.DistanceDetails;
import com.talkingstatues.model.distance.DistanceDuration;
import com.talkingstatues.model.distance.DistanceElement;
import com.talkingstatues.model.distance.DistanceRow;
import com.talkingstatues.model.statueDetail.StatueDetailMaster;
import com.talkingstatues.model.statueList.StatueDetails;
import com.talkingstatues.model.statueList.StatueMaster;
import com.talkingstatues.model.visitStatue.VisitStatueMaster;
import com.talkingstatues.networking.APIUrl;
import com.talkingstatues.networking.API_Params;
import com.talkingstatues.networking.NetworkError;
import com.talkingstatues.networking.NetworkService;
import com.talkingstatues.networking.WebServices;
import com.talkingstatues.ui.activities.main.MainPresenter;
import com.talkingstatues.ui.activities.placeDetailsActivity.PlaceDetailsPresenter;
import com.talkingstatues.ui.base.BasePresenter;
import com.talkingstatues.utilities.AppConstants;
import com.talkingstatues.utilities.CommonUtils;
import com.talkingstatues.utilities.RouteDecode;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

public class StatuesMapPresenter<V extends StatuesMapMvpView> extends BasePresenter<V>
        implements StatuesMapMvpPresenter<V> {
    @Inject
    NetworkService service;
    private static final int LOCATION_PERMISSION_CODE = 102;
    private static final String TAG = PlaceDetailsPresenter.class.getSimpleName();
    GetStatyeDistanceAsync getStatyeDistanceAsync;
    GetStatueDetailsAsync getStatueDetailsAsync;

    @Inject
    public StatuesMapPresenter(AppPreferencesHelper preferencesHelper, CompositeSubscription mSubscription) {
        super(preferencesHelper, mSubscription);
    }


    @Override
    public Bitmap createCustomMarker(Context context, Bitmap imageBitmap, int markerType) {
        View marker = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_marker, null);
        final ImageView markerImage = (ImageView) marker.findViewById(R.id.markerImage);
        final RelativeLayout relAnnonation = (RelativeLayout) marker.findViewById(R.id.relAnnonation);
        if (markerType == AppConstants.MARKER_TYPE_BLACK) {
            relAnnonation.setBackgroundResource(R.drawable.ic_annotation);
        } else if (markerType == AppConstants.MARKER_TYPE_RED) {
            relAnnonation.setBackgroundResource(R.drawable.ic_annotation_red);
        } else {
            relAnnonation.setBackgroundResource(R.drawable.ic_green_marker);
        }
        if (imageBitmap != null) {
            markerImage.setImageBitmap(imageBitmap);
        }
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        marker.setLayoutParams(new ViewGroup.LayoutParams(52, ViewGroup.LayoutParams.WRAP_CONTENT));
        marker.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        marker.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        marker.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(marker.getMeasuredWidth(), marker.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        marker.draw(canvas);
        return bitmap;
    }

    @Override
    public void getStatueListApi(String cityId) {
        if (getMvpView().isNetworkConnected()) {
            getMvpView().showLoading();
            Subscription s = service.statueList(loginParams(cityId), new NetworkService.GetStatueListCallbackCallback() {
                @Override
                public void onSuccess(StatueMaster master) {
                    getMvpView().hideLoading();
                    if (master.getCode().equals("0")) {
                        getMvpView().onStatueListLoaded(master.getJson());
                    } else {
                        getMvpView().showMessage(master.getMessage());
                    }

                }

                @Override
                public void onError(NetworkError networkError) {
                    getMvpView().hideLoading();
                    getMvpView().showMessage(R.string.oops);
                }
            });
            getSubscription().add(s);
        } else {
            getMvpView().onError(R.string.utils__no_connection);
        }
    }

    @SuppressLint("StaticFieldLeak")
    public class GetStatyeDistanceAsync extends AsyncTask<String, String, DistanceElement> {
        LatLng latOrigin, latDestination;

        public GetStatyeDistanceAsync(LatLng origin, LatLng destination) {
            this.latOrigin = origin;
            this.latDestination = destination;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected DistanceElement doInBackground(String... strings) {
            String latitude = latDestination.latitude + "";
            String longitude = latDestination.longitude + "";
            try {
                DistanceElement distanceElement = null;
                HttpURLConnection connection = null;
                StringBuilder jsonResults = new StringBuilder();
                try {
                    //String latitude = objects[0];
                    //String longitude = objects[1];
                    CommonUtils.pLog(TAG, getDistanceUrl(latOrigin, latitude, longitude));
                    URL url = new URL(getDistanceUrl(latOrigin, latitude, longitude));
                    connection = (HttpURLConnection) url.openConnection();

                    CommonUtils.pLog(TAG, "Response code : " + connection.getResponseCode());
                    InputStreamReader in = new InputStreamReader(connection.getInputStream());

                    // Load the results into a StringBuilder
                    int read;
                    char[] buff = new char[1024];
                    while ((read = in.read(buff)) != -1) {
                        jsonResults.append(buff, 0, read);
                    }

                    try {
                        JSONObject jsonMain = new JSONObject(jsonResults.toString());
                        CommonUtils.pLog(TAG, "json : " + jsonMain.toString());
                        String status = jsonMain.optString(API_Params.status);
                        if (!CommonUtils.isEmpty(status) && CommonUtils.equals(status, AppConstants.STATUS_OK)) {
                            List<String> destinationAddresses = new ArrayList<>();
                            JSONArray destinationAddressArray = jsonMain.getJSONArray(API_Params.destination_addresses);
                            if (destinationAddressArray != null && destinationAddressArray.length() > 0) {
                                destinationAddresses.add(destinationAddressArray.getString(0));
                            }

                            List<String> originAddresses = new ArrayList<>();
                            JSONArray originAddressArray = jsonMain.getJSONArray(API_Params.origin_addresses);
                            if (originAddressArray != null && originAddressArray.length() > 0) {
                                originAddresses.add(originAddressArray.getString(0));
                            }

                            List<DistanceRow> distanceRows = new ArrayList<>();
                            JSONArray rowsArray = jsonMain.getJSONArray(API_Params.rows);
                            if (rowsArray != null && rowsArray.length() > 0) {
                                List<DistanceElement> distanceElements = new ArrayList<>();
                                JSONObject jsonObject = rowsArray.getJSONObject(0);
                                JSONArray elementsArray = jsonObject.getJSONArray(API_Params.elements);
                                if (elementsArray != null && elementsArray.length() > 0) {
                                    JSONObject elementObject = elementsArray.getJSONObject(0);
                                    if (elementObject != null && CommonUtils.equals(elementObject.optString(API_Params.status), AppConstants.STATUS_OK)) {
                                        distanceElement = new DistanceElement();
                                        JSONObject distanceObject = elementObject.getJSONObject(API_Params.distance);
                                        if (distanceObject != null) {
                                            DistanceDetails distanceDetails = new DistanceDetails();
                                            distanceDetails.setText(distanceObject.optString(API_Params.text));
                                            distanceDetails.setValue(distanceObject.optInt(API_Params.value));
                                            distanceElement.setDistance(distanceDetails);
                                        }

                                        JSONObject durationObject = elementObject.getJSONObject(API_Params.duration);
                                        if (durationObject != null) {
                                            DistanceDuration duration = new DistanceDuration();
                                            duration.setText(durationObject.optString(API_Params.text));
                                            duration.setValue(durationObject.optInt(API_Params.value));
                                            distanceElement.setDuration(duration);
                                        }

                                    }
                                }
                            }
                        }
                    } catch (JSONException e) {
                        CommonUtils.pLog(TAG, "Error : " + e.getMessage());
                        //CommonUtils.printLog(LOG_TAG, "Cannot process JSON results", e);
                    }
                } catch (Exception e) {
                    CommonUtils.pLog(TAG, "Error line 235: " + e.getMessage());
                } finally {
                    if (connection != null) {
                        connection.disconnect();
                    }
                }
                CommonUtils.pLog(TAG, new Gson().toJson(distanceElement));
                if (distanceElement != null) {
                    return distanceElement;
                }
            } catch (Exception e) {
                CommonUtils.pLog(TAG, "Distance matrix error: " + e.getMessage());
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(DistanceElement s) {
            super.onPostExecute(s);
            getMvpView().onFetchingDistance(s);
        }
    }

    private String getDistanceUrl(LatLng originLatLong, String destLat, String destLong) {
        String url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" +
                originLatLong.latitude + "," + originLatLong.longitude +
                "&destinations=" + destLat + "," + destLong +
                "&key=" + APIUrl.googleApiKey;
        return url;
    }

    @Override
    public void visitStatueApi(final StatueDetails statueDetails) {
        if (getMvpView().isNetworkConnected()) {
            getMvpView().showLoading();
            Subscription subscription = service.getVisitStatue(visitStatueParams(statueDetails), new NetworkService.GetVisitStatueCallback() {
                @Override
                public void onSuccess(VisitStatueMaster master) {
                    getMvpView().hideLoading();
                    Log.e("response", new Gson().toJson(master));
                    if (master.getCode().equals("0")) {
                        getMvpView().showMessage(R.string.added_to_statue);
                        getMvpView().onStatueVisited(statueDetails);

                    } else {
                        getMvpView().onError(R.string.failed_to_upload);

                    }

                }

                @Override
                public void onError(NetworkError networkError) {
                    getMvpView().hideLoading();
                    getMvpView().onError(networkError.getMessage() + "");
                }
            });
            getSubscription().add(subscription);
        } else {
            getMvpView().onError(R.string.utils__no_connection);
        }
    }

    private JsonObject visitStatueParams(StatueDetails statueDetails) {
        JsonObject gsonObject = new JsonObject();
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(API_Params.statue_id, statueDetails.getStatueId());
            jsonObject.put(API_Params.user_id, getPreferencesHelper().getLoginModel().getId() + "");
            JsonParser parser = new JsonParser();
            gsonObject = (JsonObject) parser.parse(jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return gsonObject;
    }


    @Override
    public void getDistanceApi(LatLng orgLatLng, LatLng destLatLng) {
        getStatyeDistanceAsync = new GetStatyeDistanceAsync(orgLatLng, destLatLng);
        getStatyeDistanceAsync.execute();
    }

    private JsonObject loginParams(String cityId) {
        JsonObject gsonObject = new JsonObject();
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(API_Params.user_id, getPreferencesHelper().getLoginModel().getId() + "");
            jsonObject.put(API_Params.city_id, cityId);
            JsonParser parser = new JsonParser();
            gsonObject = (JsonObject) parser.parse(jsonObject.toString());
            Log.e("parameter", jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return gsonObject;
    }

    @Override
    public void showRequiredPermissionDialog(final Activity activity) {
        new AlertDialog.Builder(activity, R.style.Theme_AppCompat_Dialog_Alert)
                .setTitle(R.string.location_allow)
                .setMessage(R.string.permission_denied_message)
                .setPositiveButton(R.string.location_allow, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            dialog.cancel();
                            activity.requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                                    LOCATION_PERMISSION_CODE);
                        }
                    }
                })
                .setNegativeButton(R.string.location_exit, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        showRequiredPermissionDialog(activity);
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_map)
                .show();
    }


    @Override
    public List<LatLng> getPolyline(LatLng sorceLatLong, LatLng destLatLong) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://maps.googleapis.com")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        WebServices service = retrofit.create(WebServices.class);
        Call<DirectionResults> repos = service.getPolyLineJson(sorceLatLong.latitude + "," + sorceLatLong.longitude, destLatLong.latitude + "," + destLatLong.longitude, APIUrl.googleApiKey);

        Log.e("url", repos.request().url().toString());
        repos.enqueue(new Callback<DirectionResults>() {
            @Override
            public void onResponse(Call<DirectionResults> call, Response<DirectionResults> response) {
                DirectionResults directionResults = response.body();
                Log.e("PolyLine", new Gson().toJson(response.body()));
                ArrayList<LatLng> routelist = new ArrayList<LatLng>();
                if (directionResults.getRoutes().size() > 0) {
                    ArrayList<LatLng> decodelist = new ArrayList<>();
                    Route routeA = directionResults.getRoutes().get(0);
                    if (routeA.getLegs().size() > 0) {
                        List<Steps> steps = routeA.getLegs().get(0).getSteps();
                        Steps step;
                        LocationBean location;
                        String polyline;
                        for (int i = 0; i < steps.size(); i++) {
                            step = steps.get(i);
                            location = step.getStart_location();
                            routelist.add(new LatLng(location.getLat(), location.getLng()));
                            polyline = step.getPolyline().getPoints();
                            decodelist = RouteDecode.decodePoly(polyline);
                            routelist.addAll(decodelist);
                            location = step.getEnd_location();
                            routelist.add(new LatLng(location.getLat(), location.getLng()));
                        }
                    }

                    if (decodelist.size() > 0) {
                        getMvpView().onLoadedPolyline(routelist);

                    }
                }


            }

            @Override
            public void onFailure(Call<DirectionResults> call, Throwable t) {
                Log.e("Exception", t.getMessage());
            }
        });

        return null;
    }

    @Override
    public void getStatueDetailsApi(String qrCode) {
        if (getMvpView().isNetworkConnected()) {
            getMvpView().showLoading();
            Subscription subscription = service.getStatueDetails(statueDetailsparams(qrCode), new NetworkService.GetStatueDetailsCallback() {
                @Override
                public void onSuccess(StatueDetailMaster master) {
                    getMvpView().hideLoading();
                    Log.e("response", new Gson().toJson(master));
                    if (master.getCode().equals("0")) {
                        //   getMvpView().showMessage(R.string.added_to_statue);
                        visitStatueApi(master.getJson());

                    } else {
                        getMvpView().onError(R.string.failed_to_upload);

                    }

                }

                @Override
                public void onError(NetworkError networkError) {
                    getMvpView().hideLoading();
                    getMvpView().onError(networkError.getMessage() + "");
                }
            });
            getSubscription().add(subscription);
        } else {
            getMvpView().onError(R.string.utils__no_connection);
        }
    }

    private JsonObject statueDetailsparams(String qrCode) {
        JsonObject gsonObject = new JsonObject();
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(API_Params.qr_code, qrCode);
            jsonObject.put(API_Params.user_id, getPreferencesHelper().getLoginModel().getId() + "");
            JsonParser parser = new JsonParser();
            gsonObject = (JsonObject) parser.parse(jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return gsonObject;
    }

    @Override
    public void getStatueDetailsFromPrefs(String qrCode) {
        getStatueDetailsAsync = new GetStatueDetailsAsync();
        getStatueDetailsAsync.execute(qrCode);
    }

    @Override
    public String getLanguageCode() {
        return getPreferencesHelper().getLanguageCode();
    }


    @SuppressLint("StaticFieldLeak")
    private class GetStatueDetailsAsync extends AsyncTask<String, Void, StatueDetails> {
        String qrCode;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            getMvpView().showLoading();
        }

        @Override
        protected void onPostExecute(StatueDetails statueDetails) {
            super.onPostExecute(statueDetails);
            getMvpView().hideLoading();
            if (statueDetails != null) {
                visitStatueApi(statueDetails);
            } else {
                getMvpView().openStatueLInk(qrCode);
            }
        }

        @Override
        protected StatueDetails doInBackground(String... strings) {
            StatueMaster statueMaster = getPreferencesHelper().getStatueModel();
            StatueDetails returnDetails = null;
            qrCode = strings[0];
            for (StatueDetails statueDetails : statueMaster.getJson()) {
                if (statueDetails.getQrCode() != null) {
                    if (statueDetails.getQrCode().contains(strings[0]) || strings[0].equalsIgnoreCase(statueDetails.getQrCode())) {
                        returnDetails = statueDetails;
                        break;
                    }
                }

                if (statueDetails.getSpanishQrCode() != null) {
                    if (strings[0].equalsIgnoreCase(statueDetails.getSpanishQrCode()) || statueDetails.getSpanishQrCode().contains(strings[0])) {
                        returnDetails = statueDetails;
                        Log.e("Matched", "Matched");
                        break;
                    }
                }

            }
            return returnDetails;
        }
    }


}
