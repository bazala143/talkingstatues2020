package com.talkingstatues.ui.activities.login;

import android.os.Build;
import android.util.Log;
import android.widget.EditText;

import androidx.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.talkingstatues.R;
import com.talkingstatues.data.prefs.AppPreferencesHelper;
import com.talkingstatues.databinding.ActivityLoginBinding;
import com.talkingstatues.model.login.LoginDetails;
import com.talkingstatues.model.login.LoginMaster;
import com.talkingstatues.networking.APIUrl;
import com.talkingstatues.networking.API_Params;
import com.talkingstatues.networking.NetworkError;
import com.talkingstatues.networking.NetworkService;
import com.talkingstatues.ui.base.BasePresenter;
import com.talkingstatues.utilities.AppConstants;
import com.talkingstatues.utilities.CommonUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.inject.Inject;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

public class LoginPresenter<V extends LoginMvpView> extends BasePresenter<V>
        implements LoginMvpPresenter<V> {
    @Inject
    NetworkService service;
    private static final String TAG = LoginPresenter.class.getSimpleName();

    @Override
    public void onAttach(V mvpView) {
        super.onAttach(mvpView);
    }

    @Inject
    public LoginPresenter(AppPreferencesHelper preferencesHelper, CompositeSubscription mSubscription) {
        super(preferencesHelper, mSubscription);
    }

    @Override
    public boolean isValid(ActivityLoginBinding binding) {
        if (CommonUtils.isEmpty(binding.edEmail.getText().toString())) {
            binding.edEmail.setError(getMvpView().getStringFromId(R.string.please_enter_email));
            binding.edEmail.requestFocus();
            return false;
        } else if (!CommonUtils.checkEmail(binding.edEmail.getText().toString().trim())) {
            binding.edEmail.setError(getMvpView().getStringFromId(R.string.please_enter_valid_email));
            binding.edEmail.requestFocus();
            return false;
        } else if (CommonUtils.isEmpty(binding.edPassword.getText().toString())) {
            binding.edPassword.setError(getMvpView().getStringFromId(R.string.text_password));
            binding.edPassword.requestFocus();
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void loginApi(ActivityLoginBinding binding) {
        if (getMvpView().isNetworkConnected()) {
            getMvpView().showLoading();
            Subscription subscription = service.getClientLogin(loginParams(binding), new NetworkService.GetLoginCallback() {
                @Override
                public void onSuccess(LoginMaster loginMaster) {
                    getMvpView().hideLoading();
                    if (loginMaster != null) {
                        CommonUtils.pLog(TAG, "Response : " + new Gson().toJson(loginMaster).toString());
                        if (loginMaster.getCode().equals("0")) {
                            LoginDetails loginDetails = loginMaster.getLoginDetails();
                            if (loginDetails != null) {
                                getPreferencesHelper().setLoginDetails(new Gson().toJson(loginDetails));
                                getMvpView().onLoginSuccess();
                            }
                        } else {
                            getMvpView().onError(loginMaster.getMessage() + "");
                            getMvpView().onGetLogs(APIUrl.login, loginMaster.getMessage() + "");
                        }
                    }
                }

                @Override
                public void onError(NetworkError networkError) {
                    getMvpView().hideLoading();
                    getMvpView().onError(networkError.getMessage() + "");
                    getMvpView().onGetLogs(APIUrl.login, networkError.getMessage().toString() + "");
                }
            });
            getSubscription().add(subscription);
        } else {
            getMvpView().onError(R.string.utils__no_connection);
            getMvpView().onGetLogs(APIUrl.login, "NO INTERNET");
        }
    }

    @Override
    public void loginWithfacebook(String name, String email) {
        if (getMvpView().isNetworkConnected()) {
            getMvpView().showLoading();
            Subscription subscription = service.loginWithfacebook(fbParams(name,email), new NetworkService.GetLoginCallback() {
                @Override
                public void onSuccess(LoginMaster loginMaster) {
                    getMvpView().hideLoading();
                    if (loginMaster != null) {
                        CommonUtils.pLog(TAG, "Response : " + new Gson().toJson(loginMaster).toString());
                        if (loginMaster.getCode().equals("0")) {
                            loginMaster.getLoginDetails().setAge(0);
                            loginMaster.getLoginDetails().setEmail(loginMaster.getLoginDetails().getFacebook());
                            loginMaster.getLoginDetails().setMobileNumber(" ");
                            LoginDetails loginDetails = loginMaster.getLoginDetails();
                            if (loginDetails != null) {
                                getPreferencesHelper().setLoginDetails(new Gson().toJson(loginDetails));
                                getMvpView().onLoginSuccess();
                            }
                        } else {
                            getMvpView().onError(loginMaster.getMessage() + "");
                            getMvpView().onGetLogs(APIUrl.login, loginMaster.getMessage() + "");
                        }
                    }
                }

                @Override
                public void onError(NetworkError networkError) {
                    getMvpView().hideLoading();
                    getMvpView().onError(networkError.getMessage() + "");
                    getMvpView().onGetLogs(APIUrl.login, networkError.getMessage().toString() + "");
                }
            });
            getSubscription().add(subscription);
        } else {
            getMvpView().onError(R.string.utils__no_connection);
            getMvpView().onGetLogs(APIUrl.login, "NO INTERNET");
        }
    }

    @Override
    public void forgotPassword(String email) {
        if (getMvpView().isNetworkConnected()) {
            getMvpView().showLoading();
            Subscription subscription = service.forgotPasswordApi(forgotParams(email), new NetworkService.GetForgotPasswordCallback() {
                @Override
                public void onSuccess(LoginMaster loginMaster) {
                    getMvpView().hideLoading();
                    if (loginMaster != null) {
                        CommonUtils.pLog(TAG, "Response : " + new Gson().toJson(loginMaster).toString());
                        if (loginMaster.getCode().equals("0")) {
                            getMvpView().showMessage(loginMaster.getMessage());
                        } else {
                            getMvpView().onError(loginMaster.getMessage() + "");
                            getMvpView().onGetLogs(APIUrl.login, loginMaster.getMessage() + "");
                        }
                    }
                }

                @Override
                public void onError(NetworkError networkError) {
                    getMvpView().hideLoading();
                    getMvpView().onError(networkError.getMessage() + "");
                    getMvpView().onGetLogs(APIUrl.login, networkError.getMessage() + "");
                }
            });
            getSubscription().add(subscription);
        } else {
            getMvpView().onError(R.string.utils__no_connection);
            getMvpView().onGetLogs(APIUrl.login, "NO INTERNET");
        }
    }

    private JsonObject forgotParams(String email) {
        JsonObject gsonObject = new JsonObject();
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(API_Params.email, email);
            JsonParser parser = new JsonParser();
            gsonObject = (JsonObject) parser.parse(jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("loginParams", gsonObject.toString());
        return gsonObject;
    }

    private JsonObject loginParams(ActivityLoginBinding binding) {
        JsonObject gsonObject = new JsonObject();
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(API_Params.email, binding.edEmail.getText().toString().trim());
            jsonObject.put(API_Params.password, binding.edPassword.getText().toString().trim());
            JsonParser parser = new JsonParser();
            gsonObject = (JsonObject) parser.parse(jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("loginParams", gsonObject.toString());
        return gsonObject;
    }

    private JsonObject fbParams(String name,String email) {
        JsonObject gsonObject = new JsonObject();
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(API_Params.email, email);
            jsonObject.put(API_Params.name, name);
            JsonParser parser = new JsonParser();
            gsonObject = (JsonObject) parser.parse(jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("loginParams", gsonObject.toString());
        return gsonObject;
    }


    @Override
    public void onDetach() {
        if (getSubscription() != null && getSubscription().hasSubscriptions()) {
            getSubscription().clear();
        }
    }

    public static int age(Date birthday, Date date) {
        DateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        int d1 = Integer.parseInt(formatter.format(birthday));
        int d2 = Integer.parseInt(formatter.format(date));
        int age = (d2-d1)/10000;
        return age;
    }


}
