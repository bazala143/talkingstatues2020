
package com.talkingstatues.ui.activities.register;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.databinding.DataBindingUtil;

import com.talkingstatues.R;
import com.talkingstatues.databinding.ActivityRegisterBinding;
import com.talkingstatues.model.login.LoginMaster;
import com.talkingstatues.ui.activities.login.LoginActivity;
import com.talkingstatues.ui.activities.main.MainActvity;
import com.talkingstatues.ui.base.BaseActivity;
import com.talkingstatues.utilities.RequestCodes;

import javax.inject.Inject;

public class RegisterActivity extends BaseActivity implements View.OnClickListener, RegisterMvpView {

    ActivityRegisterBinding binding;
    @Inject
    RegisterMvpPresenter<RegisterMvpView> mPresenter;
    private static final String TAG = RegisterActivity.class.getSimpleName();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        getActivityComponent().inject(this);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_register);
        binding.setClickListener(this);
        mPresenter.onAttach(this);
        binding.toolbar.setClickListener(this);
        binding.toolbar.ivback.setVisibility(View.VISIBLE);
        binding.toolbar.tvTitle.setVisibility(View.VISIBLE);
        binding.toolbar.tvTitle.setText(R.string.text_register);
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        if (v == binding.btnRegister) {
            if (mPresenter.isValid(binding)) {
                mPresenter.registerApi(binding);
            }
        }else  if(v == binding.linAlready){
            Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            RegisterActivity.this.finish();
        }else if(v == binding.toolbar.ivback) {
            RegisterActivity.this.finish();
        }
    }


    @Override
    public void onGetLogs(String whichApi, String errorMsg) {
        Log.e(TAG,whichApi+"???"+errorMsg);
    }

    @Override
    public void onRegisterSuccess(LoginMaster master) {
        Intent intent = new Intent(RegisterActivity.this, MainActvity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        RegisterActivity.this.finish();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RequestCodes.REQUEST_VERIFY_OTP_ACTIVITY && resultCode == RESULT_OK) {
            finish();
        }
    }
}
