package com.talkingstatues.ui.activities.splash;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;

import com.fusedbulblib.GetCurrentLocation;
import com.fusedbulblib.interfaces.DialogClickListener;
import com.fusedbulblib.interfaces.GpsOnListener;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.GoogleMap;
import com.talkingstatues.R;
import com.talkingstatues.databinding.ActivitySplashBinding;
import com.talkingstatues.ui.activities.login.LoginActivity;
import com.talkingstatues.ui.activities.main.MainActvity;
import com.talkingstatues.ui.activities.selectLanguage.LanguageActivity;
import com.talkingstatues.ui.base.BaseActivity;
import com.talkingstatues.ui.dialog.CheckGPSDialog;
import com.talkingstatues.ui.dialog.PermissionDeniedDialog;
import com.talkingstatues.ui.dialog.updateApp.UpdateAppDialog;
import com.talkingstatues.utilities.AppConstants;
import com.talkingstatues.utilities.CommonUtils;
import com.talkingstatues.utilities.LocaleHelper;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.inject.Inject;


public class SplashActivity extends BaseActivity implements SplashMvpView, GpsOnListener, LocationListener {

    ActivitySplashBinding binding;
    @Inject
    SplashMvpPresenter<SplashMvpView> mPresenter;
    GetCurrentLocation getCurrentLocation;
    private static final String TAG = SplashActivity.class.getSimpleName();
    private static final int REQUEST = 112;

    /*map variables*/

    LocationManager locationManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        getActivityComponent().inject(this);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash);
        mPresenter.onAttach(this);
        if(mPresenter.getLanguageCode() != null){
            CommonUtils.LanguageCode = mPresenter.getLanguageCode();
            mPresenter.onNextActivity();
        }else{
          //  mPresenter.setLanguageCode(AppConstants.LANGUAGE_ENGLISH);
           // CommonUtils.LanguageCode = AppConstants.LANGUAGE_ENGLISH;
            mPresenter.onNextActivity();
        }

        generateKeyHash();
    }

    private void generateKeyHash() {
        PackageInfo info;
        try {
            info = getPackageManager().getPackageInfo("com.talkingstatues", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md;
                md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String something = new String(Base64.encode(md.digest(), 0));
                //String something = new String(Base64.encodeBytes(md.digest()));
                Log.e("hash key", something);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("no such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("exception", e.toString());
        }
    }

    public void continueWithApp() {
        getCurrentLocation = new GetCurrentLocation(SplashActivity.this);
        getCurrentLocation.getCurrentLocation();
    }


    @Override
    public void openHomeActivity() {
        Intent intent = new Intent(SplashActivity.this, MainActvity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void openLoginActivity() {
        Intent i = new Intent(SplashActivity.this, LoginActivity.class);
        startActivity(i);
        finish();
    }

    @Override
    public void openLanguageActivity() {
        Intent i = new Intent(this, LanguageActivity.class);
        startActivity(i);
        this.finish();
    }


    @Override
    public void gpsStatus(boolean _status) {
        if (!_status) {
            new CheckGPSDialog(this).showDialog(new DialogClickListener() {
                @Override
                public void positiveListener(Activity context, Dialog dialog) {
                    dialog.dismiss();
                    getCurrentLocation.getCurrentLocation();
                }

                @Override
                public void negativeListener(Activity context, Dialog dialog) {
                    dialog.dismiss();
                }
            });
        } else {
            getCurrentLocation.getCurrentLocation();
        }
    }

    @Override
    public void gpsPermissionDenied(int deviceGpsStatus) {
        if (deviceGpsStatus == 1) {
            permissionDeniedByUser();
        } else {
            getCurrentLocation.getCurrentLocation();
        }
    }

    @Override
    public void gpsLocationFetched(Location location) {
        if (location != null) {
            CommonUtils.pLog(TAG, location.getLatitude() + ", " + location.getLongitude());
            if (location.hasAccuracy() && location.getAccuracy() < AppConstants.ACCURACY_LIMIT)
                mPresenter.storeLocation(String.valueOf(location.getLatitude()), String.valueOf(location.getLongitude()));
            mPresenter.onNextActivity();
        } else {
            Toast.makeText(this, getResources().getString(R.string.unable_find_location), Toast.LENGTH_SHORT).show();
        }
    }

    private void permissionDeniedByUser() {

        new PermissionDeniedDialog(this).showDialog(new DialogClickListener() {
            @Override
            public void positiveListener(Activity context, Dialog dialog) {
                dialog.dismiss();
                getCurrentLocation.getCurrentLocation();
            }

            @Override
            public void negativeListener(Activity context, Dialog dialog) {
                dialog.dismiss();
                mPresenter.onNextActivity();
            }
        });
    }


    private static boolean hasPermissions(Context context, String... PERMISSIONS) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && PERMISSIONS != null) {
            for (String permission : PERMISSIONS) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    continueWithApp();
                } else {
                    Toast.makeText(this, "Please allow to continue with app", Toast.LENGTH_LONG).show();
                }
            }
        }

    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    void registerLocationUpdates() {
        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_LOW);
        criteria.setPowerRequirement(Criteria.POWER_LOW);
        criteria.setAltitudeRequired(false);
        criteria.setBearingRequired(false);
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        String provider = locationManager.getBestProvider(criteria, true);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locationManager.requestLocationUpdates(provider, 0, 0, this);
        //  locationManager.request
    }
}
