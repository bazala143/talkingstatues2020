package com.talkingstatues.ui.activities.callinnNew;

import com.talkingstatues.di.PerActivity;
import com.talkingstatues.ui.base.MvpPresenter;

@PerActivity
public interface CallingNewMvpPresenter<V extends CallingNewMvpView> extends MvpPresenter<V> {

}
