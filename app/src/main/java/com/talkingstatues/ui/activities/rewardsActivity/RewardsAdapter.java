package com.talkingstatues.ui.activities.rewardsActivity;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.talkingstatues.R;
import com.talkingstatues.databinding.LayoutRewardBinding;
import com.talkingstatues.model.rewardsImage.RewardsDetails;
import com.talkingstatues.model.statueList.StatueDetails;
import com.talkingstatues.networking.APIUrl;
import com.talkingstatues.ui.base.BaseViewHolder;

import java.util.List;

public class RewardsAdapter extends RecyclerView.Adapter<BaseViewHolder> implements View.OnClickListener {
    private List<StatueDetails> mList;
    LayoutInflater layoutInflater;
    Activity activity;

    public RewardsAdapter(Activity activity, List<StatueDetails> mList) {
        this.activity = activity;
        this.mList = mList;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        LayoutRewardBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.layout_reward, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public int getItemCount() {
        return 10;
    }

    @Override
    public void onClick(View v) {

    }

    public void clear() {
        mList.clear();
        notifyDataSetChanged();
    }

    public class ViewHolder extends BaseViewHolder {
        LayoutRewardBinding binding;



        public ViewHolder(final LayoutRewardBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        @Override
        protected void clear() {

        }

        public void onBind(final int position) {
            super.onBind(position);
            if(position < mList.size()){
                final StatueDetails rewardsDetails = mList.get(position);
                RequestOptions requestOptions = new RequestOptions();
                requestOptions.placeholder(R.drawable.ic_place_holder);
                requestOptions.error(R.drawable.ic_place_holder);
                requestOptions.centerInside();
                Glide.with(binding.ivImage.getContext())
                        .setDefaultRequestOptions(requestOptions)
                        .load(R.drawable.ic_statue_active).into(binding.ivImage);
            }else{
                RequestOptions requestOptions = new RequestOptions();
                requestOptions.placeholder(R.drawable.ic_place_holder);
                requestOptions.error(R.drawable.ic_place_holder);
                requestOptions.centerInside();
                Glide.with(binding.ivImage.getContext())
                        .setDefaultRequestOptions(requestOptions)
                        .load(R.drawable.rewards_image).into(binding.ivImage);
            }



        }


    }




}