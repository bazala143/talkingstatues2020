package com.talkingstatues.ui.activities.callinnNew;

import com.talkingstatues.data.prefs.AppPreferencesHelper;
import com.talkingstatues.networking.NetworkService;
import com.talkingstatues.ui.base.BasePresenter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.inject.Inject;

import rx.subscriptions.CompositeSubscription;

public class CallingNewPresenter<V extends CallingNewMvpView> extends BasePresenter<V>
        implements CallingNewMvpPresenter<V> {
    @Inject
    NetworkService service;
    private static final String TAG = CallingNewPresenter.class.getSimpleName();



    @Inject
    public CallingNewPresenter(AppPreferencesHelper preferencesHelper, CompositeSubscription mSubscription) {
        super(preferencesHelper, mSubscription);
    }

    @Override
    public void onAttach(V mvpView) {
        super.onAttach(mvpView);
    }

    @Override
    public void onDetach() {
        if (getSubscription() != null && getSubscription().hasSubscriptions()) {
            getSubscription().clear();
        }
    }

    public static int age(Date birthday, Date date) {
        DateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        int d1 = Integer.parseInt(formatter.format(birthday));
        int d2 = Integer.parseInt(formatter.format(date));
        int age = (d2-d1)/10000;
        return age;
    }


}
