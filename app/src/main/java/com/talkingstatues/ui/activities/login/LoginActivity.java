
package com.talkingstatues.ui.activities.login;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.databinding.DataBindingUtil;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginBehavior;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.talkingstatues.R;
import com.talkingstatues.databinding.ActivityLoginBinding;
import com.talkingstatues.networking.APIUrl;
import com.talkingstatues.ui.activities.placeDetailsActivity.PlaceDetailsActivity;
import com.talkingstatues.ui.activities.register.RegisterActivity;
import com.talkingstatues.ui.activities.main.MainActvity;
import com.talkingstatues.ui.activities.webviewActivity.WebViewActivity;
import com.talkingstatues.ui.base.BaseActivity;
import com.talkingstatues.ui.dialog.forgotDialoge.DialogSuccess;
import com.talkingstatues.ui.dialog.forgotDialoge.DialogSuccessClickListener;
import com.talkingstatues.utilities.RequestCodes;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import javax.inject.Inject;

public class LoginActivity extends BaseActivity implements View.OnClickListener, LoginMvpView {

    ActivityLoginBinding binding;
    @Inject
    LoginMvpPresenter<LoginMvpView> mPresenter;
    private static final String TAG = LoginActivity.class.getSimpleName();
    CallbackManager callbackManager;
    private static final String EMAIL = "email";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        getActivityComponent().inject(this);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        binding.setClickListener(this);
        mPresenter.onAttach(this);
        binding.toolbar.setClickListener(this);
        binding.toolbar.ivback.setVisibility(View.GONE);
        binding.toolbar.tvTitle.setVisibility(View.VISIBLE);
        binding.toolbar.tvTitle.setText(R.string.text_login);

        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {

                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject object, GraphResponse response) {
                                        Log.v("LoginActivity", response.toString());

                                        // Application code
                                        try {
                                            String email = object.getString("email");
                                            String name = object.getString("name");
                                        //    String birthDate = object.getString("birthday");
                                         //   Log.e("birthday",birthDate);
                                            LoginManager.getInstance().logOut();
                                            binding.btnFacebook.setVisibility(View.VISIBLE);
                                            binding.progressBar.setVisibility(View.GONE);
                                            mPresenter.loginWithfacebook(name,email);// 01/31/1980 format

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,email,gender,birthday");
                        request.setParameters(parameters);
                        request.executeAsync();


                    }

                    @Override
                    public void onCancel() {
                        binding.btnFacebook.setVisibility(View.VISIBLE);
                        binding.progressBar.setVisibility(View.GONE);                    }

                    @Override
                    public void onError(FacebookException exception) {
                        binding.btnFacebook.setVisibility(View.VISIBLE);
                        binding.progressBar.setVisibility(View.GONE);                    }
                });

        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        boolean isLoggedIn = accessToken != null && !accessToken.isExpired();

    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        if (v == binding.btnSubmit) {
            hideKeyboard();
            if (mPresenter.isValid(binding)) {
                mPresenter.loginApi(binding);
            }
        } else if (v == binding.tvRegister1) {
            startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
        } else if (v == binding.tvForgot) {
            forgotConfirmation();
        }else if(v == binding.btnFacebook){
            binding.btnFacebook.setVisibility(View.GONE);
            binding.progressBar.setVisibility(View.VISIBLE);
            LoginManager.getInstance().setLoginBehavior(LoginBehavior.WEB_VIEW_ONLY);
            LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email"));
        }else if( v== binding.tvNeed){
            Intent i = new Intent(LoginActivity.this, WebViewActivity.class);
            i.putExtra("url", APIUrl.needHelp);
            i.putExtra("toolbarName", getString(R.string.text_help));
            startActivity(i);
        }
    }

    private void forgotConfirmation() {
        new DialogSuccess(LoginActivity.this).showDialog(new DialogSuccessClickListener() {
            @Override
            public void onCloseClick(Dialog dialog) {
                dialog.dismiss();
            }

            @Override
            public void onOkClick(Dialog dialog,String email) {
                mPresenter.forgotPassword(email);
                dialog.dismiss();
            }
        });

    }


    @Override
    public void redirectToHomeScreen() {
        Intent intent = new Intent(LoginActivity.this, MainActvity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        LoginActivity.this.finish();
    }

    @Override
    public void onGetLogs(String whichApi, String errorMsg) {

    }

    @Override
    public void onLoginSuccess() {
        Intent intent = new Intent(LoginActivity.this, MainActvity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        LoginActivity.this.finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }


}
