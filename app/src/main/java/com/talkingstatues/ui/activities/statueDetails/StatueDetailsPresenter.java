package com.talkingstatues.ui.activities.statueDetails;

import android.util.Log;
import android.widget.EditText;

import androidx.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.talkingstatues.R;
import com.talkingstatues.data.prefs.AppPreferencesHelper;
import com.talkingstatues.model.login.LoginDetails;
import com.talkingstatues.model.login.LoginMaster;
import com.talkingstatues.networking.APIUrl;
import com.talkingstatues.networking.API_Params;
import com.talkingstatues.networking.NetworkError;
import com.talkingstatues.networking.NetworkService;
import com.talkingstatues.ui.base.BasePresenter;
import com.talkingstatues.utilities.AppConstants;
import com.talkingstatues.utilities.CommonUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

public class StatueDetailsPresenter<V extends StatueDetailsMvpView> extends BasePresenter<V>
        implements StatueDetailsMvpPresenter<V> {
    @Inject
    NetworkService service;
    private static final String TAG = StatueDetailsPresenter.class.getSimpleName();



    @Inject
    public StatueDetailsPresenter(AppPreferencesHelper preferencesHelper, CompositeSubscription mSubscription) {
        super(preferencesHelper, mSubscription);
    }

    @Override
    public boolean isValid(EditText edMobile, EditText edPassword) {
        if (CommonUtils.isEmpty(edMobile.getText().toString())) {
            edMobile.setError(getMvpView().getStringFromId(R.string.text_mobile_number));
            edMobile.requestFocus();
            return false;
        } else if (CommonUtils.isEmpty(edPassword.getText().toString())) {
            edPassword.setError(getMvpView().getStringFromId(R.string.text_password));
            edPassword.requestFocus();
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void loginApi(final String mobileNumber, String password) {
        if (getMvpView().isNetworkConnected()) {
            getMvpView().showLoading();
            Subscription subscription = service.getClientLogin(loginParams(mobileNumber, password), new NetworkService.GetLoginCallback() {
                @Override
                public void onSuccess(LoginMaster loginMaster) {
                    getMvpView().hideLoading();
                    if (loginMaster != null) {
                        CommonUtils.pLog(TAG, "Response : " + new Gson().toJson(loginMaster).toString());

                    }
                }

                @Override
                public void onError(NetworkError networkError) {
                    getMvpView().hideLoading();
                    getMvpView().onError(networkError.getMessage() + "");
                    getMvpView().onGetLogs(APIUrl.login, networkError.getMessage().toString() + "");
                }
            });
            getSubscription().add(subscription);
        } else {
            getMvpView().onError(R.string.utils__no_connection);
            getMvpView().onGetLogs(APIUrl.login, "NO INTERNET");
        }
    }

    @Override
    public List<String> getVideoList() {
        List<String>  videoList = new ArrayList<>();
        videoList.add("kjsdgshdg");
        videoList.add("kjsdgshdg");
        videoList.add("kjsdgshdg");
        videoList.add("kjsdgshdg");
        return videoList;
    }

    @Override
    public String getLanguageCode() {
        return getPreferencesHelper().getLanguageCode();
    }

    private JsonObject loginParams(String mobileNumber, String password) {
        JsonObject gsonObject = new JsonObject();
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(API_Params.mobileNumber, mobileNumber);
            jsonObject.put(API_Params.password, password);
            jsonObject.put(API_Params.deviceToken, getPreferencesHelper().getDeviceToken());
            jsonObject.put(API_Params.device, AppConstants.DEVICE_TYPE);
            JsonParser parser = new JsonParser();
            gsonObject = (JsonObject) parser.parse(jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("loginParams",gsonObject.toString());
        return gsonObject;
    }


    @Override
    public void onDetach() {
        if (getSubscription() != null && getSubscription().hasSubscriptions()) {
            getSubscription().clear();
        }
    }




}
