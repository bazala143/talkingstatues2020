package com.talkingstatues.ui.activities.placeDetailsActivity;

import com.talkingstatues.model.distance.DistanceElement;
import com.talkingstatues.model.placeDetails.PlaceInfoMaster;
import com.talkingstatues.ui.base.MvpView;

public interface PlaceDetailsMvpView extends MvpView {


    void onPlaceDetailsLoaded(PlaceInfoMaster loginMaster);

    void onFetchingDistance(DistanceElement s);
}
