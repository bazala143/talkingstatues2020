
package com.talkingstatues.ui.activities.calling;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.talkingstatues.R;
import com.talkingstatues.databinding.ActivityCallingBinding;
import com.talkingstatues.model.statueList.StatueDetails;
import com.talkingstatues.networking.APIUrl;
import com.talkingstatues.ui.base.BaseActivity;
import com.talkingstatues.utilities.AppConstants;
import com.talkingstatues.utilities.CommonUtils;

import java.util.Objects;

import javax.inject.Inject;

public class CallingActivity extends BaseActivity implements View.OnClickListener, CallingMvpView, View.OnTouchListener,
        MediaPlayer.OnCompletionListener, MediaPlayer.OnBufferingUpdateListener {

    ActivityCallingBinding binding;

    CallingMvpPresenter<CallingMvpView> mPresenter;
    private static final String TAG = CallingActivity.class.getSimpleName();
    StatueDetails statueDetails;
    private MediaPlayer mediaPlayer;
    private  MediaPlayer ringTonePlayer;
    private  String audioUrl;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_calling);
        binding.setClickListener(this);
        //   mPresenter.onAttach(this);

        mPresenter = new CallingMvpPresenter<CallingMvpView>() {
            @Override
            public void onAttach(CallingMvpView mvpView) {

            }

            @Override
            public void onDetach() {

            }
        };
        binding.toolbar.setClickListener(this);
        binding.toolbar.ivback.setVisibility(View.VISIBLE);
        binding.toolbar.tvTitle.setVisibility(View.VISIBLE);
        binding.toolbar.tvTitle.setText(R.string.text_calling);
        Intent i = getIntent();
        statueDetails = (StatueDetails) Objects.requireNonNull(i.getExtras()).get(AppConstants.Object);
        Log.e("LanguageCode",CommonUtils.LanguageCode);
        if(CommonUtils.LanguageCode.equals(AppConstants.LANGUAGE_ENGLISH)){
            assert statueDetails != null;
            audioUrl = APIUrl.AUDIO_URL+ (statueDetails.getAudioUrl());

        }else{
            assert statueDetails != null;
            if(statueDetails.getSpanishAudioUrl() != null){
               audioUrl = APIUrl.AUDIO_URL+ (statueDetails.getSpanishAudioUrl());
            }else{
                audioUrl = APIUrl.AUDIO_URL+ (statueDetails.getAudioUrl());
            }
        }
        Log.e("audioUrl",audioUrl);

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.ic_place_holder);
        Glide.with(binding.getRoot().getContext())
                .applyDefaultRequestOptions(requestOptions)
                .load(APIUrl.IMAGE_URL + statueDetails.getImage())
                .into(binding.ivImage);
        initializeMedia();
        ringTonePlayer = MediaPlayer.create(this,
                Settings.System.DEFAULT_RINGTONE_URI);
        if(ringTonePlayer != null){
            ringTonePlayer.start();
        }

    }

    private void initializeMedia() {
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setOnBufferingUpdateListener(this);
        mediaPlayer.setOnCompletionListener(this);
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        mediaPlayer.stop();
        if(ringTonePlayer != null){
            ringTonePlayer.stop();
        }
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mediaPlayer.stop();
        if(ringTonePlayer != null){
            ringTonePlayer.stop();
        }

    }

    @Override
    public void onClick(View v) {
        if (v == binding.ivAccept) {
            if(ringTonePlayer != null){
                ringTonePlayer.stop();
            }
            binding.progressBar.setVisibility(View.VISIBLE);
            binding.linAccept.setVisibility(View.INVISIBLE);
            binding.linDecline.setVisibility(View.INVISIBLE);
            binding.linStop.setVisibility(View.VISIBLE);
            binding.progressBar.setVisibility(View.INVISIBLE);

            playAudio();
        } else if (v == binding.ivDecline) {
            finish();
        } else if (v == binding.toolbar.ivback) {
            finish();
        } else if(v == binding.linStop){
            finish();
        }

    }

    private void playAudio() {
        try {
            mediaPlayer.setDataSource(audioUrl);
            mediaPlayer.prepare();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!mediaPlayer.isPlaying()) {
            mediaPlayer.start();
        } else {
            mediaPlayer.pause();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(ringTonePlayer != null){
            ringTonePlayer.stop();
        }
        mediaPlayer.stop();
        this.finish();
    }

    @Override
    protected void onStop() {
        if(ringTonePlayer != null){
            ringTonePlayer.stop();
        }
        mediaPlayer.stop();
        super.onStop();
    }

    @Override
    public void onBufferingUpdate(MediaPlayer mp, int percent) {

    }

    @Override
    public void onCompletion(MediaPlayer mp) {

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return false;
    }
}
