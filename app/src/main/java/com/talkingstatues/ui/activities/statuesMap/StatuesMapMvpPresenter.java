/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.talkingstatues.ui.activities.statuesMap;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.model.LatLng;
import com.talkingstatues.di.PerActivity;
import com.talkingstatues.model.statueList.StatueDetails;
import com.talkingstatues.ui.base.MvpPresenter;
import com.talkingstatues.ui.fragments.CMapFragment;

import java.util.List;

/**
 * Created by Paras Andani on 27/01/17.
 */

@PerActivity
public interface StatuesMapMvpPresenter<V extends StatuesMapMvpView> extends MvpPresenter<V> {



    Bitmap createCustomMarker(Context context,Bitmap bitmap,int markerType);
    void  showRequiredPermissionDialog(Activity activity);
    void  getStatueListApi(String cityId);
    void  visitStatueApi(StatueDetails statueDetails);
    void  getDistanceApi(LatLng orgLatLng,LatLng destLatLng);
    List<LatLng>  getPolyline(LatLng sorceLatLong, LatLng destLatLong);
    void  getStatueDetailsApi(String qrCode);
    void  getStatueDetailsFromPrefs(String qrCode);

    String getLanguageCode();
}
