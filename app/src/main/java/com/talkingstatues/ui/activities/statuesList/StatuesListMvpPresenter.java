package com.talkingstatues.ui.activities.statuesList;

import com.talkingstatues.di.PerActivity;
import com.talkingstatues.model.login.LoginDetails;
import com.talkingstatues.model.statueList.StatueDetails;
import com.talkingstatues.model.statueList.StatueMaster;
import com.talkingstatues.ui.base.MvpPresenter;

import java.util.List;

@PerActivity
public interface StatuesListMvpPresenter<V extends StatuesListMvpView> extends MvpPresenter<V> {


    LoginDetails getLoginDetails();
    void  getCityList();
    void  getStatueListApi(String cityId);

    void  getDistanceApi(StatueMaster statueMaster);


    String getLanguageCode();
}
