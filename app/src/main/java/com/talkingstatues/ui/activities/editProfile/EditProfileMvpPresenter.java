package com.talkingstatues.ui.activities.editProfile;

import android.app.Activity;
import android.widget.EditText;

import com.google.gson.JsonObject;
import com.harmis.imagepicker.CameraUtils.CameraIntentHelper;
import com.talkingstatues.databinding.ActivityEditProfileBinding;
import com.talkingstatues.di.PerActivity;
import com.talkingstatues.ui.base.MvpPresenter;
import com.talkingstatues.ui.base.MvpView;

@PerActivity
public interface EditProfileMvpPresenter<V extends MvpView> extends MvpPresenter<V> {
    void onViewPrepared();
    boolean isValid(ActivityEditProfileBinding binding);
    void updateProfileApi(ActivityEditProfileBinding binding);
}
