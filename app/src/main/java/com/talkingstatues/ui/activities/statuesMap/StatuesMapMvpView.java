/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.talkingstatues.ui.activities.statuesMap;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.talkingstatues.model.distance.DistanceElement;
import com.talkingstatues.model.statueList.StatueDetails;
import com.talkingstatues.ui.base.MvpView;
import com.talkingstatues.ui.fragments.CMapFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Paras Andani on 27/01/17.
 */

public interface StatuesMapMvpView extends MvpView {
    void onStatueListLoaded(List<StatueDetails> json);
   void  onStatueVisited(StatueDetails statueDetails);


    void onLoadedPolyline(List<LatLng> routelist);

    void onFetchingDistance(DistanceElement s);

    void openStatueLInk(String qrCode);
}
