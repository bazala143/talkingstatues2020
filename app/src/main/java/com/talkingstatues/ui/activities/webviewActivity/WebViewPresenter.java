package com.talkingstatues.ui.activities.webviewActivity;

import com.talkingstatues.data.prefs.AppPreferencesHelper;
import com.talkingstatues.networking.NetworkService;
import com.talkingstatues.ui.base.BasePresenter;

import javax.inject.Inject;

import rx.subscriptions.CompositeSubscription;

public class WebViewPresenter<V extends WebViewMvpView> extends BasePresenter<V>
        implements WebViewMvpPresenter<V> {
    @Inject
    NetworkService service;
    private static final String TAG = WebViewPresenter.class.getSimpleName();

    @Inject
    public WebViewPresenter(AppPreferencesHelper preferencesHelper, CompositeSubscription mSubscription) {
        super( preferencesHelper, mSubscription );
    }

    @Override
    public void onDetach() {
        if (getSubscription() != null && getSubscription().hasSubscriptions()) {
            getSubscription().clear();
        }
    }
}
