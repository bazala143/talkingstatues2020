package com.talkingstatues.ui.activities.aboutUs;

import android.util.Log;
import android.widget.EditText;

import androidx.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.talkingstatues.R;
import com.talkingstatues.data.prefs.AppPreferencesHelper;
import com.talkingstatues.model.aboutUs.AboutUsMaster;
import com.talkingstatues.model.login.LoginDetails;
import com.talkingstatues.model.login.LoginMaster;
import com.talkingstatues.networking.APIUrl;
import com.talkingstatues.networking.API_Params;
import com.talkingstatues.networking.NetworkError;
import com.talkingstatues.networking.NetworkService;
import com.talkingstatues.ui.activities.placeDetailsActivity.PlaceDetailsMvpPresenter;
import com.talkingstatues.ui.activities.placeDetailsActivity.PlaceDetailsMvpView;
import com.talkingstatues.ui.base.BasePresenter;
import com.talkingstatues.utilities.AppConstants;
import com.talkingstatues.utilities.CommonUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.inject.Inject;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

public class AboutUsPresenter<V extends AboutUsMvpView> extends BasePresenter<V>
        implements AboutUsMvpPresenter<V> {
    @Inject
    NetworkService service;
    private static final String TAG = AboutUsPresenter.class.getSimpleName();

    private DatabaseReference mFirebaseDatabase;
    private FirebaseDatabase mFirebaseInstance;

    @Inject
    public AboutUsPresenter(AppPreferencesHelper preferencesHelper, CompositeSubscription mSubscription) {
        super(preferencesHelper, mSubscription);
    }

    @Override
    public boolean isValid(EditText edMobile, EditText edPassword) {
        if (CommonUtils.isEmpty(edMobile.getText().toString())) {
            edMobile.setError(getMvpView().getStringFromId(R.string.text_mobile_number));
            edMobile.requestFocus();
            return false;
        } else if (CommonUtils.isEmpty(edPassword.getText().toString())) {
            edPassword.setError(getMvpView().getStringFromId(R.string.text_password));
            edPassword.requestFocus();
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void getAbousUsApi() {
        if (getMvpView().isNetworkConnected()) {
            getMvpView().showLoading();
            Subscription subscription = service.getAbousUs(new NetworkService.GetAboutUsCallback() {
                @Override
                public void onSuccess(AboutUsMaster loginMaster) {
                    getMvpView().hideLoading();
                    if (loginMaster != null) {
                        CommonUtils.pLog(TAG, "Response : " + new Gson().toJson(loginMaster).toString());
                        if(loginMaster.getCode().equals("0")){
                            getMvpView().onAbousLoaded(loginMaster.getJson());
                        }else{
                            getMvpView().showMessage(loginMaster.getMessage());
                        }

                    }
                }

                @Override
                public void onError(NetworkError networkError) {
                    getMvpView().hideLoading();
                    getMvpView().onError(networkError.getMessage() + "");
                    getMvpView().onGetLogs(APIUrl.login, networkError.getMessage().toString() + "");
                }
            });
            getSubscription().add(subscription);
        } else {
            getMvpView().onError(R.string.utils__no_connection);
            getMvpView().onGetLogs(APIUrl.login, "NO INTERNET");
        }
    }

    @Override
    public String getLanguageCode() {
        return getPreferencesHelper().getLanguageCode();
    }


    @Override
    public void onDetach() {
        if (getSubscription() != null && getSubscription().hasSubscriptions()) {
            getSubscription().clear();
        }
    }



}
