
package com.talkingstatues.ui.activities.faqActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.talkingstatues.R;
import com.talkingstatues.databinding.ActivityFaqBinding;
import com.talkingstatues.model.banner.BannerDetails;
import com.talkingstatues.model.faqList.FaqDetails;
import com.talkingstatues.model.login.LoginDetails;
import com.talkingstatues.ui.base.BaseActivity;
import com.talkingstatues.utilities.AppConstants;
import com.talkingstatues.utilities.RequestCodes;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class FaqActivity extends BaseActivity implements View.OnClickListener, FaqMvpView {

    ActivityFaqBinding binding;
    @Inject
    FaqMvpPresenter<FaqMvpView> mPresenter;
    LoginDetails loginDetails;
    private static final String TAG = FaqActivity.class.getSimpleName();
    private Activity activity;
    String languageCode = AppConstants.LANGUAGE_ENGLISH;

    @Inject
    FaqAdapter faqAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivityComponent().inject(this);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_faq);
        binding.setClickListener(this);
        setLoginDetails(mPresenter.getLoginDetails());
        mPresenter.onAttach(this);
        activity = FaqActivity.this;
        binding.toolbar.setClickListener(this);
        binding.toolbar.ivback.setVisibility(View.VISIBLE);
        binding.toolbar.tvTitle.setVisibility(View.VISIBLE);
        binding.toolbar.tvTitle.setText(R.string.text_faq);
        languageCode = mPresenter.getLanguageCode();
        mPresenter.getFaqsApi();


    }


    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        if (v == binding.toolbar.ivback) {
            finish();
        }
    }

    public void setLoginDetails(LoginDetails loginDetails) {
        this.loginDetails = loginDetails;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RequestCodes.REQUEST_MAIN_ACTIVITY && resultCode == RESULT_OK) {
            setResult(RESULT_OK);
            finish();
        }
    }

    @Override
    public void onFaqLoaded(List<FaqDetails> json) {
        faqAdapter = new FaqAdapter(activity, json);
        faqAdapter.setLangugeCode(languageCode);
        binding.rvStatuesList.setHasFixedSize(true);
        binding.rvStatuesList.setLayoutManager(new LinearLayoutManager(this));
        binding.rvStatuesList.setAdapter(faqAdapter);
    }
}
