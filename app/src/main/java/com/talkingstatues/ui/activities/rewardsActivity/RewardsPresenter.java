package com.talkingstatues.ui.activities.rewardsActivity;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.talkingstatues.R;
import com.talkingstatues.data.prefs.AppPreferencesHelper;
import com.talkingstatues.model.city.CityMaster;
import com.talkingstatues.model.login.LoginDetails;
import com.talkingstatues.model.redeem.RedeemMaster;
import com.talkingstatues.model.statueList.StatueMaster;
import com.talkingstatues.networking.API_Params;
import com.talkingstatues.networking.NetworkError;
import com.talkingstatues.networking.NetworkService;
import com.talkingstatues.ui.base.BasePresenter;
import com.talkingstatues.utilities.AppConstants;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import javax.inject.Inject;
import javax.security.auth.login.LoginException;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

public class RewardsPresenter<V extends RewardsMvpView> extends BasePresenter<V>
        implements RewardsMvpPresenter<V> {
    @Inject
    NetworkService service;
    private static final String TAG = RewardsPresenter.class.getSimpleName();

    @Inject
    public RewardsPresenter(AppPreferencesHelper preferencesHelper, CompositeSubscription mSubscription) {
        super(preferencesHelper, mSubscription);
    }


    @Override
    public LoginDetails getLoginDetails() {
        return getPreferencesHelper().getLoginModel();
    }

    @Override
    public void redeemReward(String cityId) {

        if (getMvpView().isNetworkConnected()) {
            getMvpView().showLoading();
            Subscription s = service.redeemReward(rewardParams(cityId), new NetworkService.GetRedeemRewardCallback() {
                @Override
                public void onSuccess(RedeemMaster master) {
                    getMvpView().hideLoading();
                    if (master.getCode().equals("0")) {
                        getMvpView().showMessage(R.string.reward_collected);
                        getMvpView().onRewardRedeemed(master);
                    } else {
                        getMvpView().showMessage(R.string.failed_to_collect_reward);
                    }

                }

                @Override
                public void onError(NetworkError networkError) {
                    getMvpView().showMessage(networkError.getMessage());
                }
            });
            getSubscription().add(s);
        } else {
            getMvpView().onError(R.string.utils__no_connection);
        }

    }

    @Override
    public void getStatueList(String cityId) {
        if (getMvpView().isNetworkConnected()) {
            getMvpView().showLoading();
            Subscription s = service.statueList(loginParams(cityId), new NetworkService.GetStatueListCallbackCallback() {
                @Override
                public void onSuccess(StatueMaster master) {
                    Log.e("response",new Gson().toJson(master));
                    getMvpView().hideLoading();
                    if (master.getCode().equals("0")) {
                        getMvpView().onStatueListLoaded(master.getJson());
                    } else {
                        getMvpView().showMessage(master.getMessage());
                    }
                }

                @Override
                public void onError(NetworkError networkError) {
                    getMvpView().hideLoading();
                    getMvpView().showMessage(R.string.oops);
                }
            });
            getSubscription().add(s);
        } else {
            getMvpView().onError(R.string.utils__no_connection);
        }
    }

    private JsonObject loginParams(String cityId) {
        JsonObject gsonObject = new JsonObject();
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(API_Params.user_id, getPreferencesHelper().getLoginModel().getId()+"");
            jsonObject.put(API_Params.city_id, cityId);
            JsonParser parser = new JsonParser();
            gsonObject = (JsonObject) parser.parse(jsonObject.toString());
            Log.e("parameter", jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return gsonObject;
    }


    private JsonObject rewardParams(String cityId) {
        JsonObject gsonObject = new JsonObject();
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(API_Params.user_id, getPreferencesHelper().getLoginModel().getId() + "");
            jsonObject.put(API_Params.city_id, cityId);
            JsonParser parser = new JsonParser();
            gsonObject = (JsonObject) parser.parse(jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return gsonObject;
    }

    @Override
    public void onDetach() {
        if (getSubscription() != null && getSubscription().hasSubscriptions()) {
            getSubscription().clear();
        }
    }
}
