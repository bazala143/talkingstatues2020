package com.talkingstatues.ui.activities.statuesList;

import com.talkingstatues.model.city.CityDetails;
import com.talkingstatues.model.statueList.StatueDetails;
import com.talkingstatues.ui.base.MvpView;

import java.util.List;

public interface StatuesListMvpView extends MvpView {

    void onCityListLoaded(List<CityDetails> json);

    void onStatueListLoaded(List<StatueDetails> json);

    void onFetchingDistance(List<StatueDetails> s);

}
