package com.talkingstatues.ui.activities.main;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.talkingstatues.R;
import com.talkingstatues.databinding.LayoutDrawerItemsBinding;
import com.talkingstatues.model.DrawerItem;
import com.talkingstatues.ui.base.BaseViewHolder;

import java.util.ArrayList;
import java.util.List;

public class DrawerAdapter extends RecyclerView.Adapter<BaseViewHolder>{

    private List<DrawerItem> mList = new ArrayList<>();
    private LayoutInflater layoutInflater;
    OnItemClickListener clickListener;



    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        LayoutDrawerItemsBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.layout_drawer_items, parent, false);
        return new ViewHolder(binding);

    }




    @Override
    public int getItemCount() {
        return mList.size();

    }

    public void addItems(List<DrawerItem> mList) {
        this.mList.addAll(mList);
        notifyDataSetChanged();
    }

    public void clearList() {
        if (this.mList != null && this.mList.size() > 0) {
            this.mList.clear();
        }
    }

    public void setCallback(OnItemClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public class ViewHolder extends BaseViewHolder {
        LayoutDrawerItemsBinding binding;


        public ViewHolder(final LayoutDrawerItemsBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        @Override
        protected void clear() {

        }

        public void onBind(final int position) {
            super.onBind(position);
              binding.setData(mList.get(position));
            //  binding.ivBanner.setImageResource(mList.get(position).getIcon());
              binding.getRoot().setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View view) {
                      clickListener.onChatClick(mList.get(position));
                  }
              });

        }
    }



    public interface OnItemClickListener {
        void onChatClick(DrawerItem items);
    }
}
