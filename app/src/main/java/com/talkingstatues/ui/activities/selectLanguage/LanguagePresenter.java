package com.talkingstatues.ui.activities.selectLanguage;

import android.os.Handler;

import com.talkingstatues.data.prefs.AppPreferencesHelper;
import com.talkingstatues.model.login.LoginDetails;
import com.talkingstatues.networking.NetworkService;
import com.talkingstatues.ui.base.BasePresenter;
import com.talkingstatues.utilities.CommonUtils;
import com.talkingstatues.utilities.LocaleHelper;

import javax.inject.Inject;

import rx.subscriptions.CompositeSubscription;

public class LanguagePresenter<V extends LanguageMvpView> extends BasePresenter<V>
        implements LanguageMvpPresenter<V> {
    @Inject
    NetworkService service;
    LoginDetails storedLoginDetails;

    @Inject
    public LanguagePresenter(AppPreferencesHelper preferencesHelper, CompositeSubscription mSubscription) {
        super(preferencesHelper, mSubscription);
        storedLoginDetails = getPreferencesHelper().getLoginModel();
    }

    @Override
    public void onNextActivity() {
        if (getPreferencesHelper().getLoginModel() != null) {
            getMvpView().openHomeActivity();
        } else {
            getMvpView().openLoginActivity();
        }
    }


    @Override
    public void setLanguageCode(String language) {
        getPreferencesHelper().setLanguageCode(language);

    }

    @Override
    public String getLanguageCode() {
        return getPreferencesHelper().getLanguageCode();
    }


}
