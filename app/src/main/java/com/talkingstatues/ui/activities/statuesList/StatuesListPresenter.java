package com.talkingstatues.ui.activities.statuesList;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.talkingstatues.R;
import com.talkingstatues.data.prefs.AppPreferencesHelper;
import com.talkingstatues.model.city.CityMaster;
import com.talkingstatues.model.distance.DistanceDetails;
import com.talkingstatues.model.distance.DistanceDuration;
import com.talkingstatues.model.distance.DistanceElement;
import com.talkingstatues.model.distance.DistanceRow;
import com.talkingstatues.model.login.LoginDetails;
import com.talkingstatues.model.statueList.StatueDetails;
import com.talkingstatues.model.statueList.StatueMaster;
import com.talkingstatues.networking.APIUrl;
import com.talkingstatues.networking.API_Params;
import com.talkingstatues.networking.NetworkError;
import com.talkingstatues.networking.NetworkService;
import com.talkingstatues.ui.base.BasePresenter;
import com.talkingstatues.utilities.AppConstants;
import com.talkingstatues.utilities.CommonUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.inject.Inject;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

public class StatuesListPresenter<V extends StatuesListMvpView> extends BasePresenter<V>
        implements StatuesListMvpPresenter<V> {
    @Inject
    NetworkService service;
    private static final String TAG = StatuesListPresenter.class.getSimpleName();
    public GetStatyeDistanceAsync getStatyeDistanceAsync;

    @Inject
    public StatuesListPresenter(AppPreferencesHelper preferencesHelper, CompositeSubscription mSubscription) {
        super(preferencesHelper, mSubscription);
    }


    @Override
    public LoginDetails getLoginDetails() {
        return getPreferencesHelper().getLoginModel();
    }

    @Override
    public void getCityList() {
        if (getMvpView().isNetworkConnected()) {
            getMvpView().showLoading();
            Subscription s = service.getCityList(new NetworkService.GetCityListCallback() {
                @Override
                public void onSuccess(CityMaster master) {
                    getMvpView().hideLoading();
                    if (master.getCode().equals("0")) {
                        getMvpView().onCityListLoaded(master.getJson());
                    } else {
                        getMvpView().showMessage(master.getMessage());
                    }

                }

                @Override
                public void onError(NetworkError networkError) {
                    getMvpView().showMessage(networkError.getMessage());
                }
            });
            getSubscription().add(s);
        } else {
            getMvpView().onError(R.string.utils__no_connection);
        }
    }

    @Override
    public void getStatueListApi(String cityId) {
        if (getStatyeDistanceAsync != null) {
            getStatyeDistanceAsync.cancel(true);
        }

        if (getMvpView().isNetworkConnected()) {
            getMvpView().showLoading();
            Subscription s = service.statueList(loginParams(cityId), new NetworkService.GetStatueListCallbackCallback() {
                @Override
                public void onSuccess(StatueMaster master) {
                    getMvpView().hideLoading();
                    if (master.getCode().equals("0")) {
                        getMvpView().onStatueListLoaded(master.getJson());
                        getDistanceApi(master);
                    } else {
                        getMvpView().showMessage(master.getMessage());
                    }

                }

                @Override
                public void onError(NetworkError networkError) {
                    getMvpView().hideLoading();
                    getMvpView().showMessage(R.string.oops);
                }
            });
            getSubscription().add(s);
        } else {
            getMvpView().onError(R.string.utils__no_connection);
        }
    }

    @SuppressLint("StaticFieldLeak")
    @Override
    public void getDistanceApi(final StatueMaster statueMaster) {

        getStatyeDistanceAsync = new GetStatyeDistanceAsync(statueMaster);
        getStatyeDistanceAsync.execute();

    }

    @Override
    public String getLanguageCode() {
        return getPreferencesHelper().getLanguageCode();
    }


    @SuppressLint("StaticFieldLeak")
    public class GetStatyeDistanceAsync extends AsyncTask<String, String, List<StatueDetails>> {

        StatueMaster statueMaster;

        public GetStatyeDistanceAsync(StatueMaster statueMaster) {
            this.statueMaster = statueMaster;
        }

        final List<StatueDetails> sttaueList = new ArrayList<>();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected List<StatueDetails> doInBackground(String... strings) {
            for (StatueDetails restaurant : statueMaster.getJson()) {
                String latitude = restaurant.getLatitude() + "";
                String longitude = restaurant.getLongitude() + "";
                try {
                    DistanceElement distanceElement = null;
                    HttpURLConnection connection = null;
                    StringBuilder jsonResults = new StringBuilder();
                    try {
                        //String latitude = objects[0];
                        //String longitude = objects[1];
                      //  CommonUtils.pLog(TAG, getDistanceUrl(latitude, longitude));
                        URL url = new URL(getDistanceUrl(latitude, longitude));
                        connection = (HttpURLConnection) url.openConnection();

                     //   CommonUtils.pLog(TAG, "Response code : " + connection.getResponseCode());
                        InputStreamReader in = new InputStreamReader(connection.getInputStream());

                        // Load the results into a StringBuilder
                        int read;
                        char[] buff = new char[1024];
                        while ((read = in.read(buff)) != -1) {
                            jsonResults.append(buff, 0, read);
                        }

                        try {
                            JSONObject jsonMain = new JSONObject(jsonResults.toString());
                        //    CommonUtils.pLog(TAG, "json : " + jsonMain.toString());
                            String status = jsonMain.optString(API_Params.status);
                            if (!CommonUtils.isEmpty(status) && CommonUtils.equals(status, AppConstants.STATUS_OK)) {
                                List<String> destinationAddresses = new ArrayList<>();
                                JSONArray destinationAddressArray = jsonMain.getJSONArray(API_Params.destination_addresses);
                                if (destinationAddressArray != null && destinationAddressArray.length() > 0) {
                                    destinationAddresses.add(destinationAddressArray.getString(0));
                                }

                                List<String> originAddresses = new ArrayList<>();
                                JSONArray originAddressArray = jsonMain.getJSONArray(API_Params.origin_addresses);
                                if (originAddressArray != null && originAddressArray.length() > 0) {
                                    originAddresses.add(originAddressArray.getString(0));
                                }

                                List<DistanceRow> distanceRows = new ArrayList<>();
                                JSONArray rowsArray = jsonMain.getJSONArray(API_Params.rows);
                                if (rowsArray != null && rowsArray.length() > 0) {
                                    List<DistanceElement> distanceElements = new ArrayList<>();
                                    JSONObject jsonObject = rowsArray.getJSONObject(0);
                                    JSONArray elementsArray = jsonObject.getJSONArray(API_Params.elements);
                                    if (elementsArray != null && elementsArray.length() > 0) {
                                        JSONObject elementObject = elementsArray.getJSONObject(0);
                                        if (elementObject != null && CommonUtils.equals(elementObject.optString(API_Params.status), AppConstants.STATUS_OK)) {
                                            distanceElement = new DistanceElement();
                                            JSONObject distanceObject = elementObject.getJSONObject(API_Params.distance);
                                            if (distanceObject != null) {
                                                DistanceDetails distanceDetails = new DistanceDetails();
                                                distanceDetails.setText(distanceObject.optString(API_Params.text));
                                                distanceDetails.setValue(distanceObject.optInt(API_Params.value));
                                                distanceElement.setDistance(distanceDetails);
                                            }

                                            JSONObject durationObject = elementObject.getJSONObject(API_Params.duration);
                                            if (durationObject != null) {
                                                DistanceDuration duration = new DistanceDuration();
                                                duration.setText(durationObject.optString(API_Params.text));
                                                duration.setValue(durationObject.optInt(API_Params.value));
                                                distanceElement.setDuration(duration);
                                            }

                                        }
                                    }
                                }
                            }
                        } catch (JSONException e) {
                          //  CommonUtils.pLog(TAG, "Error : " + e.getMessage());
                            //CommonUtils.printLog(LOG_TAG, "Cannot process JSON results", e);
                        }
                    } catch (Exception e) {
                     //   CommonUtils.pLog(TAG, "Error line 235: " + e.getMessage());
                    } finally {
                        if (connection != null) {
                            connection.disconnect();
                        }
                    }

                    CommonUtils.pLog(TAG, new Gson().toJson(distanceElement));
                    if (distanceElement != null) {
                        restaurant.setDistanceElement(distanceElement);
                    }
                } catch (Exception e) {
                 //   CommonUtils.pLog(TAG, "Distance matrix error: " + e.getMessage());
                    e.printStackTrace();
                }
                sttaueList.add(restaurant);
            }
            return sttaueList;
        }

        @Override
        protected void onPostExecute(List<StatueDetails> s) {
            super.onPostExecute(s);
            getMvpView().onFetchingDistance(s);

        }
    }


    private JsonObject loginParams(String cityId) {
        JsonObject gsonObject = new JsonObject();
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(API_Params.user_id, getPreferencesHelper().getLoginModel().getId() + "");
            jsonObject.put(API_Params.city_id, cityId);
            JsonParser parser = new JsonParser();
            gsonObject = (JsonObject) parser.parse(jsonObject.toString());
            Log.e("parameter", jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return gsonObject;
    }

    @Override
    public void onDetach() {
        if (getSubscription() != null && getSubscription().hasSubscriptions()) {
            getSubscription().clear();
        }
    }

    private String getDistanceUrl(String destLat, String destLong) {
        //40.727578, -73.990780
        String url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" +
                getPreferencesHelper().getCurrentLatitude() + "," + getPreferencesHelper().getCurrentLongitude() +
                "&destinations=" + destLat + "," + destLong +
                "&key=" + APIUrl.googleApiKey;
        return url;
    }
}
