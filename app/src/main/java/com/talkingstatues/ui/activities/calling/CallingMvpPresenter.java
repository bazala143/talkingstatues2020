package com.talkingstatues.ui.activities.calling;

import com.talkingstatues.di.PerActivity;
import com.talkingstatues.ui.base.MvpPresenter;

@PerActivity
public interface CallingMvpPresenter<V extends CallingMvpView> extends MvpPresenter<V> {

}
