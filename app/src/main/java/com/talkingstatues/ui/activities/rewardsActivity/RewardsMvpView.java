package com.talkingstatues.ui.activities.rewardsActivity;

import com.talkingstatues.model.city.CityDetails;
import com.talkingstatues.model.redeem.RedeemMaster;
import com.talkingstatues.model.statueList.StatueDetails;
import com.talkingstatues.ui.base.MvpView;

import java.util.List;

public interface RewardsMvpView extends MvpView {

    void onRewardRedeemed(RedeemMaster redeemMaster);

    void onStatueListLoaded(List<StatueDetails> json);
}
