package com.talkingstatues.ui.activities.selectLanguage;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;
import androidx.databinding.DataBindingUtil;
import com.talkingstatues.R;
import com.talkingstatues.databinding.ActivityLanguageBinding;
import com.talkingstatues.ui.activities.login.LoginActivity;
import com.talkingstatues.ui.activities.main.MainActvity;
import com.talkingstatues.ui.base.BaseActivity;
import com.talkingstatues.utilities.AppConstants;
import com.talkingstatues.utilities.CommonUtils;
import com.talkingstatues.utilities.LocaleHelper;

import javax.inject.Inject;


public class LanguageActivity extends BaseActivity implements LanguageMvpView,View.OnClickListener {

    ActivityLanguageBinding binding;
    @Inject
    LanguageMvpPresenter<LanguageMvpView> mPresenter;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        getActivityComponent().inject(this);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_language);
        mPresenter.onAttach(this);
        binding.setClickListener(this);
    }





    @Override
    public void openHomeActivity() {
        Intent intent = new Intent(LanguageActivity.this, MainActvity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void openLoginActivity() {
        Intent i = new Intent(LanguageActivity.this, LoginActivity.class);
        startActivity(i);
        finish();
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }


    @Override
    public void onClick(View v) {
        if(v == binding.linEnglish){
            binding.linEnglish.setBackground(getDrawable(R.drawable.bg_dark_blue_square));
            binding.linSpain.setBackground(getDrawable(R.drawable.bg_white_border));
            binding.tvEnglish.setTextColor(getResources().getColor(R.color.black));
            binding.tvSpanish.setTextColor(getResources().getColor(R.color.white));
            mPresenter.setLanguageCode(AppConstants.LANGUAGE_ENGLISH);
            CommonUtils.LanguageCode = AppConstants.LANGUAGE_ENGLISH;
            LocaleHelper.setLocale(this,AppConstants.LANGUAGE_ENGLISH);
            mPresenter.onNextActivity();

        }else if(v == binding.linSpain){
            binding.linSpain.setBackground(getDrawable(R.drawable.bg_dark_blue_square));
            binding.linEnglish.setBackground(getDrawable(R.drawable.bg_white_border));
            binding.tvEnglish.setTextColor(getResources().getColor(R.color.white));
            binding.tvSpanish.setTextColor(getResources().getColor(R.color.black));
            mPresenter.setLanguageCode(AppConstants.LANGUAGE_SPANISH);
            CommonUtils.LanguageCode = AppConstants.LANGUAGE_SPANISH;
            LocaleHelper.setLocale(this,AppConstants.LANGUAGE_SPANISH);
            mPresenter.onNextActivity();
        }else{
           mPresenter.onNextActivity();
        }

    }
}
