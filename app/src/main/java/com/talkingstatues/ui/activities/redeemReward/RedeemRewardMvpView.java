package com.talkingstatues.ui.activities.redeemReward;

import com.talkingstatues.model.getReward.GetRewardMaster;
import com.talkingstatues.model.redeem.RedeemMaster;
import com.talkingstatues.ui.base.MvpView;

public interface RedeemRewardMvpView extends MvpView {

    void onRewardRedeemed(RedeemMaster master);

    void onRewardDetails(GetRewardMaster master);
}
