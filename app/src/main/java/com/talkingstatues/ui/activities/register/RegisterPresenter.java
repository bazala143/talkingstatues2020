package com.talkingstatues.ui.activities.register;

import android.util.Log;
import android.widget.EditText;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.talkingstatues.R;
import com.talkingstatues.data.prefs.AppPreferencesHelper;
import com.talkingstatues.databinding.ActivityRegisterBinding;
import com.talkingstatues.model.login.LoginDetails;
import com.talkingstatues.model.login.LoginMaster;
import com.talkingstatues.networking.APIUrl;
import com.talkingstatues.networking.API_Params;
import com.talkingstatues.networking.NetworkError;
import com.talkingstatues.networking.NetworkService;
import com.talkingstatues.ui.base.BasePresenter;
import com.talkingstatues.utilities.AppConstants;
import com.talkingstatues.utilities.CommonUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.inject.Inject;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

public class RegisterPresenter<V extends RegisterMvpView> extends BasePresenter<V>
        implements RegisterMvpPresenter<V> {
    @Inject
    NetworkService service;

    private static final String TAG = RegisterPresenter.class.getSimpleName();


    @Inject
    public RegisterPresenter(AppPreferencesHelper preferencesHelper, CompositeSubscription mSubscription) {
        super(preferencesHelper, mSubscription);
    }


    @Override
    public boolean isValid(ActivityRegisterBinding binding) {
        if (binding.edName.getText().toString().isEmpty()) {
            getMvpView().showMessage(R.string.please_enter_name);
            return false;
        } else if (binding.edAge.getText().toString().isEmpty()) {
            getMvpView().showMessage(R.string.please_enter_age);
            return false;
        } else if (binding.edCity.getText().toString().isEmpty()) {
            getMvpView().showMessage(R.string.please_enter_city);
            return false;
        } else if (binding.edMobileNumber.getText().toString().isEmpty()) {
            getMvpView().showMessage(R.string.please_enter_mobile);
            return false;
        } else if (binding.edEmail.getText().toString().isEmpty()) {
            getMvpView().showMessage(R.string.please_enter_email);
            return false;
        } else if (!CommonUtils.checkEmail(binding.edEmail.getText().toString().trim())) {
            getMvpView().showMessage(R.string.please_enter_valid_email);
            return false;
        } else if (binding.edPassword.getText().toString().isEmpty()) {
            getMvpView().showMessage(R.string.please_enter_password);
            return false;
        } else if (binding.edFbAccount.getText().toString().isEmpty()) {
            getMvpView().showMessage(R.string.please_enter_fbemail);
            return false;
        } else if (!CommonUtils.checkEmail(binding.edFbAccount.getText().toString().trim())) {
            getMvpView().showMessage(R.string.please_enter_valid_fbemail);
            return false;
        } else if (binding.edGoogle.getText().toString().isEmpty()) {
            getMvpView().showMessage(R.string.please_enter_googleid);
            return false;
        } else if (!CommonUtils.checkEmail(binding.edGoogle.getText().toString().trim())) {
            getMvpView().showMessage(R.string.please_enter_valid_goemail);
            return false;
        }
        return true;
    }

    @Override
    public void registerApi(ActivityRegisterBinding binding) {
        if (getMvpView().isNetworkConnected()) {
            getMvpView().showLoading();
            Subscription subscription = service.register(loginParams(binding), new NetworkService.GetLoginCallback() {
                @Override
                public void onSuccess(LoginMaster loginMaster) {
                    getMvpView().hideLoading();
                    if (loginMaster != null) {
                        CommonUtils.pLog(TAG, "Response : " + new Gson().toJson(loginMaster).toString());
                        if (loginMaster.getCode().equals("0")) {
                            getMvpView().showMessage(R.string.register_success);
                            getPreferencesHelper().setLoginDetails(new Gson().toJson(loginMaster.getLoginDetails()));
                            getMvpView().onRegisterSuccess(loginMaster);
                        } else {
                            getMvpView().onError(R.string.registration_failed);
                        }
                    }
                }

                @Override
                public void onError(NetworkError networkError) {
                    getMvpView().hideLoading();
                    getMvpView().onError(networkError.getMessage() + "");
                    getMvpView().onGetLogs(APIUrl.login, networkError.getMessage().toString() + "");
                }
            });
            getSubscription().add(subscription);
        } else {
            getMvpView().onError(R.string.utils__no_connection);
            getMvpView().onGetLogs(APIUrl.login, "NO INTERNET");
        }
    }

    private JsonObject loginParams(ActivityRegisterBinding binding) {
        JsonObject gsonObject = new JsonObject();
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(API_Params.name, binding.edName.getText().toString().trim());
            jsonObject.put(API_Params.mobile_number, binding.edMobileNumber.getText().toString().trim());
            jsonObject.put(API_Params.email, binding.edEmail.getText().toString().trim());
            jsonObject.put(API_Params.password, binding.edPassword.getText().toString().trim());
            jsonObject.put(API_Params.age, binding.edAge.getText().toString().trim());
            jsonObject.put(API_Params.city, binding.edCity.getText().toString().trim());
            jsonObject.put(API_Params.facebook, binding.edFbAccount.getText().toString().trim());
            jsonObject.put(API_Params.google, binding.edGoogle.getText().toString().trim());
            jsonObject.put(API_Params.gmail, binding.edGoogle.getText().toString().trim());
            JsonParser parser = new JsonParser();
            gsonObject = (JsonObject) parser.parse(jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return gsonObject;
    }

    @Override
    public void onDetach() {
        if (getSubscription() != null && getSubscription().hasSubscriptions()) {
            getSubscription().clear();
        }
    }


}
