package com.talkingstatues.ui.activities.splash;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.talkingstatues.R;
import com.talkingstatues.data.prefs.AppPreferencesHelper;
import com.talkingstatues.model.login.LoginDetails;
import com.talkingstatues.model.login.LoginMaster;
import com.talkingstatues.networking.API_Params;
import com.talkingstatues.networking.NetworkError;
import com.talkingstatues.networking.NetworkService;
import com.talkingstatues.ui.base.BasePresenter;
import com.talkingstatues.utilities.AppConstants;
import com.talkingstatues.utilities.CommonUtils;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;

import javax.inject.Inject;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

public class SplashPresenter<V extends SplashMvpView> extends BasePresenter<V>
        implements SplashMvpPresenter<V> {
    @Inject
    NetworkService service;
    Handler handler;
    private static final int SPLASH_DURATION = 2000;
    private static final String TAG = SplashPresenter.class.getSimpleName();

    LoginDetails storedLoginDetails;

    @Inject
    public SplashPresenter(AppPreferencesHelper preferencesHelper, CompositeSubscription mSubscription) {
        super(preferencesHelper, mSubscription);
        storedLoginDetails = getPreferencesHelper().getLoginModel();
    }

    @Override
    public void onNextActivity() {
        CommonUtils.pLog(TAG, getPreferencesHelper().getLoginModel() + " Login Model");
        handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(getPreferencesHelper().getLanguageCode() == null){
                    getMvpView().openLanguageActivity();
                }else if (getPreferencesHelper().getLoginModel() != null) {
                   getMvpView().openHomeActivity();
                } else {
                    getMvpView().openLoginActivity();
                }

            }
        }, SPLASH_DURATION);
    }

    @Override
    public void storeLocation(String latitude, String longitude) {
        getPreferencesHelper().setCurrentLatitude(latitude);
        getPreferencesHelper().setCurrentLongitude(longitude);
    }

    @Override
    public void setLanguageCode(String language) {
        getPreferencesHelper().setLanguageCode(language);
    }

    @Override
    public String getLanguageCode() {
        return getPreferencesHelper().getLanguageCode();
    }



}
