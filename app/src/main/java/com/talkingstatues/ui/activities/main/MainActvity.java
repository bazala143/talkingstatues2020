
package com.talkingstatues.ui.activities.main;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.blikoon.qrcodescanner.QrCodeActivity;
import com.crashlytics.android.Crashlytics;
import com.fusedbulblib.GetCurrentLocation;
import com.fusedbulblib.interfaces.DialogClickListener;
import com.fusedbulblib.interfaces.GpsOnListener;
import com.harmis.imagepicker.CameraUtils.CameraIntentHelper;
import com.talkingstatues.R;
import com.talkingstatues.databinding.ActivityUserHomeBinding;
import com.talkingstatues.model.DrawerItem;
import com.talkingstatues.model.city.CityDetails;
import com.talkingstatues.model.login.LoginDetails;
import com.talkingstatues.model.logoDetails.LogoDetails;
import com.talkingstatues.model.statueList.StatueDetails;
import com.talkingstatues.ui.activities.aboutUs.AboutUsActivity;
import com.talkingstatues.ui.activities.calling.CallingActivity;
import com.talkingstatues.ui.activities.editProfile.EditProfileActivity;
import com.talkingstatues.ui.activities.faqActivity.FaqActivity;
import com.talkingstatues.ui.activities.login.LoginActivity;
import com.talkingstatues.ui.activities.webviewActivity.WebViewActivity;
import com.talkingstatues.ui.base.BaseActivity;
import com.talkingstatues.ui.dialog.CheckGPSDialog;
import com.talkingstatues.ui.dialog.PermissionDeniedDialog;
import com.talkingstatues.ui.fragments.main.MainFragment;
import com.talkingstatues.utilities.AppConstants;
import com.talkingstatues.utilities.CommonUtils;
import com.talkingstatues.utilities.LocaleHelper;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;

public class MainActvity extends BaseActivity implements View.OnClickListener, MainMvpView, GpsOnListener {

    ActivityUserHomeBinding binding;

    @Inject
    MainMvpPresenter<MainMvpView> mPresenter;

    @Inject
    DrawerAdapter drawerAdapter;

    @Inject
    LogosAdapter logosAdapter;

    GetCurrentLocation getCurrentLocation;
    LoginDetails loginDetails;
    private static final String TAG = MainActvity.class.getSimpleName();
    Location locationTest;
    CameraIntentHelper mCameraIntentHelper;
    String[] cameraPermissions = {Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
    int whichImage = 1;
    private final int REQUEST_PERMISSION_CAMERA = 1;
    private static final int REQUEST_CODE_QR_SCAN = 101;
    private int SHARE_APP_CODE = 103;
    private int SELFIE_CODE = 133;
    private boolean isFirstTime = true,isFirstTimeLoading = true;
    String defaultCity = "";
    String[] language = {"Select Language", "English", "Spanish"};

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivityComponent().inject(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimary));
        }

     /*   Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);
        float density = getResources().getDisplayMetrics().density;
        float dpHeight = outMetrics.heightPixels / density;
        float dpWidth = outMetrics.widthPixels / density;
        float ratio = ((float)outMetrics.heightPixels / (float)outMetrics.widthPixels);
        Log.e("Tag", "size" + dpHeight);
        Log.e("Tag", "size" + dpWidth);
        Log.e("Tag", "ratio" + ratio);
        Log.e("Tag", "density" + density);*/
        binding = DataBindingUtil.setContentView(this, R.layout.activity_user_home);
        binding.setClickListener(this);
        binding.toolbar.setClickListener(this);
        binding.header.setClickListener(this);
        binding.layoutFooter.setClickListener(this);
        binding.toolbar.ivDrawer.setVisibility(View.VISIBLE);
        binding.toolbar.rlSpinner.setVisibility(View.VISIBLE);
        binding.toolbar.scanner.setVisibility(View.VISIBLE);
        mPresenter.onAttach(this);
        getCurrentLocation = new GetCurrentLocation(MainActvity.this);
        getCurrentLocation.getCurrentLocation();
       /* getCurrentLocation.getContinuousLocation(true);
        getCurrentLocation.setLocationIntervals(1000,1000);*/
        binding.fragmentContainer.setVisibility(View.GONE);
        binding.linProgress.setVisibility(View.VISIBLE);
        CommonUtils.selectedIndex = 0;
        setLoginDetails(mPresenter.getLoginDetails());
        mPresenter.getAllStatueList();
        setUpDrawer();
        mCameraIntentHelper = mPresenter.getmCameraIntentHelper(whichImage, MainActvity.this);


    }

    private void setUpDrawer() {


        /* set up drawer items*/
        drawerAdapter.addItems(mPresenter.getDrawerList(this));

        drawerAdapter.setCallback(new DrawerAdapter.OnItemClickListener() {
            @Override
            public void onChatClick(DrawerItem items) {
                String itemName = items.getName();
                if (itemName.equals(getString(R.string.text_contact_us))) {
                    Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                            "mailto", "berthamedia@gmail.com", null));
                    emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Talking Statues Help");
                    startActivity(Intent.createChooser(emailIntent, null));
                } else if (itemName.equals(getString(R.string.text_about_us))) {
                    Intent intent = new Intent(MainActvity.this, AboutUsActivity.class);
                    startActivity(intent);
                } else if (itemName.equals(getString(R.string.text_faq))) {
                    Intent intent = new Intent(MainActvity.this, FaqActivity.class);
                    startActivity(intent);
                } else if (itemName.equals(getString(R.string.text_rate_app))) {
                    mPresenter.redirectToPlayStore(MainActvity.this);
                } else if (itemName.equals(getString(R.string.text_share_app))) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                                && checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                            mPresenter.shareApp(MainActvity.this);
                        } else {
                            requestPermissions(cameraPermissions, SHARE_APP_CODE);
                        }
                    } else {
                        mPresenter.shareApp(MainActvity.this);
                    }
                } else {
                    mPresenter.setLoginDetails(null);
                    CommonUtils.selectedIndex = 0;
                    Intent i = new Intent(MainActvity.this, LoginActivity.class);
                    startActivity(i);
                }

                if (binding.drawerlayout.isDrawerOpen(GravityCompat.START)) {
                    binding.drawerlayout.closeDrawer(GravityCompat.START);
                }
            }
        });


        /* langauge spinner*/

        binding.recDrawer.setAdapter(drawerAdapter);
        binding.spLanguage.setAdapter(new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, language));
        if(mPresenter.getLanguageCode().equals(AppConstants.LANGUAGE_ENGLISH)){
            binding.spLanguage.setSelection(1);
        }else{
            binding.spLanguage.setSelection(2);
        }
        binding.spLanguage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    if (position == 1) {
                        LocaleHelper.setLocale(MainActvity.this, AppConstants.LANGUAGE_ENGLISH);
                        mPresenter.setLanguageCode(AppConstants.LANGUAGE_ENGLISH);
                        CommonUtils.LanguageCode = AppConstants.LANGUAGE_ENGLISH;
                    } else if (position == 2) {
                        LocaleHelper.setLocale(MainActvity.this, AppConstants.LANGUAGE_SPANISH);
                        mPresenter.setLanguageCode(AppConstants.LANGUAGE_SPANISH);
                        CommonUtils.LanguageCode = AppConstants.LANGUAGE_SPANISH;

                    }
                    if(!isFirstTimeLoading){
                        refreshActivity();
                    }
                    isFirstTimeLoading = false;

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });




    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    private  void refreshActivity(){
        finish();
        overridePendingTransition(0, 0);
        startActivity(getIntent());
        overridePendingTransition(0, 0);

    }

    @Override
    public void onClick(View v) {
        if (v == binding.toolbar.ivDrawer) {

            if (binding.drawerlayout.isDrawerOpen(GravityCompat.START)) {
                binding.drawerlayout.closeDrawer(GravityCompat.START);
            } else {
                binding.drawerlayout.openDrawer(GravityCompat.START);
            }

        } else if (v == binding.header.linearEdit) {
            Intent intent = new Intent(MainActvity.this, EditProfileActivity.class);
            startActivity(intent);
        } else if (v == binding.toolbar.scanner) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                        && checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                    Intent i = new Intent(MainActvity.this, QrCodeActivity.class);
                    startActivityForResult(i, REQUEST_CODE_QR_SCAN);
                } else {
                    requestPermissions(cameraPermissions, REQUEST_PERMISSION_CAMERA);
                }
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_PERMISSION_CAMERA) {
            if (grantResults.length > 0) {
                boolean isGranted = true;
                for (int i = 0; i < grantResults.length; i++) {
                    if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                        isGranted = false;
                        break;
                    }
                }
                if (isGranted) {
                    Intent i = new Intent(MainActvity.this, QrCodeActivity.class);
                    startActivityForResult(i, REQUEST_CODE_QR_SCAN);
                }
            }
        }
    }

    public void setLoginDetails(LoginDetails loginDetails) {
        binding.header.tvProfileName.setText(loginDetails.getName());
        this.loginDetails = loginDetails;
    }

    public void loadFragment(Fragment fragment) {
        String backState = fragment.getClass().getSimpleName();
        String fragmentTag = backState;
        CommonUtils.pLog(TAG, fragment.getClass().getSimpleName().toString());
        FragmentManager fragmentManager = getSupportFragmentManager();
        boolean fragmentPopped = fragmentManager.popBackStackImmediate(backState, 0);
        if (!fragmentPopped && fragmentManager.findFragmentByTag(fragmentTag) == null) {
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.fragmentContainer, fragment, fragmentTag);
            fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            fragmentTransaction.addToBackStack(backState);
            fragmentTransaction.commit();
        }
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
            ActivityCompat.finishAffinity(this);
        } else {
        }
    }

    @Override
    public void gpsStatus(boolean _status) {
        if (!_status) {
            new CheckGPSDialog(this).showDialog(new DialogClickListener() {
                @Override
                public void positiveListener(Activity context, Dialog dialog) {
                    dialog.dismiss();
                    getCurrentLocation.getCurrentLocation();
                }

                @Override
                public void negativeListener(Activity context, Dialog dialog) {
                    dialog.dismiss();
                }
            });
        } else {
            getCurrentLocation.getCurrentLocation();
        }
    }

    @Override
    public void gpsPermissionDenied(int deviceGpsStatus) {
        if (deviceGpsStatus == 1) {
            permissionDeniedByUser();
        } else {
            getCurrentLocation.getCurrentLocation();
        }
    }

    @Override
    public void gpsLocationFetched(Location location) {
        if (location != null) {
            this.locationTest = location;
            binding.fragmentContainer.setVisibility(View.VISIBLE);
            binding.linProgress.setVisibility(View.GONE);
            Log.e(TAG, location.getLatitude() + ", " + location.getLongitude());
            if (location.hasAccuracy() && location.getAccuracy() < AppConstants.ACCURACY_LIMIT)
                mPresenter.storeLocation(String.valueOf(location.getLatitude()), String.valueOf(location.getLongitude()));
                mPresenter.getDefaultCity(location.getLatitude(), location.getLongitude(), MainActvity.this);
            //   mPresenter.getDefaultCity(55.6688736, 12.5798416, MainActvity.this);
        } else {
            Toast.makeText(this, getResources().getString(R.string.unable_find_location), Toast.LENGTH_SHORT).show();
        }
    }

    private void permissionDeniedByUser() {
        new PermissionDeniedDialog(this).showDialog(new DialogClickListener() {
            @Override
            public void positiveListener(Activity context, Dialog dialog) {
                dialog.dismiss();
                getCurrentLocation.getCurrentLocation();
            }

            @Override
            public void negativeListener(Activity context, Dialog dialog) {
                dialog.dismiss();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SELFIE_CODE && resultCode == Activity.RESULT_OK) {
            String path = data.getStringExtra("image");
            mPresenter.setSelfieApi(path, mPresenter.getLoginDetails().getId());
        }
        if (requestCode == whichImage) {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragmentContainer);
            if (fragment != null) {
                fragment.onActivityResult(requestCode, resultCode, data);
            }
        }
        if (requestCode == REQUEST_CODE_QR_SCAN) {
            if (data == null)
                return;
            String result = data.getStringExtra("com.blikoon.qrcodescanner.got_qr_scan_relult");
            //  mPresenter.getStatueDetailsApi(result);
            if (result != null) {
                mPresenter.getStatueDetailsFromPrefs(result);
            } else {
                Toast.makeText(this, "Unable to get info", Toast.LENGTH_SHORT).show();
            }
        } else if (requestCode == SHARE_APP_CODE) {
            mPresenter.shareApp(MainActvity.this);
        }


    }

    @Override
    public void onCityListLoaded(final List<CityDetails> cityList) {
        List<String> strCityList = new ArrayList<>();
        strCityList.clear();
        strCityList.add(getString(R.string.select_city));
        CityDetails defaultCityDetails = null;
        for (CityDetails cityDetails : cityList) {
            if (defaultCity.contains(cityDetails.getName())) {
                defaultCityDetails = cityDetails;
            }
            strCityList.add(cityDetails.getName());
        }
        loadFragment(MainFragment.newInstance(defaultCityDetails != null ? defaultCityDetails.getCityId() : cityList.get(0).getCityId()));
        mPresenter.getSponserLogos(defaultCityDetails != null ? defaultCityDetails.getCityId() : cityList.get(0).getCityId(),this);
        binding.toolbar.citySpinner.setAdapter(new ArrayAdapter<String>(this, R.layout.default_spinner_iem, strCityList));
        binding.toolbar.citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!isFirstTime) {
                    if (position != 0) {
                        mPresenter.getSponserLogos(cityList.get(position - 1).getCityId(),MainActvity.this);
                        CommonUtils.selectedIndex = position;
                        MainFragment fragment = (MainFragment) getSupportFragmentManager().findFragmentById(R.id.fragmentContainer);
                        if (fragment != null) {
                            fragment.changeCity(cityList.get(position - 1).getCityId());
                        }
                    } else {
                        binding.toolbar.citySpinner.setSelection(CommonUtils.selectedIndex);
                    }
                }
                isFirstTime = false;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        binding.toolbar.citySpinner.setSelection(0);
    }


    @Override
    public void onLogosLoaded(List<LogoDetails> json) {

        logosAdapter.clearList();
        logosAdapter.addItems(json);
        binding.layoutFooter.recLogos.setAdapter(logosAdapter);
        logosAdapter.setCallback(new LogosAdapter.PlaceAdapterOnItemClickListener() {
            @Override
            public void onChatClick(LogoDetails items) {
                if (items.getSponsorLogoWebUrl() != null) {
                    Intent i = new Intent(MainActvity.this, WebViewActivity.class);
                    i.putExtra("url", items.getSponsorLogoWebUrl());
                    i.putExtra("toolbarName", R.string.app_name);
                    startActivity(i);
                }

            }
        });
    }


    @Override
    public void onStatueVisited(StatueDetails statueDetails) {
        Intent i = new Intent(MainActvity.this, CallingActivity.class);
        i.putExtra(AppConstants.Object, statueDetails);
        startActivity(i);
    }

    @Override
    public void shareSelfie(String mImage) {
        Bitmap icon = BitmapFactory.decodeFile(mImage);
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("image/jpeg");
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, "title");
        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
        Uri uri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                values);
        OutputStream outstream;
        try {
            outstream = getContentResolver().openOutputStream(uri != null ? uri : null);
            icon.compress(Bitmap.CompressFormat.JPEG, 100, outstream);
            if (outstream != null) {
                outstream.close();
            }
        } catch (Exception e) {
            System.err.println(e.toString());
        }

        share.putExtra(Intent.EXTRA_STREAM, uri);
        startActivity(Intent.createChooser(share, getString(R.string.shareSelfieUsing)));
    }

    @Override
    public void onDefaultCityFound(String city) {

        if (city != null) {
            Log.e("CITY", city);
            if (city.contains("København")) {
                defaultCity = "Copenhagen";
            } else {
                defaultCity = city;
            }
        }
        mPresenter.getCityList();
    }

    @Override
    public void onDefaualtCityNotFound() {
        mPresenter.getCityList();
    }

    @Override
    public void openStatueLInk(String qrCode) {
        Intent i = new Intent(MainActvity.this,WebViewActivity.class);
        i.putExtra("url",qrCode);
        i.putExtra("toolbarName",qrCode);
        startActivity(i);
    }


}
