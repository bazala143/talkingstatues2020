package com.talkingstatues.ui.activities.faqActivity;

import com.talkingstatues.model.banner.BannerDetails;
import com.talkingstatues.model.faqList.FaqDetails;
import com.talkingstatues.ui.base.MvpView;

import java.util.List;

public interface FaqMvpView extends MvpView {

    void onFaqLoaded(List<FaqDetails> json);
}
