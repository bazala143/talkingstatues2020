package com.talkingstatues.ui.activities.redeemReward;

import android.app.Activity;
import android.content.DialogInterface;
import android.util.Log;

import androidx.appcompat.app.AlertDialog;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.talkingstatues.R;
import com.talkingstatues.data.prefs.AppPreferencesHelper;
import com.talkingstatues.model.getReward.GetRewardMaster;
import com.talkingstatues.model.redeem.RedeemMaster;
import com.talkingstatues.networking.API_Params;
import com.talkingstatues.networking.NetworkError;
import com.talkingstatues.networking.NetworkService;
import com.talkingstatues.ui.base.BasePresenter;
import com.talkingstatues.utilities.CommonUtils;

import org.json.JSONException;
import org.json.JSONObject;

import javax.inject.Inject;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

public class RedeemRewardPresenter<V extends RedeemRewardMvpView> extends BasePresenter<V>
        implements RedeemRewardMvpPresenter<V> {
    @Inject
    NetworkService service;
    private static final String TAG = RedeemRewardPresenter.class.getSimpleName();

    @Inject
    public RedeemRewardPresenter(AppPreferencesHelper preferencesHelper, CompositeSubscription mSubscription) {
        super(preferencesHelper, mSubscription);
    }

    @Override
    public void onDetach() {
        if (getSubscription() != null && getSubscription().hasSubscriptions()) {
            getSubscription().clear();
        }
    }

    @Override
    public void redeemReward(String cityId) {
        if (getMvpView().isNetworkConnected()) {
            getMvpView().showLoading();
            Subscription s = service.redeemReward(rewardParams(cityId), new NetworkService.GetRedeemRewardCallback() {
                @Override
                public void onSuccess(RedeemMaster master) {
                    getMvpView().hideLoading();
                    CommonUtils.pLog(TAG, "Response : " + new Gson().toJson(master).toString());

                    getMvpView().onRewardRedeemed(master);

                }

                @Override
                public void onError(NetworkError networkError) {
                    getMvpView().showMessage(networkError.getMessage());
                }
            });
            getSubscription().add(s);
        } else {
            getMvpView().onError(R.string.utils__no_connection);
        }

    }

    @Override
    public void showConformationDialog(final Activity activity, final String cityId) {
        AlertDialog alertDialog = new AlertDialog.Builder(activity).create();
        alertDialog.setTitle(activity.getString(R.string.text_warning));
        alertDialog.setMessage(activity.getString(R.string.text_cant_redone));

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, activity.getString(R.string.text_accept), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                redeemReward(cityId);
            }
        });

        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, activity.getString(R.string.text_decline), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                activity.finish();
            }
        });

        alertDialog.show();
    }

    @Override
    public void getRewardDetails(String cityId) {
        if (getMvpView().isNetworkConnected()) {
            getMvpView().showLoading();
            Subscription s = service.getRewardDetails(rewardParams(cityId), new NetworkService.GetRewardDetailsCallback() {
                @Override
                public void onSuccess(GetRewardMaster master) {
                    getMvpView().hideLoading();
                    getMvpView().onRewardDetails(master);
                }

                @Override
                public void onError(NetworkError networkError) {
                    getMvpView().showMessage(networkError.getMessage());
                }
            });
            getSubscription().add(s);
        } else {
            getMvpView().onError(R.string.utils__no_connection);
        }
    }

    private JsonObject rewardParams(String cityId) {
        JsonObject gsonObject = new JsonObject();
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(API_Params.user_id, getPreferencesHelper().getLoginModel().getId() + "");
            jsonObject.put(API_Params.city_id, cityId);
            JsonParser parser = new JsonParser();
            gsonObject = (JsonObject) parser.parse(jsonObject.toString());
            Log.e("RedeemReawrdParams",jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return gsonObject;
    }


}
