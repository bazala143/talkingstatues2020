package com.talkingstatues.ui.activities.aboutUs;

import com.talkingstatues.model.aboutUs.AboutUsDetails;
import com.talkingstatues.ui.base.MvpView;

import java.util.List;

public interface AboutUsMvpView extends MvpView {

    void onGetLogs(String whichApi, String errorMsg);

    void onAbousLoaded(List<AboutUsDetails> json);
}
