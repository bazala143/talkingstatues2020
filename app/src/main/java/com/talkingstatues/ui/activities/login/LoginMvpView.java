package com.talkingstatues.ui.activities.login;

import com.talkingstatues.ui.base.MvpView;

public interface LoginMvpView extends MvpView {
    void redirectToHomeScreen();

    void onGetLogs(String whichApi, String errorMsg);

    void onLoginSuccess();
}
