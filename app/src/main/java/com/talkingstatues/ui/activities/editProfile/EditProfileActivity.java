package com.talkingstatues.ui.activities.editProfile;

import android.os.Bundle;
import android.view.View;

import androidx.databinding.DataBindingUtil;

import com.talkingstatues.R;
import com.talkingstatues.databinding.ActivityEditProfileBinding;
import com.talkingstatues.model.login.LoginDetails;
import com.talkingstatues.ui.base.BaseActivity;
import com.talkingstatues.utilities.CommonUtils;

import javax.inject.Inject;

public class EditProfileActivity extends BaseActivity implements View.OnClickListener, EditProfileMvpView {

    ActivityEditProfileBinding binding;
    @Inject
    EditProfileMvpPresenter<EditProfileMvpView> mPresenter;

    private static final String TAG = EditProfileActivity.class.getSimpleName();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivityComponent().inject(this);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_edit_profile);
        binding.setClickListener(this);
        binding.toolbar.setClickListener(this);
        mPresenter.onAttach(this);
        mPresenter.onViewPrepared();
        binding.toolbar.setClickListener(this);
        binding.toolbar.ivback.setVisibility(View.VISIBLE);
        binding.toolbar.tvTitle.setVisibility(View.VISIBLE);
        binding.toolbar.tvTitle.setText(R.string.text_profile);
        mPresenter.onViewPrepared();

    }

    @Override
    public void onClick(View v) {
        if (v == binding.toolbar.ivback) {
            EditProfileActivity.this.finish();
        } else if (v == binding.btnSave) {
            if (mPresenter.isValid(binding)) {
                mPresenter.updateProfileApi(binding);
            }

        }
    }

    @Override
    public void getLoginDetails(LoginDetails loginDetails) {
        if (loginDetails != null) {
            binding.toolbar.tvTitle.setText(!CommonUtils.isEmpty(loginDetails.getName()) ? loginDetails.getName() : "");
            binding.setLoginDetail(loginDetails);
            if (!CommonUtils.isEmpty(loginDetails.getName())) {
                binding.edName.setText(loginDetails.getName());
            }
            if (!CommonUtils.isEmpty(loginDetails.getEmail())) {
                binding.edEmail.setText(loginDetails.getEmail());
            }
            if (!CommonUtils.isEmpty(loginDetails.getMobileNumber())) {
                binding.edMobileNumber.setText(loginDetails.getMobileNumber());
            }
            if (!CommonUtils.isEmpty(loginDetails.getCity())) {
                binding.edCity.setText(loginDetails.getCity());
            }

            if (!CommonUtils.isEmpty(loginDetails.getGoogle())) {
                binding.edGoogle.setText(loginDetails.getGoogle());
            }
            if (!CommonUtils.isEmpty(loginDetails.getFacebook())) {
                binding.edFbAccount.setText(loginDetails.getFacebook());
            }
            if (!CommonUtils.isEmpty(loginDetails.getAge().toString())) {
                binding.edAge.setText(loginDetails.getAge() + "");
            }


        }
    }


    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }
}
