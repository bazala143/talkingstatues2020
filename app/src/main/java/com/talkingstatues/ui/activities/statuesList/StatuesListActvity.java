
package com.talkingstatues.ui.activities.statuesList;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.SyncStateContract;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.talkingstatues.R;
import com.talkingstatues.databinding.ActivityStatuesListBinding;
import com.talkingstatues.model.city.CityDetails;
import com.talkingstatues.model.login.LoginDetails;
import com.talkingstatues.model.statueList.StatueDetails;
import com.talkingstatues.networking.API_Params;
import com.talkingstatues.ui.activities.statueDetails.StatueDetailsActivity;
import com.talkingstatues.ui.activities.statuesMap.StatuesMapActivity;
import com.talkingstatues.ui.base.BaseActivity;
import com.talkingstatues.utilities.AppConstants;
import com.talkingstatues.utilities.CommonUtils;
import com.talkingstatues.utilities.RequestCodes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.inject.Inject;

public class StatuesListActvity extends BaseActivity implements View.OnClickListener, StatuesListMvpView {

    ActivityStatuesListBinding binding;
    @Inject
    StatuesListMvpPresenter<StatuesListMvpView> mPresenter;
    LoginDetails loginDetails;
    private static final String TAG = StatuesListActvity.class.getSimpleName();

    @Inject
    StatuesListAdapter statuesListAdapter;
    String cityId = "";
    int selectedIndex = 0;
    boolean isFirstTime = true;
    List<StatueDetails> allStatueList = new ArrayList<>();
    CityDetails defaultCity;
    String languageCode= AppConstants.LANGUAGE_ENGLISH;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivityComponent().inject(this);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_statues_list);
        binding.setClickListener(this);
        binding.toolbar.rlSpinner.setVisibility(View.VISIBLE);
        binding.toolbar.setClickListener(this);
        binding.toolbar.ivback.setVisibility(View.VISIBLE);
        setLoginDetails(mPresenter.getLoginDetails());
        mPresenter.onAttach(this);
        mPresenter.getCityList();
        cityId = getIntent().getExtras().getString(API_Params.city_id);
        Log.e("cityId", cityId);
        languageCode = mPresenter.getLanguageCode();
        if(languageCode == null){
            Log.e("LanguageCode","NNNNNN");
        }
        mPresenter.getStatueListApi(cityId);
    }


    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        if (v == binding.tvCancel) {
            binding.edSearchStore.setText("");
        } else if (v == binding.toolbar.ivback) {
            finish();
        }
    }

    public void setLoginDetails(LoginDetails loginDetails) {
        this.loginDetails = loginDetails;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RequestCodes.REQUEST_MAIN_ACTIVITY && resultCode == RESULT_OK) {
            setResult(RESULT_OK);
            finish();
        }
    }

    @Override
    public void onCityListLoaded(final List<CityDetails> cityList) {
        List<String> strCityList = new ArrayList<>();
        strCityList.add(getString(R.string.select_city));
        for (CityDetails cityDetails : cityList) {
            if (cityDetails.getCityId().equals(cityId)) {
                selectedIndex = cityList.indexOf(cityDetails);
                defaultCity = cityDetails;

            }
            strCityList.add(cityDetails.getName());
        }

        binding.toolbar.citySpinner.setAdapter(new ArrayAdapter<String>(this, R.layout.default_spinner_iem, strCityList));
        binding.toolbar.citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0 && !isFirstTime) {
                    mPresenter.getStatueListApi(cityList.get(position - 1).getCityId());
                    cityId = cityList.get(position - 1).getCityId();
                    defaultCity = cityList.get(position-1);
                    if(statuesListAdapter != null && defaultCity != null){
                        statuesListAdapter.setCountry(defaultCity.getCountry());
                        statuesListAdapter.notifyDataSetChanged();
                    }
                }else{
                    binding.toolbar.citySpinner.setSelection(CommonUtils.selectedIndex);
                }
                isFirstTime = false;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        binding.toolbar.citySpinner.setSelection(CommonUtils.selectedIndex);

    }

    @Override
    public void onStatueListLoaded(List<StatueDetails> statueList) {
        allStatueList = statueList;
        statuesListAdapter = new StatuesListAdapter(StatuesListActvity.this, statueList);
        statuesListAdapter.setLanguageCode(languageCode);
        if(defaultCity != null){
            statuesListAdapter.setCountry(defaultCity.getCountry());
        }
        binding.rvStatuesList.setHasFixedSize(true);
        binding.rvStatuesList.setLayoutManager(new LinearLayoutManager(this));
        binding.rvStatuesList.setAdapter(statuesListAdapter);
        statuesListAdapter.setonItemClickListnere(new StatuesListAdapter.OnItemClickCallBack() {
            @Override
            public void onInfoCallBack(StatueDetails statueDetails, int position) {
                if (statueDetails == allStatueList.get(position)) {
                    Intent i = new Intent(StatuesListActvity.this, StatueDetailsActivity.class);
                    i.putExtra(AppConstants.Object, statueDetails);
                    startActivity(i);
                } else {
                    int indexOfFiltered = allStatueList.indexOf(statueDetails);
                    Intent i = new Intent(StatuesListActvity.this, StatueDetailsActivity.class);
                    i.putExtra(AppConstants.Object, allStatueList.get(indexOfFiltered));
                    startActivity(i);
                }

            }

            @Override
            public void onMapCallBack(StatueDetails statueDetails, int position) {
                if (statueDetails == allStatueList.get(position)) {
                    Intent i = new Intent(StatuesListActvity.this, StatuesMapActivity.class);
                    Bundle args = new Bundle();
                    args.putSerializable(AppConstants.statueList, (Serializable) allStatueList);
                    args.putInt(AppConstants.index, position);
                    i.putExtra("FROM", AppConstants.STATUE_LIST_ACTIVITY);
                    i.putExtra("BUNDLE", args);
                    startActivity(i);
                } else {
                    int indexOfFiltered = allStatueList.indexOf(statueDetails);
                    Intent i = new Intent(StatuesListActvity.this, StatuesMapActivity.class);
                    Bundle args = new Bundle();
                    args.putSerializable(AppConstants.statueList, (Serializable) allStatueList);
                    args.putInt(AppConstants.index, indexOfFiltered);
                    i.putExtra("FROM", AppConstants.STATUE_LIST_ACTIVITY);
                    i.putExtra("BUNDLE", args);
                    startActivity(i);
                }

            }
        });
        binding.edSearchStore.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                statuesListAdapter.getFilter().filter(s);

            }

            @Override
            public void afterTextChanged(Editable s) {
                statuesListAdapter.getFilter().filter(s);

            }
        });


    }

    @Override
    public void onFetchingDistance(final List<StatueDetails> s) {
        Collections.sort(s, new Comparator<StatueDetails>() {
            @Override
            public int compare(StatueDetails c1, StatueDetails c2) {
                if (c1.getDistanceElement() != null && c2.getDistanceElement() != null) {
                    if (c1.getDistanceElement().getDistance() != null && c2.getDistanceElement().getDistance() != null) {
                        return Integer.compare(c1.getDistanceElement().getDistance().getValue(), c2.getDistanceElement().getDistance().getValue());
                    } else {
                        return Integer.compare(1, 0);
                    }
                } else {
                    return Integer.compare(1, 0);
                }
            }
        });
        allStatueList.clear();
        allStatueList = s;
        statuesListAdapter.clearAll();
        statuesListAdapter.addAll(s);
    }


}
