package com.talkingstatues.ui.activities.statuesMap;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;

import com.blikoon.qrcodescanner.QrCodeActivity;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.talkingstatues.R;
import com.talkingstatues.databinding.ActivityStatuesMapBinding;
import com.talkingstatues.model.distance.DistanceElement;
import com.talkingstatues.model.statueList.StatueDetails;
import com.talkingstatues.networking.APIUrl;
import com.talkingstatues.networking.API_Params;
import com.talkingstatues.ui.activities.calling.CallingActivity;
import com.talkingstatues.ui.activities.main.MainActvity;
import com.talkingstatues.ui.activities.statueDetails.StatueDetailsActivity;
import com.talkingstatues.ui.activities.webviewActivity.WebViewActivity;
import com.talkingstatues.ui.base.BaseActivity;
import com.talkingstatues.ui.fragments.CMapFragment;
import com.talkingstatues.utilities.AppConstants;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;


public class StatuesMapActivity extends BaseActivity implements StatuesMapMvpView, OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {

    ActivityStatuesMapBinding binding;
    @Inject
    StatuesMapMvpPresenter<StatuesMapMvpView> mPresenter;
    StatueAdapter statueAdapter;
    private static final int LOCATION_PERMISSION_CODE = 102;


    /* map variable */
    private GoogleMap mMap;
    private double longitude;
    private double latitude;
    private GoogleApiClient googleApiClient;
    CMapFragment mapFragment;
    LatLng currentLatLong;
    private static final int REQUEST_CODE_QR_SCAN = 101;
    /* map variable */
    String[] cameraPermissions = {Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};

    private final int REQUEST_PERMISSION_CAMERA = 1;
    String cityId = "";
    LatLng latLng;
    StatueDetails intentStatueDetails;
    StatueDetails varStatueDetails;
    LatLng selectedMarkerLatLong = null;
    boolean isFirstTime = true;
    Map<StatueDetails, Circle> circuleMap = new HashMap<>();
    ;
    List<StatueDetails> allStatueList = new ArrayList<>();
    List<StatueDetails> apiStatueList = new ArrayList<>();
    HashMap<StatueDetails, Marker> markerMap = new HashMap<>();
    int listIndex = 0;
    int apiIndex = 0;
    Polyline singlePolyline;


    /* just for location updates*/

    // protected LocationListener locationListener;
    protected LocationManager locationManager;
    protected Context context;
    boolean isApiCalled = false;
    String fromActivity = "";
    String languageCode = AppConstants.LANGUAGE_ENGLISH;


    /*changed location*/

    private FusedLocationProviderClient mFusedLocationClient;
    LocationRequest locationRequest;
    LocationCallback locationCallback;
    boolean isPlolyLineDrawn = true;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivityComponent().inject(this);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_statues_map);
        mPresenter.onAttach(this);
        binding.toolbar.setClickListener(this);
        binding.toolbar.ivback.setVisibility(View.VISIBLE);
        binding.toolbar.tvTitle.setVisibility(View.VISIBLE);
        binding.toolbar.scanner.setVisibility(View.VISIBLE);
        binding.toolbar.tvTitle.setText(R.string.text_statue_map);
        binding.setClickListener(this);
        binding.toolbar.setClickListener(this);
        cityId = getIntent().getExtras().getString(API_Params.city_id);
        Intent intent = getIntent();
        fromActivity = intent.getStringExtra("FROM");
        Bundle args = intent.getBundleExtra("BUNDLE");
        languageCode = mPresenter.getLanguageCode();
        if (args != null) {
            allStatueList = (ArrayList<StatueDetails>) args.getSerializable(AppConstants.statueList);
            listIndex = args.getInt(AppConstants.index);
            intentStatueDetails = allStatueList.get(listIndex);
            intentStatueDetails.setInZone("no");
            // 22.721268, 72.874752
            //22.721232, 72.874772
            //22.721254, 72.874824
            /*intentStatueDetails.setLatitude("22.721254");
            intentStatueDetails.setLongitude("72.874824");
            intentStatueDetails.setRangeRadius("15");*/
            /* hellojh hdhfghg ghfhjgf */
            allStatueList.set(listIndex, intentStatueDetails);
            statueAdapter = new StatueAdapter();
            statueAdapter.setLanguageCode(languageCode);
            statueAdapter.addItems(allStatueList);
            binding.recStatues.setAdapter(statueAdapter);
            statueAdapter.setCallback(new StatueAdapter.OnItemClickListener() {
                @Override
                public void onChatClick(StatueDetails item) {
                    Intent i = new Intent(StatuesMapActivity.this, StatueDetailsActivity.class);
                    i.putExtra(AppConstants.Object, item);
                    startActivity(i);
                }
            });
        }
        startAnimation();
        initGoogleMap();

        /* this is fro location update*/
        context = StatuesMapActivity.this;
        if (allStatueList.size() != 0) {
            //  registerLocationUpdates();
            registerLocationCallback();
        }


        /* changed location*/

    }

    private void registerLocationCallback() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(2000);
        locationRequest.setFastestInterval(1000);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locationCallback = new LocationCallback() {

            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                List<Location> locationList = locationResult.getLocations();
                Location location = locationList.get(locationList.size() - 1);
                if (location != null) {
                    binding.linProgress.setVisibility(View.GONE);
                    //  Toast.makeText(context, ""+location.getLatitude(), Toast.LENGTH_SHORT).show();
                    startTrackingDistance(location);
                }


            }
        };
        mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (grantResults.length == 2) {
            if (requestCode == LOCATION_PERMISSION_CODE) {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    getCurrentLocation();
                } else {
                    mPresenter.showRequiredPermissionDialog(StatuesMapActivity.this);
                }
                return;
            }
        } else if (requestCode == REQUEST_PERMISSION_CAMERA) {
            if (grantResults.length > 0) {
                boolean isGranted = true;
                for (int i = 0; i < grantResults.length; i++) {
                    if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                        isGranted = false;
                        break;
                    }
                }
                if (isGranted) {
                    Intent i = new Intent(StatuesMapActivity.this, QrCodeActivity.class);
                    startActivityForResult(i, REQUEST_CODE_QR_SCAN);
                }
            }
        }


    }

    private void zoomToMarker(LatLng currentLocation) {
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 15));
        mMap.animateCamera(CameraUpdateFactory.zoomIn());
        mMap.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);
    }

    /* map methods*/

    private void initGoogleMap() {
        mapFragment = (CMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();


    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        //  mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
        try {

            boolean success = mMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.map_style));
            if (!success) {
                Log.e("MapsActivityRaw", "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e("MapsActivityRaw", "Can't find style.", e);
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                        LOCATION_PERMISSION_CODE);
            }
            return;
        } else {
            mMap.setMyLocationEnabled(true);

        }
        mMap.getUiSettings().setZoomControlsEnabled(false);
        mapFragment.setListener(new CMapFragment.OnTouchListener() {
            @Override
            public void onTouch() {

            }
        });

    }

    private void startAnimation() {
        Animation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(1000); //You can manage the blinking time with this parameter
        anim.setStartOffset(20);
        anim.setRepeatMode(Animation.REVERSE);
        anim.setRepeatCount(Animation.INFINITE);
        binding.btnGetCloser.startAnimation(anim);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        getCurrentLocation();
    }


    @Override
    protected void onResume() {
        super.onResume();
       /* if (locationManager != null) {
            registerLocationUpdates();
        }*/
        if (mFusedLocationClient != null) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);
        }

    }

    @Override
    protected void onDestroy() {
       /* if (locationManager != null) {
            locationManager.removeUpdates(this);
        }*/
        if (mFusedLocationClient != null) {
            mFusedLocationClient.removeLocationUpdates(locationCallback);
        }
        super.onDestroy();

    }


    public void makeVisitStatueApi() {
        if (varStatueDetails != null) {
            mPresenter.visitStatueApi(varStatueDetails);
        } else {
            mPresenter.visitStatueApi(intentStatueDetails);
        }
    }


    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    @Override
    protected void onStart() {
        googleApiClient.connect();
        super.onStart();
    }

    @Override
    protected void onStop() {
        googleApiClient.disconnect();
       /* if (locationManager != null) {
            locationManager.removeUpdates(this);
        }*/
        if (mFusedLocationClient != null) {
            mFusedLocationClient.removeLocationUpdates(locationCallback);
        }
        super.onStop();
    }


    private void getCurrentLocation() {
        //    mMap.clear();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                        LOCATION_PERMISSION_CODE);
            }
            return;
        } else {
            Location location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
            if (location != null) {
                longitude = location.getLongitude();
                latitude = location.getLatitude();
                Log.e("location", "" + location.getLatitude());
                currentLatLong = new LatLng(latitude, longitude);
                if (intentStatueDetails != null && isFirstTime) {
                    addMarkers(allStatueList);
                    zoomToMarker(new LatLng(Double.parseDouble(intentStatueDetails.getLatitude()), Double.parseDouble(intentStatueDetails.getLongitude())));
                    if (intentStatueDetails != null) {
                        mPresenter.getPolyline(currentLatLong, new LatLng(Double.parseDouble(intentStatueDetails.getLatitude()), Double.parseDouble(intentStatueDetails.getLongitude())));
                    }
                }
                moveMap();
            }
        }

    }

    private void moveMap() {
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                selectedMarkerLatLong = new LatLng(marker.getPosition().latitude, marker.getPosition().longitude);
                mMap.setInfoWindowAdapter(new InfoWidnowAdapter(StatuesMapActivity.this));
                return false;
            }
        });

        if (isFirstTime && allStatueList.size() == 0) {
            mPresenter.getStatueListApi(cityId);
        }
    }

    @Override
    public void onClick(View view) {
        if (view == binding.toolbar.ivback) {
            finish();
        } else if (view == binding.btnGetCloser) {
            if (selectedMarkerLatLong != null) {
                latLng = selectedMarkerLatLong;
                CameraUpdate update = CameraUpdateFactory.newLatLngZoom(latLng, 15);
                mMap.moveCamera(update);
            } else if (intentStatueDetails != null) {
                latLng = new LatLng(Double.parseDouble(intentStatueDetails.getLatitude()), Double.parseDouble(intentStatueDetails.getLongitude()));
                CameraUpdate update = CameraUpdateFactory.newLatLngZoom(latLng, 15);
                mMap.moveCamera(update);
            } else {
                Toast.makeText(context, "Please select marker", Toast.LENGTH_SHORT).show();
            }

        } else if (view == binding.toolbar.scanner) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                        && checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                    Intent i = new Intent(StatuesMapActivity.this, QrCodeActivity.class);
                    startActivityForResult(i, REQUEST_CODE_QR_SCAN);
                } else {
                    requestPermissions(cameraPermissions, REQUEST_PERMISSION_CAMERA);
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_QR_SCAN) {
            if (data == null)
                return;
            String result = data.getStringExtra("com.blikoon.qrcodescanner.got_qr_scan_relult");
            mPresenter.getStatueDetailsFromPrefs(result);
        }
    }



    /* end here*/

    @Override
    public void onStatueListLoaded(List<StatueDetails> json) {
        statueAdapter = new StatueAdapter();
        statueAdapter.setLanguageCode(languageCode);
      /*  StatueDetails statueDetails = json.get(0);
        statueDetails.setLatitude("22.721254");
        statueDetails.setLongitude("72.874824");
        statueDetails.setRangeRadius("15");
        json.set(0,statueDetails);*/
        statueAdapter.addItems(json);
        binding.recStatues.setAdapter(statueAdapter);
        apiStatueList = json;

        /*for (StatueDetails details : apiStatueList) {
            details.setRangeRadius("50");
        }*/
        if (allStatueList.size() == 0) {
            addMarkers(json);
        }
        binding.linProgress.setVisibility(View.VISIBLE);
        //    registerLocationUpdates();
        registerLocationCallback();
        statueAdapter.setCallback(new StatueAdapter.OnItemClickListener() {
            @Override
            public void onChatClick(StatueDetails item) {
                Intent i = new Intent(StatuesMapActivity.this, StatueDetailsActivity.class);
                i.putExtra(AppConstants.Object, item);
                startActivity(i);
            }
        });
    }

    @Override
    public void onStatueVisited(StatueDetails statueDetails) {
        Intent i = new Intent(StatuesMapActivity.this, CallingActivity.class);
        i.putExtra(AppConstants.Object, statueDetails);
        startActivity(i);
    }


    List<LatLng> polyLine = new ArrayList<>();

    @Override
    public void onLoadedPolyline(List<LatLng> polyLineList) {
        polyLine.clear();
        polyLine = polyLineList;
        isPlolyLineDrawn = true;
        if (intentStatueDetails != null) {
            drawPolyline(currentLatLong, new LatLng(Double.parseDouble(intentStatueDetails.getLatitude()), Double.parseDouble(intentStatueDetails.getLongitude())));
        }
        if (varStatueDetails != null) {
            drawPolyline(currentLatLong, new LatLng(Double.parseDouble(varStatueDetails.getLatitude()), Double.parseDouble(varStatueDetails.getLongitude())));
        }
    }

    @Override
    public void onFetchingDistance(DistanceElement s) {

    }

    @Override
    public void openStatueLInk(String qrCode) {
        Intent i = new Intent(StatuesMapActivity.this, WebViewActivity.class);
        i.putExtra("url", qrCode);
        i.putExtra("toolbarName", qrCode);
        startActivity(i);
    }

    private void drawPolyline(LatLng userLatLng, LatLng placeLatLng) {
        if (singlePolyline != null) {
            singlePolyline.remove();
        }
        zoomTwoMarkers(userLatLng, placeLatLng);
        PolylineOptions polylineOptions = new PolylineOptions().color(Color.BLUE).width(20).addAll(polyLine).visible(true);
        singlePolyline = mMap.addPolyline(polylineOptions);
        if (intentStatueDetails != null) {
            changeCirculeColor();
        }
    }

    int mapCount = 0;

    private void addMarkers(List<StatueDetails> list) {
        circuleMap.clear();
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (final StatueDetails statueDetails : list) {
            Marker marker = mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(Double.parseDouble(statueDetails.getLatitude()), Double.parseDouble(statueDetails.getLongitude())))
                    .draggable(false)
                    .title(statueDetails.getStatueName()));

            if (intentStatueDetails == statueDetails) {
                marker.setIcon(BitmapDescriptorFactory.fromBitmap(mPresenter.createCustomMarker(StatuesMapActivity.this, null, AppConstants.MARKER_TYPE_RED)));
            } else {
                marker.setIcon(BitmapDescriptorFactory.fromBitmap(mPresenter.createCustomMarker(StatuesMapActivity.this, null, AppConstants.MARKER_TYPE_BLACK)));
            }
            markerMap.put(statueDetails, marker);
            addCircule(new LatLng(Double.parseDouble(statueDetails.getLatitude()), Double.parseDouble(statueDetails.getLongitude())), statueDetails);
            if (allStatueList.size() == 0) {
                builder.include(new LatLng(Double.parseDouble(statueDetails.getLatitude()), Double.parseDouble(statueDetails.getLongitude())));
            }
        }
        Log.e("mapSize", markerMap.size() + "");
        Log.e("statueLisSize", allStatueList.size() + "");
        for (final StatueDetails statueDetails : list) {
            Glide.with(this)
                    .asBitmap()
                    .load(APIUrl.IMAGE_URL + statueDetails.getImage())
                    .into(new SimpleTarget<Bitmap>(100, 100) {
                        @Override
                        public void onResourceReady(@NonNull Bitmap image, Transition<? super Bitmap> transition) {
                            Marker marker = markerMap.get(statueDetails);
                            if (intentStatueDetails == statueDetails) {
                                marker.setIcon(BitmapDescriptorFactory.fromBitmap(mPresenter.createCustomMarker(StatuesMapActivity.this, image, AppConstants.MARKER_TYPE_RED)));
                            } else {
                                marker.setIcon(BitmapDescriptorFactory.fromBitmap(mPresenter.createCustomMarker(StatuesMapActivity.this, image, AppConstants.MARKER_TYPE_BLACK)));
                            }
                            marker.setTag(image);
                            markerMap.put(statueDetails, marker);
                            mapCount = mapCount + 1;
                            Log.e("MapCount", mapCount + "");

                        }
                    });
        }
        if (allStatueList.size() == 0) {
            LatLngBounds bounds = builder.build();
            int width = getResources().getDisplayMetrics().widthPixels;
            //  int height = getResources().getDisplayMetrics().heightPixels;
            int padding = (int) (width * 0.10);
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
            mMap.animateCamera(cu);
        }
        isFirstTime = false;
    }

    private void addCircule(LatLng point, StatueDetails statueDetails) {
        CircleOptions circleOptions = new CircleOptions();
        circleOptions.center(point);
        circleOptions.radius(Double.parseDouble(statueDetails.getRangeRadius()));
        circleOptions.strokeColor(getResources().getColor(R.color.colorPrimaryDark));
        circleOptions.fillColor(getResources().getColor(R.color.redTrans));
        circleOptions.strokeWidth(2);
        Circle circle = mMap.addCircle(circleOptions);
        circuleMap.put(statueDetails, circle);
    }


    private void zoomTwoMarkers(LatLng userLatLng, LatLng placeLong) {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(userLatLng);
        builder.include(placeLong);
        LatLngBounds bounds = builder.build();
        int width = getResources().getDisplayMetrics().widthPixels;
        //  int height = getResources().getDisplayMetrics().heightPixels;
        int padding = (int) (width * 0.30);
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
        mMap.animateCamera(cu);
    }


   /* void registerLocationUpdates() {
        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_LOW);
        criteria.setPowerRequirement(Criteria.POWER_LOW);
        criteria.setAltitudeRequired(false);
        criteria.setBearingRequired(false);
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        String provider = locationManager.getBestProvider(criteria, true);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        if (fromActivity.equals(AppConstants.DASHBOARD_ACTIVITY)) {
            locationManager.requestLocationUpdates(provider, 0, 0, this);
        } else if (fromActivity.equals(AppConstants.STATUE_LIST_ACTIVITY)) {
            locationManager.requestLocationUpdates(provider, 0, 0, this);
        }
    }*/

    private void checkNearByStatue() {
        for (StatueDetails apiStatueDetails : apiStatueList) {
            double apiDistance = calculationByDistance(currentLatLong, new LatLng(Double.parseDouble(apiStatueDetails.getLatitude()), Double.parseDouble(apiStatueDetails.getLongitude())));
            double statueRadious = Double.parseDouble(apiStatueDetails.getRangeRadius());
            if (apiDistance < statueRadious + 5) {
                varStatueDetails = apiStatueDetails;
                apiIndex = apiStatueList.indexOf(apiStatueDetails);
            }
        }
    }

  /*  @Override
    public void onLocationChanged(Location location) {


    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }*/

    private void changeCirculeColor() {
        double countDistance = calculationByDistance(currentLatLong, new LatLng(Double.parseDouble(intentStatueDetails.getLatitude()), Double.parseDouble(intentStatueDetails.getLongitude())));
        Log.e("SSSSSS", countDistance + "");
        //   binding.tvDistance.setText("Distance = " + countDistance);
        double statueRangeradious = Double.parseDouble(intentStatueDetails.getRangeRadius());
        if (allStatueList.size() == markerMap.size()) {
            if (countDistance < statueRangeradious) {
                Circle gCircule = circuleMap.get(intentStatueDetails);
                gCircule.setFillColor(getResources().getColor(R.color.greenTrans));
                Marker marker = markerMap.get(intentStatueDetails);
                marker.setIcon(BitmapDescriptorFactory.fromBitmap(mPresenter.createCustomMarker(StatuesMapActivity.this, (Bitmap) marker.getTag(), AppConstants.MARKER_TYPE_GREEN)));
                zoomTwoMarkers(currentLatLong, new LatLng(Double.parseDouble(intentStatueDetails.getLatitude()), Double.parseDouble(intentStatueDetails.getLongitude())));
                if (allStatueList.size() > 0) {
                    if (statueAdapter != null) {
                        intentStatueDetails.setInZone("yes");
                        statueAdapter.setItem(listIndex, intentStatueDetails);
                    }
                }
                if (!isApiCalled) {
                    isApiCalled = true;
                    makeVisitStatueApi();
                }
            } else {
                Circle gCircule = circuleMap.get(intentStatueDetails);
                gCircule.setFillColor(getResources().getColor(R.color.redTrans));
                Marker marker = markerMap.get(intentStatueDetails);
                marker.setIcon(BitmapDescriptorFactory.fromBitmap(mPresenter.createCustomMarker(StatuesMapActivity.this, (Bitmap) marker.getTag(), AppConstants.MARKER_TYPE_RED)));
                if (allStatueList.size() > 0) {
                    if (statueAdapter != null) {
                        intentStatueDetails.setInZone("no");
                        statueAdapter.setItem(listIndex, intentStatueDetails);
                    }
                }
                isApiCalled = false;
                zoomTwoMarkers(currentLatLong, new LatLng(Double.parseDouble(intentStatueDetails.getLatitude()), Double.parseDouble(intentStatueDetails.getLongitude())));

            }
        } else {
            Toast.makeText(StatuesMapActivity.this, "List size not matching", Toast.LENGTH_SHORT).show();
        }
    }


    public double calculationByDistance(LatLng StartP, LatLng EndP) {
        int Radius = 6371;// radius of earth in Km
        double lat1 = StartP.latitude;
        double lat2 = EndP.latitude;
        double lon1 = StartP.longitude;
        double lon2 = EndP.longitude;
        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lon2 - lon1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                + Math.cos(Math.toRadians(lat1))
                * Math.cos(Math.toRadians(lat2)) * Math.sin(dLon / 2)
                * Math.sin(dLon / 2);
        double c = 2 * Math.asin(Math.sqrt(a));
        double valueResult = Radius * c;
        double km = valueResult / 1;
        DecimalFormat newFormat = new DecimalFormat("####");
        int kmInDec = Integer.valueOf(newFormat.format(km));
        double meter = valueResult % 1000;
        int meterInDec = Integer.valueOf(newFormat.format(meter));
        Log.i("Radius Value", "" + valueResult + "   KM  " + kmInDec
                + " Meter   " + meterInDec);
        return meter * 1000;
    }

    /* this two methods applied when user comes from dashboard*/

    private void changeNearBYStatueColorGREEN() {
        if (markerMap.size() == apiStatueList.size()) {
            Circle gCircule = circuleMap.get(varStatueDetails);
            gCircule.setFillColor(getResources().getColor(R.color.greenTrans));
            Marker marker = markerMap.get(varStatueDetails);
            if (marker.getTag() == null) {
                marker.setIcon(BitmapDescriptorFactory.fromBitmap(mPresenter.createCustomMarker(StatuesMapActivity.this, null, AppConstants.MARKER_TYPE_GREEN)));
            } else {
                marker.setIcon(BitmapDescriptorFactory.fromBitmap(mPresenter.createCustomMarker(StatuesMapActivity.this, (Bitmap) marker.getTag(), AppConstants.MARKER_TYPE_GREEN)));
            }
            zoomTwoMarkers(currentLatLong, new LatLng(Double.parseDouble(varStatueDetails.getLatitude()), Double.parseDouble(varStatueDetails.getLongitude())));
            if (apiStatueList.size() > 0) {
                if (statueAdapter != null) {
                    varStatueDetails.setInZone("yes");
                    statueAdapter.setItem(apiIndex, varStatueDetails);
                }
            }
            if (!isApiCalled) {
                isApiCalled = true;
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        makeVisitStatueApi();

                    }
                }, 3000);
            }
        }
    }

    private void changeNearBYStatueColorRED() {
        if (markerMap.size() == apiStatueList.size()) {
            Circle gCircule = circuleMap.get(varStatueDetails);
            gCircule.setFillColor(getResources().getColor(R.color.redTrans));
            Marker marker = markerMap.get(varStatueDetails);
            marker.setIcon(BitmapDescriptorFactory.fromBitmap(mPresenter.createCustomMarker(StatuesMapActivity.this, (Bitmap) marker.getTag(), AppConstants.MARKER_TYPE_RED)));
            zoomTwoMarkers(currentLatLong, new LatLng(Double.parseDouble(varStatueDetails.getLatitude()), Double.parseDouble(varStatueDetails.getLongitude())));
            if (apiStatueList.size() > 0) {
                if (statueAdapter != null) {
                    varStatueDetails.setInZone("no");
                    statueAdapter.setItem(apiIndex, varStatueDetails);
                }
            }
            isApiCalled = false;
        }

    }

    private void changeNearBYStatueColorBLACK() {
        if (markerMap.size() == apiStatueList.size()) {
            Circle gCircule = circuleMap.get(varStatueDetails);
            gCircule.setFillColor(getResources().getColor(R.color.redTrans));
            Marker marker = markerMap.get(varStatueDetails);
            marker.setIcon(BitmapDescriptorFactory.fromBitmap(mPresenter.createCustomMarker(StatuesMapActivity.this, (Bitmap) marker.getTag(), AppConstants.MARKER_TYPE_BLACK)));
            if (apiStatueList.size() > 0) {
                if (statueAdapter != null) {
                    varStatueDetails.setInZone("out");
                    statueAdapter.setItem(apiIndex, varStatueDetails);
                }
            }
            isApiCalled = false;
        }

    }


    private void startTrackingDistance(Location location) {
        Log.e("Changed Location", location.getLatitude() + " " + location.getLongitude());
        currentLatLong = new LatLng(location.getLatitude(), location.getLongitude());
        binding.tvDistance.setVisibility(View.GONE);
        binding.tvDistance.setText("0 Meters");
        if (currentLatLong != null) {
            binding.linProgress.setVisibility(View.GONE);
        }
        if (intentStatueDetails != null) {
            if (isPlolyLineDrawn) {
                isPlolyLineDrawn = false;
                mPresenter.getPolyline(currentLatLong, new LatLng(Double.parseDouble(intentStatueDetails.getLatitude()), Double.parseDouble(intentStatueDetails.getLongitude())));
            }
        } else {
            checkNearByStatue();
            if (varStatueDetails != null) {
                double apiDistance = calculationByDistance(currentLatLong, new LatLng(Double.parseDouble(varStatueDetails.getLatitude()), Double.parseDouble(varStatueDetails.getLongitude())));
                double statueRadious = Double.parseDouble(varStatueDetails.getRangeRadius());
                if (isPlolyLineDrawn) {
                    isPlolyLineDrawn = false;
                    if (apiDistance < statueRadious) {
                        mPresenter.getPolyline(currentLatLong, new LatLng(Double.parseDouble(varStatueDetails.getLatitude()), Double.parseDouble(varStatueDetails.getLongitude())));
                        changeNearBYStatueColorGREEN();
                    } else if (apiDistance > statueRadious && apiDistance < statueRadious + 5) {
                        mPresenter.getPolyline(currentLatLong, new LatLng(Double.parseDouble(varStatueDetails.getLatitude()), Double.parseDouble(varStatueDetails.getLongitude())));
                        changeNearBYStatueColorRED();
                    } else if (apiDistance > statueRadious + 5) {
                        singlePolyline.remove();
                        isPlolyLineDrawn = true;
                        changeNearBYStatueColorBLACK();
                    }
                }


                binding.tvDistance.setText("DISTANCE " + apiDistance);

            }

        }
    }


}



