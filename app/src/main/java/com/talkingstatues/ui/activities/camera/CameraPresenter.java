package com.talkingstatues.ui.activities.camera;

import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.talkingstatues.R;
import com.talkingstatues.data.prefs.AppPreferencesHelper;
import com.talkingstatues.model.login.LoginDetails;
import com.talkingstatues.model.login.LoginMaster;
import com.talkingstatues.model.selfie.SelfieMaster;
import com.talkingstatues.networking.API_Params;
import com.talkingstatues.networking.NetworkError;
import com.talkingstatues.networking.NetworkService;
import com.talkingstatues.ui.base.BasePresenter;
import com.talkingstatues.utilities.AppConstants;
import com.talkingstatues.utilities.CommonUtils;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;

import javax.inject.Inject;

import okhttp3.MultipartBody;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

public class CameraPresenter<V extends CameraMvpView> extends BasePresenter<V>
        implements CameraMvpPresenter<V> {
    @Inject
    NetworkService service;

    @Inject
    public CameraPresenter(AppPreferencesHelper preferencesHelper, CompositeSubscription mSubscription) {
        super(preferencesHelper, mSubscription);
    }



}
