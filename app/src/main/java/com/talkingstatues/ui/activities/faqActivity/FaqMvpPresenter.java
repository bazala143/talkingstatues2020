package com.talkingstatues.ui.activities.faqActivity;

import com.talkingstatues.di.PerActivity;
import com.talkingstatues.model.login.LoginDetails;
import com.talkingstatues.ui.activities.statuesList.StatuesListMvpView;
import com.talkingstatues.ui.base.MvpPresenter;
import com.talkingstatues.ui.base.MvpView;

@PerActivity
public interface FaqMvpPresenter<V extends FaqMvpView> extends MvpPresenter<V> {


    LoginDetails getLoginDetails();
    void  getFaqsApi();
    String getLanguageCode();
}
