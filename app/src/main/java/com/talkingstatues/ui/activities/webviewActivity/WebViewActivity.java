
package com.talkingstatues.ui.activities.webviewActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.databinding.DataBindingUtil;


import com.talkingstatues.R;
import com.talkingstatues.databinding.ActivityWebviewBinding;
import com.talkingstatues.ui.base.BaseActivity;
import com.talkingstatues.utilities.CommonUtils;
import com.talkingstatues.utilities.RequestCodes;

import javax.inject.Inject;

import im.delight.android.webview.AdvancedWebView;

public class WebViewActivity extends BaseActivity implements View.OnClickListener, WebViewMvpView, AdvancedWebView.Listener {

    ActivityWebviewBinding binding;
    @Inject
    WebViewMvpPresenter<WebViewMvpView> mPresenter;
    private static final String TAG = WebViewActivity.class.getSimpleName();

    private String url = "";
    private String toolbarName = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivityComponent().inject(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimaryDark));
        }
        binding = DataBindingUtil.setContentView(this, R.layout.activity_webview);
        mPresenter.onAttach(this);


        binding.toolbar.setClickListener(this);
        binding.toolbar.ivback.setVisibility(View.VISIBLE);
        binding.toolbar.tvTitle.setVisibility(View.VISIBLE);
        url = getIntent().getStringExtra("url");
        Log.e("URL", url);
        toolbarName = getIntent().getStringExtra("toolbarName");
        binding.toolbar.tvTitle.setText(toolbarName);
        binding.setClickListener(this);
        binding.toolbar.setClickListener(this);
        binding.webView.setListener(this, this);
        binding.webView.loadUrl(url);


    }


    @Override
    public void onClick(View v) {
        if (v == binding.toolbar.ivback) {
            WebViewActivity.this.finish();
        }
    }


    @Override
    public void onPageStarted(String url, Bitmap favicon) {
        binding.progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onPageFinished(String url) {
        Log.e("DecodedUrl", url);
        binding.progressBar.setVisibility(View.GONE);

    }

    @Override
    public void onPageError(int errorCode, String description, String failingUrl) {

    }

    @Override
    public void onDownloadRequested(String url, String suggestedFilename, String mimeType, long contentLength, String contentDisposition, String userAgent) {

    }

    @Override
    public void onExternalPageRequest(String url) {

    }


    @SuppressLint("NewApi")
    @Override
    protected void onResume() {
        super.onResume();
        binding.webView.onResume();
        // ...
    }

    @SuppressLint("NewApi")
    @Override
    protected void onPause() {
        binding.webView.onPause();
        // ...
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        binding.webView.onDestroy();
        // ...
        super.onDestroy();
        mPresenter.onDetach();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        binding.webView.onActivityResult(requestCode, resultCode, intent);
        // ...
    }

    @Override
    public void onBackPressed() {
        if (!binding.webView.onBackPressed()) {
            return;
        }
        // ...
        super.onBackPressed();
    }
}
