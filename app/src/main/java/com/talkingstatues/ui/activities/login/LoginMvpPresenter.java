package com.talkingstatues.ui.activities.login;

import android.widget.EditText;

import com.talkingstatues.databinding.ActivityLoginBinding;
import com.talkingstatues.di.PerActivity;
import com.talkingstatues.ui.base.MvpPresenter;

@PerActivity
public interface LoginMvpPresenter<V extends LoginMvpView> extends MvpPresenter<V> {
    boolean isValid(ActivityLoginBinding binding);

    void loginApi(ActivityLoginBinding binding);

    void loginWithfacebook(String name, String email);

    void forgotPassword(String email);
}
