package com.talkingstatues.ui.activities.aboutUs;

import android.widget.EditText;

import com.talkingstatues.di.PerActivity;
import com.talkingstatues.ui.activities.placeDetailsActivity.PlaceDetailsMvpView;
import com.talkingstatues.ui.base.MvpPresenter;

@PerActivity
public interface AboutUsMvpPresenter<V extends AboutUsMvpView> extends MvpPresenter<V> {
    boolean isValid(EditText edMobileNumber, EditText edPassword);
    void getAbousUsApi();
    String getLanguageCode();
}
