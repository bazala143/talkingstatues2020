package com.talkingstatues.ui.activities.placeDetailsActivity;

import android.widget.EditText;

import com.talkingstatues.di.PerActivity;
import com.talkingstatues.ui.activities.login.LoginMvpView;
import com.talkingstatues.ui.base.MvpPresenter;

@PerActivity
public interface PlaceDetailsMvpPresenter<V extends PlaceDetailsMvpView> extends MvpPresenter<V> {

    void  getPlaceInfoApi(String placeId);

    String getLangugeCode();
}
