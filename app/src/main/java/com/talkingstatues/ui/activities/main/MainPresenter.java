package com.talkingstatues.ui.activities.main;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.util.Log;
import android.widget.EditText;

import androidx.core.content.res.ResourcesCompat;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.harmis.imagepicker.CameraUtils.CameraIntentHelper;
import com.harmis.imagepicker.CameraUtils.CameraIntentHelperCallback;
import com.harmis.imagepicker.activities.CropImageActivity;
import com.harmis.imagepicker.utils.CommonKeyword;
import com.talkingstatues.BuildConfig;
import com.talkingstatues.R;
import com.talkingstatues.data.prefs.AppPreferencesHelper;
import com.talkingstatues.model.DrawerItem;
import com.talkingstatues.model.city.CityDetails;
import com.talkingstatues.model.city.CityMaster;
import com.talkingstatues.model.login.LoginDetails;
import com.talkingstatues.model.logoDetails.LogoDetails;
import com.talkingstatues.model.selfie.SelfieMaster;
import com.talkingstatues.model.statueDetail.StatueDetailMaster;
import com.talkingstatues.model.statueList.StatueDetails;
import com.talkingstatues.model.statueList.StatueMaster;
import com.talkingstatues.model.visitStatue.VisitStatueMaster;
import com.talkingstatues.networking.API_Params;
import com.talkingstatues.networking.NetworkError;
import com.talkingstatues.networking.NetworkService;
import com.talkingstatues.ui.base.BasePresenter;
import com.talkingstatues.utilities.CommonUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import okhttp3.MultipartBody;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

public class MainPresenter<V extends MainMvpView> extends BasePresenter<V>
        implements MainMvpPresenter<V> {
    @Inject
    NetworkService service;
    private static final String TAG = MainPresenter.class.getSimpleName();
    CameraIntentHelper mCameraIntentHelper;
    GetStatueDetailsAsync getStatueDetailsAsync;


    @Inject
    public MainPresenter(AppPreferencesHelper preferencesHelper, CompositeSubscription mSubscription) {
        super(preferencesHelper, mSubscription);
    }

    @Override
    public boolean isValid(EditText edUsername, EditText edPassword) {
        if (CommonUtils.isEmpty(edUsername.getText().toString())) {
            edUsername.setError(getMvpView().getStringFromId(R.string.text_email));
            edUsername.requestFocus();
            return false;
        } else if (!CommonUtils.checkEmail(edUsername.getText().toString().trim())) {
            edUsername.setError(getMvpView().getStringFromId(R.string.enter_valid_email));
            edUsername.requestFocus();
            return false;
        } else if (CommonUtils.isEmpty(edPassword.getText().toString())) {
            edPassword.setError(getMvpView().getStringFromId(R.string.text_password));
            edPassword.requestFocus();
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void storeLocation(String latitude, String longitude) {
        getPreferencesHelper().setCurrentLatitude(latitude);
        getPreferencesHelper().setCurrentLongitude(longitude);


    }

    @Override
    public void setDefaultCity(CityDetails cityDetails) {
        getPreferencesHelper().setDefaultCity(new Gson().toJson(cityDetails));
    }


    @Override
    public LoginDetails getLoginDetails() {
        return getPreferencesHelper().getLoginModel();
    }


    @Override
    public List<DrawerItem> getDrawerList(Activity activity) {
        List<DrawerItem> drawerItems = new ArrayList<>();
        drawerItems.add(new DrawerItem(R.drawable.ic_contact_us, activity.getString(R.string.text_contact_us)));
        drawerItems.add(new DrawerItem(R.drawable.ic_about_us, activity.getString(R.string.text_about_us)));
        drawerItems.add(new DrawerItem(R.drawable.ic_faq, activity.getString(R.string.text_faq)));
        drawerItems.add(new DrawerItem(R.drawable.ic_rage, activity.getString(R.string.text_rate_app)));
        drawerItems.add(new DrawerItem(R.drawable.ic_share, activity.getString(R.string.text_share_app)));
        drawerItems.add(new DrawerItem(R.drawable.ic_signout, activity.getString(R.string.text_logout)));
        return drawerItems;
    }

    @Override
    public CameraIntentHelper getmCameraIntentHelper(final int whichImage, final Activity activity) {
        mCameraIntentHelper = new CameraIntentHelper(activity, new CameraIntentHelperCallback() {
            @Override
            public void onPhotoUriFound(int requestCode, Date dateCameraIntentStarted, Uri photoUri, int rotateXDegrees) {
                List<String> imagesList = new ArrayList<>();
                imagesList.add(photoUri.getPath());
                Intent intent = new Intent(new Intent(activity, CropImageActivity.class));
                intent.putExtra(CommonKeyword.RESULT, (Serializable) imagesList);
                activity.startActivityForResult(intent, whichImage);
            }

            @Override
            public void deletePhotoWithUri(Uri photoUri) {

            }

            @Override
            public void onSdCardNotMounted() {

            }

            @Override
            public void onCanceled() {

            }

            @Override
            public void onCouldNotTakePhoto() {

            }

            @Override
            public void onPhotoUriNotFound(int requestCode) {

            }

            @Override
            public void logException(Exception e) {
                CommonUtils.pLog("log_tag", "log Exception : " + e.getMessage());
            }
        });
        return mCameraIntentHelper;
    }


    @Override
    public void redirectToPlayStore(Activity activity) {
        final String appPackageName = activity.getPackageName(); // package name of the app
        try {
            activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

    @Override
    public void shareApp(Activity activity) {
        Bitmap myLogo = null;

        if (Build.VERSION.SDK_INT > 22) {
            myLogo = ((BitmapDrawable) ResourcesCompat.getDrawable(activity.getResources(), R.drawable.ic_launcher, null)).getBitmap();
        } else {
            myLogo = BitmapFactory.decodeResource(activity.getResources(), R.drawable.ic_launcher);
        }

        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("image/jpeg");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myLogo.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        File f = new File(Environment.getExternalStorageDirectory() + File.separator + "app_icon.jpg");
        try {
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
        } catch (IOException e) {
            e.printStackTrace();
        }
        share.putExtra(Intent.EXTRA_STREAM, Uri.parse(Environment.getExternalStorageDirectory() + File.separator + "/app_icon.jpg"));
        share.setType("text/plain");
        share.putExtra(Intent.EXTRA_SUBJECT, activity.getString(R.string.app_name));
        String shareMessage = "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID + "\n\n";
        share.putExtra(Intent.EXTRA_TEXT, shareMessage);
        activity.startActivity(Intent.createChooser(share, getMvpView().getStringFromId(R.string.shareAppVia)));
    }

    @Override
    public void getCityList() {
        if (getMvpView().isNetworkConnected()) {
            getMvpView().showLoading();
            Subscription s = service.getCityList(new NetworkService.GetCityListCallback() {
                @Override
                public void onSuccess(CityMaster master) {
                    getMvpView().hideLoading();
                    if (master.getCode().equals("0")) {
                        getMvpView().onCityListLoaded(master.getJson());
                    } else {
                        getMvpView().showMessage(master.getMessage());
                    }

                }

                @Override
                public void onError(NetworkError networkError) {
                    getMvpView().showMessage(networkError.getMessage());
                }
            });
            getSubscription().add(s);
        } else {
            getMvpView().onError(R.string.utils__no_connection);
        }
    }


    @Override
    public void onDetach() {
        if (getSubscription() != null && getSubscription().hasSubscriptions()) {
            getSubscription().clear();
        }
    }

    @Override
    public void setSelfieApi(final String mImage, int userId) {
        if (getMvpView().isNetworkConnected()) {
            getMvpView().showLoading();
            MultipartBody.Part body = CommonUtils.getMultipartImageBody(mImage, API_Params.image);
            Subscription subscription = service.setSelfieApi(userId, body, new NetworkService.GetSetSelfieCallback() {
                @Override
                public void onSuccess(SelfieMaster master) {
                    getMvpView().hideLoading();
                    Log.e("response", new Gson().toJson(master));
                    if (master.getCode().equals("0")) {
                        getMvpView().showMessage(R.string.selfie_uploaded_success);
                        getMvpView().shareSelfie(mImage);
                    } else {
                        getMvpView().onError(R.string.failed_to_upload);

                    }

                }

                @Override
                public void onError(NetworkError networkError) {
                    getMvpView().hideLoading();
                    getMvpView().onError(networkError.getMessage() + "");
                }
            });
            getSubscription().add(subscription);
        } else {
            getMvpView().onError(R.string.utils__no_connection);
        }
    }

    @Override
    public void getSponserLogos(String cityId, Activity activity) {
        LogoDetails logoDetails = new LogoDetails();
        logoDetails.setImageUrl("topslide1541794937.png");
        logoDetails.setTitle(activity.getString(R.string.text_web));
        logoDetails.setSponsorLogoWebUrl("http://www.talkingstatues.com");
        LogoDetails logoDetails1 = new LogoDetails();
        logoDetails1.setImageUrl("topslide1541795189.png");
        logoDetails1.setSponsorLogoWebUrl("http://www.newyorktalkingstatues.com/how-does-it-work.html");
        logoDetails1.setTitle(activity.getString(R.string.text_help));
        LogoDetails logoDetails2 = new LogoDetails();
        logoDetails2.setImageUrl("topslide1541795249.png");
        logoDetails2.setSponsorLogoWebUrl("http://www.talkingstatues.com/contact.html");
        logoDetails2.setTitle(activity.getString(R.string.text_contact));

        List<LogoDetails> list = new ArrayList<>();
        list.add(logoDetails);
        list.add(logoDetails1);
        list.add(logoDetails2);
        getMvpView().onLogosLoaded(list);
    }

    @Override
    public void getAllStatueList() {
        if (getMvpView().isNetworkConnected()) {
            getMvpView().showLoading();
            Subscription s = service.statueList(statueParams(), new NetworkService.GetStatueListCallbackCallback() {
                @Override
                public void onSuccess(StatueMaster master) {
                    getMvpView().hideLoading();
                    if (master.getCode().equals("0")) {
                        Log.e("Json", "Stored");
                        getPreferencesHelper().setStatueJson(new Gson().toJson(master));
                    } else {
                        getMvpView().showMessage(master.getMessage());
                    }

                }

                @Override
                public void onError(NetworkError networkError) {
                    getMvpView().hideLoading();
                    getMvpView().showMessage(R.string.oops);
                }
            });
            getSubscription().add(s);
        } else {
            getMvpView().onError(R.string.utils__no_connection);
        }
    }


    @Override
    public void visitStatueApi(final StatueDetails statueDetails) {
        if (getMvpView().isNetworkConnected()) {
            getMvpView().showLoading();
            Subscription subscription = service.getVisitStatue(visitStatueParams(statueDetails), new NetworkService.GetVisitStatueCallback() {
                @Override
                public void onSuccess(VisitStatueMaster master) {
                    getMvpView().hideLoading();
                    Log.e("response", new Gson().toJson(master));
                    if (master.getCode().equals("0")) {
                        getMvpView().showMessage(R.string.added_to_statue);
                        getMvpView().onStatueVisited(statueDetails);
                    } else {
                        getMvpView().onError(R.string.failed_to_upload);

                    }

                }

                @Override
                public void onError(NetworkError networkError) {
                    getMvpView().hideLoading();
                    getMvpView().onError(networkError.getMessage() + "");
                }
            });
            getSubscription().add(subscription);
        } else {
            getMvpView().onError(R.string.utils__no_connection);
        }
    }

    @Override
    public void setLanguageCode(String language) {
        getPreferencesHelper().setLanguageCode(language);
    }

    @Override
    public String getLanguageCode() {
        return getPreferencesHelper().getLanguageCode();
    }

    @Override
    public void getStatueDetailsApi(String qrCode) {
        if (getMvpView().isNetworkConnected()) {
            getMvpView().showLoading();
            Subscription subscription = service.getStatueDetails(statueDetailsparams(qrCode), new NetworkService.GetStatueDetailsCallback() {
                @Override
                public void onSuccess(StatueDetailMaster master) {
                    getMvpView().hideLoading();
                    Log.e("response", new Gson().toJson(master));
                    if (master.getCode().equals("0")) {
                        //    getMvpView().onStatueVisited(statueDetails);
                        //    visitStatueApi(master.getJson());

                    } else {
                        getMvpView().onError(R.string.failed_to_upload);

                    }

                }

                @Override
                public void onError(NetworkError networkError) {
                    getMvpView().hideLoading();
                    getMvpView().onError(networkError.getMessage() + "");
                }
            });
            getSubscription().add(subscription);
        } else {
            getMvpView().onError(R.string.utils__no_connection);
        }
    }

    @Override
    public String getDefaultCity(double cLat, double cLong, Activity activity) {
        if (getPreferencesHelper().getStoredCity() != null) {
            CityDetails cityDetails = new Gson().fromJson(getPreferencesHelper().getStoredCity(), CityDetails.class);
            getMvpView().onDefaultCityFound(cityDetails.getName());
        } else {
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(activity, Locale.getDefault());
            try {
                addresses = geocoder.getFromLocation(cLat, cLong, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                String city = addresses.get(0).getAddressLine(1);
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                getMvpView().onDefaultCityFound(address);
            } catch (IOException e) {
                e.printStackTrace();
                getMvpView().onDefaualtCityNotFound();
            }
        }


        return null;
    }

    @Override
    public void getStatueDetailsFromPrefs(String qrCode) {
        Log.e("QrCode", qrCode);
        getStatueDetailsAsync = new GetStatueDetailsAsync(qrCode);
        getStatueDetailsAsync.execute();
    }

    @SuppressLint("StaticFieldLeak")
    public class GetStatueDetailsAsync extends AsyncTask<String, Void, StatueDetails> {

        String qrCode;

        public GetStatueDetailsAsync(String qrCode) {
            this.qrCode = qrCode;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            getMvpView().showLoading();
        }

        @Override
        protected void onPostExecute(StatueDetails statueDetails) {
            super.onPostExecute(statueDetails);
            getMvpView().hideLoading();
            if (statueDetails != null) {
                visitStatueApi(statueDetails);
                Log.e("details", statueDetails.getStatueName());
            } else {
                getMvpView().openStatueLInk(qrCode);
            }
        }

        @Override
        protected StatueDetails doInBackground(String... strings) {
            StatueMaster statueMaster = getPreferencesHelper().getStatueModel();
            StatueDetails returnDetails = null;
            for (StatueDetails statueDetails : statueMaster.getJson()) {
                if (statueDetails.getQrCode() != null) {
                    if (qrCode.equalsIgnoreCase(statueDetails.getQrCode()) || statueDetails.getQrCode().contains(qrCode)) {
                        returnDetails = statueDetails;
                        Log.e("Matched", "English qr code "+statueDetails.getQrCode());
                        break;
                    }
                }

                if (statueDetails.getSpanishQrCode() != null) {
                    if (qrCode.equalsIgnoreCase(statueDetails.getSpanishQrCode()) || statueDetails.getSpanishQrCode().contains(qrCode)) {
                        returnDetails = statueDetails;
                        Log.e("Matched", "Spanish qr code "+statueDetails.getSpanishQrCode());
                        break;
                    }
                }

            }
            return returnDetails;
        }
    }


    @Override
    public void setLoginDetails(String s) {
        getPreferencesHelper().setLoginDetails(null);
        getPreferencesHelper().setDefaultCity(null);
    }

    private JsonObject statueDetailsparams(String qrCode) {
        JsonObject gsonObject = new JsonObject();
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(API_Params.qr_code, qrCode);
            jsonObject.put(API_Params.user_id, getPreferencesHelper().getLoginModel().getId() + "");
            JsonParser parser = new JsonParser();
            gsonObject = (JsonObject) parser.parse(jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return gsonObject;
    }

    private JsonObject visitStatueParams(StatueDetails statueDetails) {
        JsonObject gsonObject = new JsonObject();
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(API_Params.statue_id, statueDetails.getStatueId());
            jsonObject.put(API_Params.user_id, getPreferencesHelper().getLoginModel().getId() + "");
            JsonParser parser = new JsonParser();
            gsonObject = (JsonObject) parser.parse(jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return gsonObject;
    }

    private JsonObject statueParams() {
        JsonObject gsonObject = new JsonObject();
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(API_Params.user_id, getPreferencesHelper().getLoginModel().getId() + "");
            JsonParser parser = new JsonParser();
            gsonObject = (JsonObject) parser.parse(jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return gsonObject;
    }


}



