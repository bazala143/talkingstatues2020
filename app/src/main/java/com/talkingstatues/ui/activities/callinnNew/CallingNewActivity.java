
package com.talkingstatues.ui.activities.callinnNew;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;

import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.talkingstatues.R;
import com.talkingstatues.databinding.ActivityCallingBinding;
import com.talkingstatues.model.statueList.StatueDetails;
import com.talkingstatues.networking.APIUrl;
import com.talkingstatues.ui.base.BaseActivity;
import com.talkingstatues.utilities.AppConstants;

import javax.inject.Inject;

public class CallingNewActivity extends BaseActivity implements View.OnClickListener, CallingNewMvpView{

    ActivityCallingBinding binding;
    @Inject
    CallingNewMvpPresenter<CallingNewMvpView> mPresenter;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_calling);
        binding.setClickListener(this);
    //    mPresenter.onAttach(this);


    }



    @Override
    protected void onDestroy() {
     //   mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        if (v == binding.ivAccept) {

        } else if (v == binding.ivDecline) {
            finish();
        } else if (v == binding.toolbar.ivback) {
             finish();
        }

    }




}
