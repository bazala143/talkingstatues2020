
package com.talkingstatues.ui.activities.statueDetails;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.SeekBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.YouTubePlayerFullScreenListener;
import com.talkingstatues.R;
import com.talkingstatues.databinding.ActivityStatueDetailsBinding;
import com.talkingstatues.model.statueList.StatueDetails;
import com.talkingstatues.networking.APIUrl;
import com.talkingstatues.ui.activities.VideoPlayerActivity;
import com.talkingstatues.ui.activities.statuesMap.StatuesMapActivity;
import com.talkingstatues.ui.base.BaseActivity;
import com.talkingstatues.utilities.AppConstants;
import com.talkingstatues.utilities.RequestCodes;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

//import wseemann.media.FFmpegMediaMetadataRetriever;

//import wseemann.media.FFmpegMediaMetadataRetriever;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

public class StatueDetailsActivity extends BaseActivity implements View.OnClickListener, StatueDetailsMvpView,
        View.OnTouchListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnBufferingUpdateListener {

    ActivityStatueDetailsBinding binding;
    @Inject
    StatueDetailsMvpPresenter<StatueDetailsMvpView> mPresenter;
    private static final String TAG = StatueDetailsActivity.class.getSimpleName();
    private MediaPlayer mediaPlayer;
    private int mediaFileLengthInMilliseconds; // this value contains the song
    private final Handler handler = new Handler();
    String audioUrl;
    StatueDetails statueDetails;
    String languageCode;
    String videoId = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivityComponent().inject(this);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_statue_details);
        binding.setClickListener(this);
        mPresenter.onAttach(this);
        binding.toolbar.setClickListener(this);
        binding.toolbar.ivback.setVisibility(View.VISIBLE);
        binding.toolbar.tvTitle.setVisibility(View.VISIBLE);
        binding.toolbar.tvTitle.setText(R.string.app_name);
        statueDetails = (StatueDetails) getIntent().getExtras().get(AppConstants.Object);
        languageCode = mPresenter.getLanguageCode();
        initilizeUI();
        setdata();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(videoId != null){
            binding.youtubePlayerView.exitFullScreen();
        }
    }

    private void setdata() {

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.ic_place_holder);
        Log.e("imageUrl", APIUrl.IMAGE_URL + statueDetails.getImage());

        Glide.with(binding.getRoot().getContext())
                .applyDefaultRequestOptions(requestOptions)
                .load(APIUrl.IMAGE_URL + statueDetails.getImage())
                .into(binding.ivStatueImage);
        if(statueDetails.getTemplate() != null){
            if (languageCode.equalsIgnoreCase(AppConstants.LANGUAGE_SPANISH)) {

                if (statueDetails.getTemplate().getSpanishTemplatesDescription() != null) {
                    binding.tvDetails.setText(statueDetails.getTemplate().getSpanishTemplatesDescription());
                } else {
                    binding.tvDetails.setText(statueDetails.getTemplate().getTemplatesDescription());
                }

                assert statueDetails.getTemplate() != null;
                if (statueDetails.getTemplate().getSpanishTemplatesName() != null) {
                    binding.tvTemplateName.setText(statueDetails.getTemplate().getSpanishTemplatesName());
                } else {
                    binding.tvTemplateName.setText(statueDetails.getTemplate().getTemplatesName());
                }

                if (statueDetails.getTemplate().getSpanishAudioUrl() != null) {
                    audioUrl = APIUrl.AUDIO_URL + statueDetails.getTemplate().getSpanishAudioUrl();
                } /*else {
                audioUrl = APIUrl.AUDIO_URL + statueDetails.getTemplate().getAudioUrl();
            }*/

                if (statueDetails.getTemplate().getSpanishVideoUrl() != null) {
                    videoId = statueDetails.getTemplate().getSpanishVideoUrl().substring(statueDetails.getTemplate().getSpanishVideoUrl().lastIndexOf('/') + 1).trim();
                } else {
                    videoId = statueDetails.getTemplate().getVideoUrl().substring(statueDetails.getTemplate().getVideoUrl().lastIndexOf('/') + 1).trim();
                }
                if (statueDetails.getTemplate().getSpanishVideoDescription() != null) {
                    binding.tvVideoDecsription.setText(statueDetails.getTemplate().getSpanishVideoDescription());
                } else {
                    binding.tvVideoDecsription.setText(statueDetails.getTemplate().getVideoDescription());
                }

            } else {
                binding.tvDetails.setText(statueDetails.getTemplate().getTemplatesDescription());
                binding.tvTemplateName.setText(statueDetails.getTemplate().getTemplatesName());
                binding.tvVideoDecsription.setText(statueDetails.getTemplate().getVideoDescription());
                if(statueDetails.getTemplate() != null && statueDetails.getTemplate().getAudioUrl() != null){
                    audioUrl = APIUrl.AUDIO_URL + statueDetails.getTemplate().getAudioUrl();
                }
                videoId = statueDetails.getTemplate().getVideoUrl().substring(statueDetails.getTemplate().getVideoUrl().lastIndexOf('/') + 1).trim();
            }

            Glide.with(binding.getRoot().getContext())
                    .applyDefaultRequestOptions(requestOptions)
                    .load(APIUrl.IMAGE_URL + statueDetails.getTemplate().getTemplatesImage())
                    .into(binding.tvTemplateImage);
           // final String finalVideoId = videoId;
            binding.youtubePlayerView.addYouTubePlayerListener(new AbstractYouTubePlayerListener() {
                @Override
                public void onReady(@NonNull YouTubePlayer youTubePlayer) {
                    youTubePlayer.loadVideo(videoId, 0);
                    youTubePlayer.pause();
                }
            });
        }

        if(audioUrl == null){
            binding.linAudio.setVisibility(View.GONE);
        }

       binding.youtubePlayerView.addFullScreenListener(new YouTubePlayerFullScreenListener() {
            @Override
            public void onYouTubePlayerEnterFullScreen() {
               Log.e("FullScreeenMode","ON");

               Intent i = new Intent(StatueDetailsActivity.this, VideoPlayerActivity.class);
               i.putExtra("videoId",videoId);
               startActivity(i);
            }

            @Override
            public void onYouTubePlayerExitFullScreen() {
                Log.e("FullScreeenMode","OFF");
            }
        });
          if(audioUrl != null){
           //   mediaFileLengthInMilliseconds = getDurationInMilliseconds(audioUrl);
          }
    }


    private void initilizeUI() {
        binding.btnPlayPause.setOnClickListener(this);
        binding.seekbar.setMax(99);
        binding.seekbar.setOnTouchListener(this);
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setOnBufferingUpdateListener(this);
        mediaPlayer.setOnCompletionListener(this);
    }

    private void primarySeekBarProgressUpdater() {
        binding.seekbar.setProgress((int) (((float) mediaPlayer
                .getCurrentPosition() / mediaFileLengthInMilliseconds) * 100));
        binding.tvCurrentDuration.setText(getFormatedTime(mediaPlayer.getCurrentPosition()));
        if (mediaPlayer.isPlaying()) {
            Runnable notification = new Runnable() {
                public void run() {
                    primarySeekBarProgressUpdater();
                }
            };
            handler.postDelayed(notification, 1000);
        }
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    //    binding.youtubePlayerView.release();
        if (mediaPlayer != null) {
            mediaPlayer.stop();
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
   //     binding.youtubePlayerView.release();
        if (mediaPlayer != null) {
            mediaPlayer.stop();
        }
    }

    @Override
    public void onClick(View v) {
        if (v == binding.toolbar.ivback) {
            finish();
        }
        if (v.getId() == R.id.btnPlayPause) {
           if(audioUrl != null){
               binding.progressBar.setVisibility(View.VISIBLE);
               binding.btnPlayPause.setVisibility(View.GONE);
               Log.e("Total Duration", mediaFileLengthInMilliseconds + "");
               Log.e("audioUrl", audioUrl);
               try {
                   mediaPlayer.setDataSource(audioUrl);
                   mediaPlayer.prepare();
               } catch (Exception e) {
                   e.printStackTrace();
               }
               mediaFileLengthInMilliseconds = mediaPlayer.getDuration();
               binding.tvTotalDuration.setText(getFormatedTime(mediaFileLengthInMilliseconds));
               if (!mediaPlayer.isPlaying()) {
                   mediaPlayer.start();
                   binding.progressBar.setVisibility(View.GONE);
                   binding.btnPlayPause.setVisibility(View.VISIBLE);
                   binding.btnPlayPause.setImageResource(R.drawable.ic_pause);

               } else {
                   mediaPlayer.pause();
                   binding.btnPlayPause.setImageResource(R.drawable.ic_play);
               }
               primarySeekBarProgressUpdater();
           }else{
               Toast.makeText(this, "Unable to get audio", Toast.LENGTH_SHORT).show();
           }
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        mediaPlayer.stop();
        this.finish();
    }

    @Override
    public void onGetLogs(String whichApi, String errorMsg) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RequestCodes.REQUEST_MAIN_ACTIVITY && resultCode == RESULT_OK) {
            setResult(RESULT_OK);
            finish();
        }
    }

    @Override
    public void onBufferingUpdate(MediaPlayer mp, int percent) {
        binding.seekbar.setSecondaryProgress(percent);
    }

    @Override
    public void onCompletion(MediaPlayer mp) {

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (v.getId() == R.id.seekbar) {
            if (mediaPlayer.isPlaying()) {
                SeekBar sb = (SeekBar) v;
                int playPositionInMillisecconds = (mediaFileLengthInMilliseconds / 100)
                        * sb.getProgress();
                mediaPlayer.seekTo(playPositionInMillisecconds);
            }
        }
        return false;
    }

    private String getFormatedTime(int fileLenght) {
        @SuppressLint("DefaultLocale") String time = String.format("%02d:%02d",
                MILLISECONDS.toMinutes(fileLenght),
                MILLISECONDS.toSeconds(fileLenght) -
                        TimeUnit.MINUTES.toSeconds(MILLISECONDS.toMinutes(fileLenght))
        );

        return time;
    }


   /*private int getDurationInMilliseconds(String path) {
        FFmpegMediaMetadataRetriever mmr = new FFmpegMediaMetadataRetriever();
        mmr.setDataSource(path);
        int duration = Integer.parseInt(mmr.extractMetadata(FFmpegMediaMetadataRetriever.METADATA_KEY_DURATION));
        Log.e("Duration", duration + "");
        mmr.release();
        return duration;
    }*/


}
