package com.talkingstatues.ui.activities.faqActivity;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.talkingstatues.R;
import com.talkingstatues.data.prefs.AppPreferencesHelper;
import com.talkingstatues.model.banner.BannerMaster;
import com.talkingstatues.model.faqList.FaqMaster;
import com.talkingstatues.model.login.LoginDetails;
import com.talkingstatues.networking.API_Params;
import com.talkingstatues.networking.NetworkError;
import com.talkingstatues.networking.NetworkService;
import com.talkingstatues.ui.activities.statuesList.StatuesListMvpPresenter;
import com.talkingstatues.ui.activities.statuesList.StatuesListMvpView;
import com.talkingstatues.ui.base.BasePresenter;
import com.talkingstatues.utilities.AppConstants;

import org.json.JSONException;
import org.json.JSONObject;

import javax.inject.Inject;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

public class FaqPresenter<V extends FaqMvpView> extends BasePresenter<V>
        implements FaqMvpPresenter<V> {
    @Inject
    NetworkService service;
    private static final String TAG = FaqPresenter.class.getSimpleName();

    @Inject
    public FaqPresenter(AppPreferencesHelper preferencesHelper, CompositeSubscription mSubscription) {
        super( preferencesHelper, mSubscription );
    }





    @Override
    public LoginDetails getLoginDetails() {
        return getPreferencesHelper().getLoginModel();
    }

    @Override
    public void getFaqsApi() {
        if (getMvpView().isNetworkConnected()) {
            getMvpView().showLoading();
            Subscription s = service.faqList(new NetworkService.GetFaqListCallback() {
                @Override
                public void onSuccess(FaqMaster master) {
                    Log.e("response",new Gson().toJson(master));
                    getMvpView().hideLoading();
                    if (master.getCode().equals("0")) {
                        getMvpView().onFaqLoaded(master.getJson());
                    } else {
                        getMvpView().showMessage(master.getMessage());
                    }

                }

                @Override
                public void onError(NetworkError networkError) {
                    getMvpView().showMessage(networkError.getLocalizedMessage());
                }
            });
            getSubscription().add(s);
        } else {
            getMvpView().onError(R.string.utils__no_connection);
        }
    }

    @Override
    public String getLanguageCode() {
        return getPreferencesHelper().getLanguageCode();
    }


    @Override
    public void onDetach() {
        if (getSubscription() != null && getSubscription().hasSubscriptions()) {
            getSubscription().clear();
        }
    }
}
