package com.talkingstatues.ui.activities.webviewActivity;

import com.talkingstatues.di.PerActivity;
import com.talkingstatues.ui.base.MvpPresenter;

@PerActivity
public interface WebViewMvpPresenter<V extends WebViewMvpView> extends MvpPresenter<V> {

}
