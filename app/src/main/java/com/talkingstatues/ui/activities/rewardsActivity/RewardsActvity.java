
package com.talkingstatues.ui.activities.rewardsActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.talkingstatues.R;
import com.talkingstatues.databinding.ActivityRewardsListBinding;
import com.talkingstatues.model.login.LoginDetails;
import com.talkingstatues.model.redeem.RedeemMaster;
import com.talkingstatues.model.rewardsImage.RewardsDetails;
import com.talkingstatues.model.statueList.StatueDetails;
import com.talkingstatues.networking.API_Params;
import com.talkingstatues.ui.activities.redeemReward.RedeemRewardActivity;
import com.talkingstatues.ui.base.BaseActivity;
import com.talkingstatues.utilities.RequestCodes;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class RewardsActvity extends BaseActivity implements View.OnClickListener, RewardsMvpView {

    ActivityRewardsListBinding binding;
    @Inject
    RewardsMvpPresenter<RewardsMvpView> mPresenter;
    LoginDetails loginDetails;
    private static final String TAG = RewardsActvity.class.getSimpleName();
    private Activity activity;
    RewardsAdapter rewardsAdapter;
    String cityId = "";
    List<StatueDetails> rewsrdList = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivityComponent().inject(this);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_rewards_list);
        binding.setClickListener(this);
        binding.toolbar.setClickListener(this);
        binding.toolbar.ivback.setVisibility(View.VISIBLE);
        binding.toolbar.tvTitle.setVisibility(View.VISIBLE);
        binding.toolbar.tvTitle.setText(R.string.app_name);
        mPresenter.onAttach(this);
        activity = RewardsActvity.this;
        Intent i = getIntent();
        cityId = i.getExtras().getString(API_Params.city_id);
        mPresenter.getStatueList(cityId);


    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        if (v == binding.toolbar.ivback) {
            finish();
        } else if (v == binding.tvGetReward) {
            Log.e("size", rewsrdList.size() + "");
            if (rewsrdList.size() >= 10) {
                Intent i = new Intent(RewardsActvity.this, RedeemRewardActivity.class);
                i.putExtra(API_Params.city_id, cityId);
                startActivity(i);
            } else {
                Toast.makeText(RewardsActvity.this, "" + getResources().getString(R.string.minimun_ten), Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void setLoginDetails(LoginDetails loginDetails) {
        this.loginDetails = loginDetails;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RequestCodes.REQUEST_MAIN_ACTIVITY && resultCode == RESULT_OK) {
            setResult(RESULT_OK);
            finish();
        }
    }

    @Override
    public void onRewardRedeemed(RedeemMaster redeemMaster) {

    }

    @Override
    public void onStatueListLoaded(List<StatueDetails> json) {

        List<StatueDetails> filteredList = new ArrayList<>();
        for (StatueDetails statueDetails : json) {
            if (statueDetails.getIsVisited()) {
                filteredList.add(statueDetails);
            }
        }
        rewsrdList = filteredList;
        rewardsAdapter = new RewardsAdapter(activity, filteredList);
        binding.rvRewardsList.setHasFixedSize(true);
        binding.rvRewardsList.setAdapter(rewardsAdapter);
    }
}
