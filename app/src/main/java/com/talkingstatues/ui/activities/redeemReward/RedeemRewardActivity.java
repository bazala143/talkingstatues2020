
package com.talkingstatues.ui.activities.redeemReward;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.Glide;
import com.talkingstatues.R;
import com.talkingstatues.databinding.ActivityRedeemRewardBinding;
import com.talkingstatues.model.getReward.GetRewardMaster;
import com.talkingstatues.model.redeem.RedeemMaster;
import com.talkingstatues.networking.APIUrl;
import com.talkingstatues.networking.API_Params;
import com.talkingstatues.ui.activities.main.MainActvity;
import com.talkingstatues.ui.base.BaseActivity;
import com.talkingstatues.utilities.AppConstants;
import com.talkingstatues.utilities.CommonUtils;

import javax.inject.Inject;

public class RedeemRewardActivity extends BaseActivity implements View.OnClickListener, RedeemRewardMvpView {

    ActivityRedeemRewardBinding binding;
    @Inject
    RedeemRewardMvpPresenter<RedeemRewardMvpView> mPresenter;
    private static final String TAG = RedeemRewardActivity.class.getSimpleName();

    private String cityId = "";
    private String toolbarName = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivityComponent().inject(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimaryDark));
        }
        binding = DataBindingUtil.setContentView(this, R.layout.activity_redeem_reward);
        mPresenter.onAttach(this);
        binding.toolbar.setClickListener(this);
        binding.toolbar.ivback.setVisibility(View.VISIBLE);
        binding.toolbar.tvTitle.setVisibility(View.VISIBLE);
        cityId = getIntent().getStringExtra(API_Params.city_id);
        Log.e("URL", cityId);
        binding.toolbar.tvTitle.setText(R.string.app_name);
        binding.setClickListener(this);
        binding.toolbar.setClickListener(this);
        mPresenter.getRewardDetails(cityId);

    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        if (v == binding.toolbar.ivback) {
            RedeemRewardActivity.this.finish();
        } else if (v == binding.tvGetReward) {
            mPresenter.showConformationDialog(RedeemRewardActivity.this, cityId);
        }
    }


    @Override
    public void onRewardRedeemed(RedeemMaster master) {
        if (master.getStatus().equals("success") && master.getMessage().equals("Successfully Inserted")) {
            Toast.makeText(this, "" + getString(R.string.reward_collected), Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this, MainActvity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            RedeemRewardActivity.this.finish(); //        } else {
        } else {
            if (master.getMessage().equals("Already Inserted")) {
                Toast.makeText(this, "" + getString(R.string.reward_already_collected), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "" + master.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }


    }

    @Override
    public void onRewardDetails(GetRewardMaster master) {
        if (CommonUtils.LanguageCode.equalsIgnoreCase(AppConstants.LANGUAGE_ENGLISH)) {
            binding.tvTopText.setText(master.getJson().get(0).getTopText());
            binding.tvBottomText.setText(master.getJson().get(0).getBottomText());
            Glide.with(this).load(APIUrl.IMAGE_URL+ master.getJson().get(0).getRewardImage()).into(binding.trophyImage);
        } else {
            if (master.getJson().get(0).getSpanishTopText() != null) {
                binding.tvTopText.setText(master.getJson().get(0).getSpanishTopText());
            } else {
                binding.tvTopText.setText(master.getJson().get(0).getTopText());

            }
            if (master.getJson().get(0).getSpanishBottomText() != null) {
                binding.tvBottomText.setText(master.getJson().get(0).getSpanishBottomText());
            } else {
                binding.tvBottomText.setText(master.getJson().get(0).getBottomText());

            }
            if (master.getJson().get(0).getSpanishRewardImage() != null) {
                Glide.with(this).load(APIUrl.IMAGE_URL+ master.getJson().get(0).getSpanishRewardImage()).into(binding.trophyImage);
            } else {
                Glide.with(this).load(APIUrl.IMAGE_URL+ master.getJson().get(0).getRewardImage()).into(binding.trophyImage);
            }
        }

    }
}
