package com.talkingstatues.ui.base;

import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.google.android.material.snackbar.Snackbar;
import com.talkingstatues.MvpApp;
import com.talkingstatues.R;
import com.talkingstatues.broadcastReceiver.AlarmReceiver;
import com.talkingstatues.di.component.ActivityComponent;
import com.talkingstatues.di.component.DaggerActivityComponent;
import com.talkingstatues.di.module.ActivityModule;
import com.talkingstatues.networking.NetworkModule;
import com.talkingstatues.utilities.CommonUtils;
import com.talkingstatues.utilities.LocaleHelper;
import com.talkingstatues.utilities.NetworkUtils;

import java.io.File;

import static android.net.ConnectivityManager.RESTRICT_BACKGROUND_STATUS_DISABLED;
import static android.net.ConnectivityManager.RESTRICT_BACKGROUND_STATUS_ENABLED;

/**
 * Created by Paras Andani on 22/09/18.
 */

public abstract class BaseActivity extends AppCompatActivity
        implements MvpView, BaseFragment.Callback {

    private ProgressDialog mProgressDialog;

    private ActivityComponent mActivityComponent;
    private PendingIntent pendingIntent;
    private static final String TAG = BaseActivity.class.getSimpleName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        File cacheFile = new File(getCacheDir(), "responses");
        mActivityComponent = DaggerActivityComponent.builder()
                .activityModule(new ActivityModule(this))
                .networkModule(new NetworkModule(cacheFile))
                .applicationComponent(((MvpApp) getApplication()).getComponent())
                .build();

        /* Retrieve a PendingIntent that will perform a broadcast */
        Intent alarmIntent = new Intent(BaseActivity.this, AlarmReceiver.class);
        pendingIntent = PendingIntent.getBroadcast(BaseActivity.this, 0, alarmIntent, 0);
    }

    public ActivityComponent getActivityComponent() {
        return mActivityComponent;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(LocaleHelper.onAttach(newBase));
    }

    @TargetApi(Build.VERSION_CODES.M)
    public void requestPermissionsSafely(String[] permissions, int requestCode) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permissions, requestCode);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    public boolean hasPermission(String permission) {
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.M ||
                checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED;
    }



    @Override
    public void showLoading() {
        hideLoading();
        mProgressDialog = CommonUtils.showLoadingDialog(this);
    }

    @Override
    public void hideLoading() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.cancel();
        }
    }

    private void showSnackBar(String message) {
        Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content),
                message, Snackbar.LENGTH_SHORT);
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView
                .findViewById(R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(this, R.color.white));
        snackbar.show();
    }

    @Override
    public void onError(String message) {
        if (message != null) {
            showSnackBar(message);
        } else {
            showSnackBar(getString(R.string.some_error));
        }
    }

    @Override
    public void onError(@StringRes int resId) {
        onError(getString(resId));
    }

    @Override
    public void showMessage(String message) {
        if (message != null) {
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, getString(R.string.some_error), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void showMessage(@StringRes int resId) {
        showMessage(getString(resId));
    }

    @Override
    public String getStringFromId(@StringRes int resId) {
        return getString(resId);
    }

    @Override
    public boolean isNetworkConnected() {
        return NetworkUtils.isNetworkConnected(getApplicationContext());
    }

    @Override
    public void onFragmentAttached() {

    }

    @Override
    public void onFragmentDetached(String tag) {

    }

    public void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)
                    getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public void startBackgroundService() {
        /*if (!isMyServiceRunning( MyService.class )) {
            Intent intent = new Intent( this, MyService.class );
            startService( intent );
            CommonUtils.pLog( "ad", "service started" );
        } else {
            CommonUtils.pLog( "ad", "service running" );
        }*/

        AlarmManager manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        int interval = 8000;
        //manager.setInexactRepeating(AlarmManager.RTC_WAKEUP, time, interval, pendingIntent);
        manager.setInexactRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + 8000, interval, pendingIntent);
        CommonUtils.pLog(TAG, "service started");
    }


    public void stopBackgroundService() {
        AlarmManager manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        manager.cancel(pendingIntent);
        CommonUtils.pLog(TAG, "service stoped");
    }

    public boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                CommonUtils.pLog("Service already", "running");
                return true;
            }
        }
        CommonUtils.pLog("Service not", "running");
        return false;
    }



    public boolean checkBackgroundDataRestricted() {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

                switch (connMgr.getRestrictBackgroundStatus()) {
                    case RESTRICT_BACKGROUND_STATUS_ENABLED:
                        // Background data usage and push notifications are blocked for this app
                        return true;

                    case ConnectivityManager.RESTRICT_BACKGROUND_STATUS_WHITELISTED:
                    case RESTRICT_BACKGROUND_STATUS_DISABLED:
                        // Data Saver is disabled or the app is whitelisted
                        return false;
                }
            }
        } catch (Exception e) {
            CommonUtils.pLog(TAG, "Check Data saver permission error : " + e.getMessage());
        }
        return false;
    }

    public void showDataSaverPermission() {
        if (checkBackgroundDataRestricted()) {
            CommonUtils.pLog(TAG, "data saver permission required.");
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.permission_data_saver_alert_dialog_title);
            builder.setMessage(R.string.permission_data_saver_dialog_message);
            builder.setPositiveButton(R.string.text_ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    Intent intent = new Intent(Settings.ACTION_IGNORE_BACKGROUND_DATA_RESTRICTIONS_SETTINGS, Uri.parse("package:" + getPackageName()));
                    startActivity(intent);
                }
            });
            builder.show();
        } else {
            CommonUtils.pLog(TAG, "data saver permission doesn't required.");
        }
        /*if (!isMyServiceRunning(MyService.class)) {
            String xiaomi = "Xiaomi";
            final String CALC_PACKAGE_NAME = "com.miui.securitycenter";
            final String CALC_PACKAGE_ACITIVITY = "com.miui.permcenter.autostart.AutoStartManagementActivity";
            String deviceManufacturer = android.os.Build.MANUFACTURER;
            if (deviceManufacturer.equalsIgnoreCase(xiaomi)) {
                try {
                    Intent intent = new Intent();
                    intent.setComponent(new ComponentName(CALC_PACKAGE_NAME, CALC_PACKAGE_ACITIVITY));
                    startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    CommonUtils.pLog("TAG", "Failed to launch AutoStart Screen " + e.getMessage());
                } catch (Exception e) {
                    CommonUtils.pLog("TAG", "Failed to launch AutoStart Screen " + e.getMessage());
                }
            }
        }*/
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
