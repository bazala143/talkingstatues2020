package com.talkingstatues.ui.dialog.forgotDialoge;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.talkingstatues.R;
import com.talkingstatues.utilities.CommonUtils;


public class DialogSuccess {
    Activity activity;

    public DialogSuccess(Activity activity) {
        this.activity = activity;
    }

    Dialog dialog;
    DialogSuccessClickListener dialogSuccessClickListener;

    public void showDialog(final DialogSuccessClickListener dialogSuccessClickListener) {
        try {
            dialog = new Dialog(activity);
            this.dialogSuccessClickListener = dialogSuccessClickListener;

            LayoutInflater layoutInflater = activity.getLayoutInflater();
            View v = layoutInflater.inflate(R.layout.dialog_success, null);

            dialog.setContentView(v);
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(dialog.getWindow().getAttributes());
            lp.width = (int) (getDisplayWidth(activity) * .8);
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.gravity = Gravity.CENTER;
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow().setAttributes(lp);

            TextView tvClose = (TextView) v.findViewById(R.id.tvCancel);
            TextView tvOk = (TextView) v.findViewById(R.id.tvOk);
            final EditText edEmail = (EditText) v.findViewById(R.id.edEmail);

            tvClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialogSuccessClickListener.onCloseClick(dialog);
                }
            });
            tvOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (CommonUtils.checkEmail(edEmail.getText().toString().trim())) {
                        dialogSuccessClickListener.onOkClick(dialog,edEmail.getText().toString().trim());
                    } else {
                        edEmail.setError(activity.getResources().getString(R.string.please_enter_valid_email));
                    }
                }
            });

            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static int getDisplayWidth(Context context) {
        Activity activity = (Activity) context;
        if (Integer.valueOf(Build.VERSION.SDK_INT) < 13) {
            Display display = activity.getWindowManager()
                    .getDefaultDisplay();
            return display.getWidth();
        } else {
            Display display = activity.getWindowManager()
                    .getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            return size.x;
        }
    }
}
