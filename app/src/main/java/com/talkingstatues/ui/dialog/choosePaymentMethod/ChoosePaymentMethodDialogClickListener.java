package com.talkingstatues.ui.dialog.choosePaymentMethod;

public interface ChoosePaymentMethodDialogClickListener {
    void onCashSelected();

    void onMachineSelected();
}
