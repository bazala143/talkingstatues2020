package com.talkingstatues.ui.dialog.choosePaymentMethod;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;

import androidx.databinding.DataBindingUtil;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.talkingstatues.R;
import com.talkingstatues.databinding.DialogChoosePaymentMethodBinding;
import com.talkingstatues.utilities.CommonUtils;

public class ChoosePaymentMethodDialog {
    Activity activity;

    public ChoosePaymentMethodDialog() {

    }

    BottomSheetDialog dialog;
    ChoosePaymentMethodDialogClickListener dialogClickListener;
    DialogChoosePaymentMethodBinding binding;



    public void showDialog(Activity activity, ChoosePaymentMethodDialogClickListener listener) {
        try {
            this.activity = activity;

            dialogClickListener = listener;
            dialog = new BottomSheetDialog(activity);

            LayoutInflater layoutInflater = activity.getLayoutInflater();
            binding = DataBindingUtil.inflate(layoutInflater, R.layout.dialog_choose_payment_method, null, false);
            dialog.setContentView(binding.getRoot());

            binding.tvCash.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    dialogClickListener.onCashSelected();
                }
            });

            binding.tvCreditCardOnDelivery.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    dialogClickListener.onMachineSelected();
                }
            });

            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtils.pLog("dialog", e.getMessage());
        }
    }


    public void dismiss() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }
}
