package com.talkingstatues.ui.dialog.forgotDialoge;

import android.app.Dialog;

public interface DialogSuccessClickListener {
    void onCloseClick(Dialog dialog);
    void onOkClick(Dialog dialog,String email);
}
