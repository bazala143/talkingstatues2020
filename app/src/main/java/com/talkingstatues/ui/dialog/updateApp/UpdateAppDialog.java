package com.talkingstatues.ui.dialog.updateApp;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fusedbulblib.interfaces.DialogClickListener;
import com.talkingstatues.R;


/**
 * Created by AnkurYadav on 23-09-2017.
 */

public class UpdateAppDialog {

    Activity activity;

    public UpdateAppDialog(Activity context) {
        this.activity = context;
    }

    AlertDialog.Builder alertDialog;
    Dialog dialog;
    DialogClickListener dialogClickListner;

    public void showDialog(DialogClickListener listner) {
        try {
            dialogClickListner = listner;
            alertDialog = new AlertDialog.Builder(activity);
            alertDialog.setCancelable(false);
            LayoutInflater inflater = activity.getLayoutInflater();
          //  View v = inflater.inflate(R.layout.update_app_dialog, null);
            View v =null;

            alertDialog.setView(v);
            if (dialog == null) {
                dialog = alertDialog.create();
            }
            LinearLayout positive_button = (LinearLayout) v.findViewById(R.id.positive_button);
            LinearLayout negative_button = (LinearLayout) v.findViewById(R.id.negative_button);

            TextView title = (TextView) v.findViewById(R.id.title);
            TextView message = (TextView) v.findViewById(R.id.message);

            TextView negative_txt = (TextView) v.findViewById(R.id.negative_txt);
            TextView positive_txt = (TextView) v.findViewById(R.id.positive_txt);

            positive_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialogClickListner.positiveListener(activity, dialog);

                    /*Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    activity.startActivity(intent);
                    gpsOnHandler();*/


                }
            });

            negative_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialogClickListner.negativeListener(activity, dialog);
                    // gpsOnListener.gpsPermissionDenied(1);
                }
            });

            dialog.show();


        } catch (Exception e) {

        }

    }

    int count = 0;
}
