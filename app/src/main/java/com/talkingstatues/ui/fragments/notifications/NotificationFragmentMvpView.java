package com.talkingstatues.ui.fragments.notifications;



import com.talkingstatues.model.notification.NotificationDetails;
import com.talkingstatues.ui.base.MvpView;

import java.util.List;

public interface NotificationFragmentMvpView extends MvpView {

    void showWait();

    void removeWait();

    void emptyData();

    void updateNotificationList(List<NotificationDetails> notificationList, Integer totalcount);
}
