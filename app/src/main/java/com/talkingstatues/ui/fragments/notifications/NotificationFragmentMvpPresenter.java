package com.talkingstatues.ui.fragments.notifications;

import com.talkingstatues.ui.base.MvpPresenter;

public interface NotificationFragmentMvpPresenter<V extends NotificationFragmentMvpView>
        extends MvpPresenter<V> {

    void onViewPrepared(int startLimit);
}


