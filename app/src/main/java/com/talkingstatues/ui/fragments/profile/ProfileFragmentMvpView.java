package com.talkingstatues.ui.fragments.profile;



import com.talkingstatues.model.login.LoginDetails;
import com.talkingstatues.ui.base.MvpView;

public interface ProfileFragmentMvpView extends MvpView {

    void setLoginDetails(LoginDetails loginDetails);
}
