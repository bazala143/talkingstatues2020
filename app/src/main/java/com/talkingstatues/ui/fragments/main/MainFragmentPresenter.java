package com.talkingstatues.ui.fragments.main;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.harmis.imagepicker.CameraUtils.CameraIntentHelper;
import com.harmis.imagepicker.CameraUtils.CameraIntentHelperCallback;
import com.harmis.imagepicker.activities.CropImageActivity;
import com.harmis.imagepicker.utils.CommonKeyword;
import com.talkingstatues.R;
import com.talkingstatues.data.prefs.AppPreferencesHelper;
import com.talkingstatues.databinding.ActivityRegisterBinding;
import com.talkingstatues.model.banner.BannerMaster;
import com.talkingstatues.model.placeList.PlaceMaster;
import com.talkingstatues.model.statueList.StatueMaster;
import com.talkingstatues.networking.API_Params;
import com.talkingstatues.networking.NetworkError;
import com.talkingstatues.networking.NetworkService;
import com.talkingstatues.ui.base.BasePresenter;
import com.talkingstatues.utilities.CommonUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;


public class MainFragmentPresenter<V extends MainFragmentMvpView> extends BasePresenter<V>
        implements MainFragmentMvpPresenter<V> {
    @Inject
    NetworkService service;
    private static final String TAG = MainFragmentPresenter.class.getSimpleName();
    CameraIntentHelper mCameraIntentHelper;


    @Inject
    public MainFragmentPresenter(AppPreferencesHelper preferencesHelper, CompositeSubscription mSubscription) {
        super(preferencesHelper, mSubscription);

    }

    @Override
    public void onDetach() {
        if (getSubscription() != null && getSubscription().hasSubscriptions()) {
            getSubscription().clear();
        }
    }

    @Override
    public List<Integer> addBannerItems() {
        List<Integer> bannerItems = new ArrayList<>();
        bannerItems.add(R.drawable.img_banner);
        bannerItems.add(R.drawable.img_banner);
        bannerItems.add(R.drawable.img_banner);
        bannerItems.add(R.drawable.img_banner);
        bannerItems.add(R.drawable.img_banner);
        bannerItems.add(R.drawable.img_banner);
        return bannerItems;

    }

    @Override
    public CameraIntentHelper getmCameraIntentHelper(final int whichImage, final Activity activity) {
        mCameraIntentHelper = new CameraIntentHelper(activity, new CameraIntentHelperCallback() {
            @Override
            public void onPhotoUriFound(int requestCode, Date dateCameraIntentStarted, Uri photoUri, int rotateXDegrees) {
                List<String> imagesList = new ArrayList<>();
                imagesList.add(photoUri.getPath());
                Intent intent = new Intent(new Intent(activity, CropImageActivity.class));
                intent.putExtra(CommonKeyword.RESULT, (Serializable) imagesList);
                activity.startActivityForResult(intent, whichImage);
            }

            @Override
            public void deletePhotoWithUri(Uri photoUri) {

            }

            @Override
            public void onSdCardNotMounted() {

            }

            @Override
            public void onCanceled() {

            }

            @Override
            public void onCouldNotTakePhoto() {

            }

            @Override
            public void onPhotoUriNotFound(int requestCode) {

            }

            @Override
            public void logException(Exception e) {
                CommonUtils.pLog("log_tag", "log Exception : " + e.getMessage());
            }
        });
        return mCameraIntentHelper;
    }

    @Override
    public void openCamera(int whichImage) {
        try {
            if (mCameraIntentHelper != null) {
                mCameraIntentHelper.startCameraIntent(whichImage);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("exception", e.getMessage());
        }

    }

    @Override
    public void getBannerItems() {
        if (getMvpView().isNetworkConnected()) {
            getMvpView().showLoading();
            Subscription s = service.bannerList(new NetworkService.GetBannerListCallback() {
                @Override
                public void onSuccess(BannerMaster master) {
                    Log.e("response", new Gson().toJson(master));
                    getMvpView().hideLoading();
                    if (master.getCode().equals("0")) {
                        getMvpView().onBannerLoaded(master.getJson());
                    } else {
                        getMvpView().showMessage(master.getMessage());
                    }

                }

                @Override
                public void onError(NetworkError networkError) {
                    getMvpView().showMessage(networkError.getLocalizedMessage());
                }
            });
            getSubscription().add(s);
        } else {
            getMvpView().onError(R.string.utils__no_connection);
        }
    }

    @Override
    public void getPlacesApi(String cityId) {
        if (getMvpView().isNetworkConnected()) {
            getMvpView().showLoading();
            Subscription s = service.getPlaceList(placeParams(cityId), new NetworkService.GetNearPlaceListCallbackCallback() {
                @Override
                public void onSuccess(PlaceMaster master) {
                    Log.e("response", new Gson().toJson(master));
                    getMvpView().hideLoading();
                    if (master.getCode().equals("0")) {
                        if (master.getJson() != null && master.getJson().size() > 0) {
                            getMvpView().onPlacesLoaded(master.getJson());
                        } else {
                            getMvpView().onEmptyPlaceList();
                            getMvpView().showMessage(master.getMessage());
                        }
                    } else {
                        getMvpView().onEmptyPlaceList();
                        getMvpView().showMessage(master.getMessage());
                    }

                }

                @Override
                public void onError(NetworkError networkError) {
                    getMvpView().showMessage(networkError.getMessage());
                }
            });
            getSubscription().add(s);
        } else {
            getMvpView().onError(R.string.utils__no_connection);
        }
    }

    @Override
    public String getLanguageCode() {
        return getPreferencesHelper().getLanguageCode();
    }

    private JsonObject placeParams(String cityId) {
        JsonObject gsonObject = new JsonObject();
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(API_Params.city_id, cityId);
            JsonParser parser = new JsonParser();
            gsonObject = (JsonObject) parser.parse(jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return gsonObject;
    }
}
