package com.talkingstatues.ui.fragments.profile;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.talkingstatues.R;
import com.talkingstatues.data.prefs.AppPreferencesHelper;
import com.talkingstatues.model.login.LoginDetails;
import com.talkingstatues.model.login.LoginMaster;
import com.talkingstatues.networking.API_Params;
import com.talkingstatues.networking.NetworkError;
import com.talkingstatues.networking.NetworkService;
import com.talkingstatues.ui.base.BasePresenter;
import com.talkingstatues.utilities.AppConstants;
import com.talkingstatues.utilities.CommonUtils;

import org.json.JSONException;
import org.json.JSONObject;

import javax.inject.Inject;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;


public class ProfileFragmentPresenter<V extends ProfileFragmentMvpView> extends BasePresenter<V>
        implements ProfileFragmentMvpPresenter<V> {
    @Inject
    NetworkService service;
    private static final String TAG = ProfileFragmentPresenter.class.getSimpleName();
    LoginDetails storedLoginDetails;

    @Inject
    public ProfileFragmentPresenter(AppPreferencesHelper preferencesHelper, CompositeSubscription mSubscription) {
        super(preferencesHelper, mSubscription);
        storedLoginDetails = getPreferencesHelper().getLoginModel();

    }



    @Override
    public void userDetailsApi() {

    }

    @Override
    public String getLanguageCode() {
        return getPreferencesHelper().getLanguageCode();
    }

    private JsonObject userDetailsParams() {
        JsonObject gsonObject = new JsonObject();
        try {
            JSONObject jsonObject = new JSONObject();
        //    jsonObject.put(API_Params.userId, getPreferencesHelper().getLoginModel().getCustomerID());
            jsonObject.put(API_Params.deviceToken, getPreferencesHelper().getDeviceToken());
            jsonObject.put(API_Params.device, AppConstants.DEVICE_TYPE);
            JsonParser parser = new JsonParser();
            gsonObject = (JsonObject) parser.parse(jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return gsonObject;
    }

    @Override
    public void onDetach() {
        if (getSubscription() != null && getSubscription().hasSubscriptions()) {
            getSubscription().clear();
        }
    }

    @Override
    public void clearUserSession() {
        getPreferencesHelper().setLoginDetails(null);
        getPreferencesHelper().setIsFirstTimeLaunch(true);
    }

    @Override
    public void setLanguageCode(String language) {
        getPreferencesHelper().setLanguageCode(language);
    }

    @Override
    public LoginDetails getLoginDetails() {
        return getPreferencesHelper().getLoginModel();
    }
}
