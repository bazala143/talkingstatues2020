package com.talkingstatues.ui.fragments.profile;

import com.talkingstatues.model.login.LoginDetails;
import com.talkingstatues.ui.base.MvpPresenter;

public interface ProfileFragmentMvpPresenter<V extends ProfileFragmentMvpView>
        extends MvpPresenter<V> {

    void clearUserSession();

    LoginDetails getLoginDetails();

    void userDetailsApi();
    void  setLanguageCode(String languageCode);

    String getLanguageCode();
}


