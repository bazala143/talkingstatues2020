package com.talkingstatues.ui.fragments.main;

import android.app.Activity;

import com.harmis.imagepicker.CameraUtils.CameraIntentHelper;
import com.talkingstatues.ui.base.MvpPresenter;

import java.util.List;

public interface MainFragmentMvpPresenter<V extends MainFragmentMvpView>
        extends MvpPresenter<V> {
    List<Integer> addBannerItems();
    CameraIntentHelper getmCameraIntentHelper(int whichImage, Activity editProfileActivity);
    void  openCamera(int whichImage);
    void  getBannerItems();
    void  getPlacesApi(String cityId);
    String getLanguageCode();
}


