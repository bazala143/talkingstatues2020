package com.talkingstatues.ui.fragments.main;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.harmis.imagepicker.CameraUtils.CameraIntentHelper;
import com.harmis.imagepicker.model.Images;
import com.harmis.imagepicker.utils.CommonKeyword;
import com.talkingstatues.R;
import com.talkingstatues.databinding.FragmentMainBinding;
import com.talkingstatues.di.component.ActivityComponent;
import com.talkingstatues.model.banner.BannerDetails;
import com.talkingstatues.model.notification.NotificationDetails;
import com.talkingstatues.model.placeList.PlaceDetails;
import com.talkingstatues.networking.API_Params;
import com.talkingstatues.ui.activities.camera.CameraActivity;
import com.talkingstatues.ui.activities.editProfile.EditProfileActivity;
import com.talkingstatues.ui.activities.login.LoginActivity;
import com.talkingstatues.ui.activities.placeDetailsActivity.PlaceDetailsActivity;
import com.talkingstatues.ui.activities.rewardsActivity.RewardsActvity;
import com.talkingstatues.ui.activities.statuesList.StatuesListActvity;
import com.talkingstatues.ui.activities.statuesMap.StatuesMapActivity;
import com.talkingstatues.ui.base.BaseFragment;
import com.talkingstatues.ui.fragments.notifications.NotificationAdapter;
import com.talkingstatues.utilities.AppConstants;
import com.talkingstatues.utilities.CommonUtils;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class MainFragment extends BaseFragment implements
        MainFragmentMvpView, View.OnClickListener {

    private static final String TAG = MainFragment.class.getSimpleName();

    @Inject
    MainFragmentMvpPresenter<MainFragmentMvpView> mPresenter;
    @Inject
    PlaceAdapter placeAdapter;
    FragmentMainBinding binding;
    TextView[] dot;
    CameraIntentHelper mCameraIntentHelper;
    String[] cameraPermissions = {Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
    int whichImage = 1;
    private final int REQUEST_PERMISSION_CAMERA = 1;
    private int SELFIE_CODE = 133;
    String cityId = "";
    float density = 0;
    BannerAdapter pagerAdapter;
    List<BannerDetails> allBannerList = new ArrayList<>();
    boolean isCitySelected = true;
    String languageCode = AppConstants.LANGUAGE_ENGLISH;

    public static MainFragment newInstance(String cityId) {
        Bundle args = new Bundle();
        args.putString(API_Params.city_id, cityId);
        MainFragment fragment = new MainFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        /* dont remove this comment*/

        Display display = getActivity().getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);
        density = getResources().getDisplayMetrics().density;
        Log.e("Density", density + "");
        float dpHeight = outMetrics.heightPixels / density;
        float dpWidth = outMetrics.widthPixels / density;

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_main, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            mPresenter.onAttach(this);
        }
        languageCode = mPresenter.getLanguageCode();
        binding.setClickListener(this);
        Glide.with(getActivity()).load(R.drawable.ic_statue_active).into(binding.ivStatues);
        Glide.with(getActivity()).load(R.drawable.ic_gps).into(binding.ivMap);
        Glide.with(getActivity()).load(R.drawable.ic_selifie).into(binding.ivSelfie);
        Glide.with(getActivity()).load(R.drawable.ic_reward).into(binding.ivReward);
        return binding.getRoot();
    }

    public void changeCity(String cityId) {
        this.cityId = cityId;
        List<BannerDetails> filterList = new ArrayList<>();
        for (BannerDetails bannerDetails : allBannerList) {
            Log.e("cityId", bannerDetails.getCityId() + ":::" + cityId);
            if (bannerDetails.getCityId().equalsIgnoreCase("1")) {
                filterList.add(bannerDetails);

            }
        }
        if(filterList.size() == 0){
            binding.rlNodata.setVisibility(View.VISIBLE);
            binding.rlPager.setVisibility(View.GONE);
        }else{
            binding.rlNodata.setVisibility(View.GONE);
            binding.rlPager.setVisibility(View.VISIBLE);
        }
        if (pagerAdapter != null) {
            pagerAdapter.clearAll();
            pagerAdapter.addAll(filterList);
            if(filterList.size() > 0){
                addDot(0, filterList.size());
            }
            binding.viewPager.setAdapter(pagerAdapter);
        }

        if (cityId != null) {
            mPresenter.getPlacesApi(cityId);
        }
    }
    public void  setCitySelected(boolean isCitySelected){
        this.isCitySelected = isCitySelected;
    }

    public void addDot(int page_position, int size) {
        dot = new TextView[size];
        binding.layoutDot.removeAllViews();
        for (int i = 0; i < dot.length; i++) {
            dot[i] = new TextView(getActivity());
            dot[i].setText(Html.fromHtml("&#9679;"));
            if (density == 2.0) {
                dot[i].setTextSize(10);
            } else {
                dot[i].setTextSize(15);
            }
            dot[i].setPadding(5, 5, 5, 5);
            dot[i].setTextColor(getResources().getColor(R.color.grey));
            binding.layoutDot.addView(dot[i]);
        }
        dot[page_position].setTextColor(getResources().getColor(R.color.white));
    }

    @Override
    protected void setUp(View view) {
        binding.setClickListener(this);
        if (getArguments() != null) {
            cityId = getArguments().getString(API_Params.city_id);
            mPresenter.getPlacesApi(cityId);

        } else {
            Toast.makeText(getActivity(), "Rceived null", Toast.LENGTH_SHORT).show();
        }
        mPresenter.getBannerItems();
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

    @Override
    public void onClick(View v) {
        if (v == binding.linMap) {
            Intent i = new Intent(getActivity(), StatuesMapActivity.class);
            i.putExtra("FROM",AppConstants.DASHBOARD_ACTIVITY);
            i.putExtra(API_Params.city_id, cityId);
            startActivity(i);
        } else if (v == binding.linStatues) {
            Intent i = new Intent(getActivity(), StatuesListActvity.class);
            i.putExtra(API_Params.city_id, cityId);
            startActivity(i);
        } else if (v == binding.linReward) {
            Intent i = new Intent(getActivity(), RewardsActvity.class);
            i.putExtra(API_Params.city_id, cityId);
            startActivity(i);
        } else if (v == binding.linSelfie) {
            Log.e("clcik", "clekce");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (getActivity().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                        && getActivity().checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                    Intent i = new Intent(getActivity(), CameraActivity.class);
                    getActivity().startActivityForResult(i, SELFIE_CODE);
                } else {
                    requestPermissions(cameraPermissions, REQUEST_PERMISSION_CAMERA);
                }
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_PERMISSION_CAMERA) {
            if (grantResults != null && grantResults.length > 0) {
                boolean isGranted = true;
                for (int i = 0; i < grantResults.length; i++) {
                    if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                        isGranted = false;
                        break;
                    }
                }
                if (isGranted) {
                    Intent i = new Intent(getActivity(), CameraActivity.class);
                    getActivity().startActivityForResult(i, SELFIE_CODE);
                }
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == whichImage) {
            mCameraIntentHelper.onActivityResult(requestCode, resultCode, data);
            if (resultCode == CommonKeyword.RESULT_CODE_CROP_IMAGE) {
                //Camera
                if (requestCode == whichImage) {
                    List<Images> imagesList = (List<Images>) data.getSerializableExtra(CommonKeyword.RESULT);
                    if (imagesList != null && imagesList.size() > 0) {
                        Log.e("imagePath", imagesList.get(0).getImageUrl());
                    }
                }
            }
        }

    }

    @Override
    public void onBannerLoaded(final List<BannerDetails> json) {
        allBannerList.clear();
        allBannerList = json;
        final List<BannerDetails> filteredBannerList = new ArrayList<>();
        for (BannerDetails bannerDetails : allBannerList) {
            if (bannerDetails.getCityId().equals("1")) {
                filteredBannerList.add(bannerDetails);
            }
        }
        if(filteredBannerList.size() == 0){
            binding.rlNodata.setVisibility(View.VISIBLE);
            binding.rlPager.setVisibility(View.GONE);
        }else{
            binding.rlNodata.setVisibility(View.GONE);
            binding.rlPager.setVisibility(View.VISIBLE);
        }
        pagerAdapter = new BannerAdapter(getActivity(), filteredBannerList);
        pagerAdapter.setLanguageCode(languageCode);
        pagerAdapter.setCallBack(new BannerAdapter.OnImageClickListner() {
            @Override
            public void OnImaegClick(BannerDetails bannerDetails) {
                Intent i = new Intent(getActivity(), PlaceDetailsActivity.class);
                i.putExtra(AppConstants.BannerDetails, bannerDetails);
                startActivity(i);
            }
        });
        binding.viewPager.setAdapter(pagerAdapter);
        binding.viewPager.setPageMargin(20);
        mCameraIntentHelper = mPresenter.getmCameraIntentHelper(whichImage, getActivity());
        addDot(0, filteredBannerList.size());

        binding.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                addDot(i, filteredBannerList.size());
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }

    @Override
    public void onPlacesLoaded(List<PlaceDetails> json) {
        binding.recPlaces.setVisibility(View.VISIBLE);
        binding.tvNoPlaceFound.setVisibility(View.GONE);
        binding.recPlaces.setAdapter(placeAdapter);
        placeAdapter.clearList();
        placeAdapter.addItems(json);
        placeAdapter.setCallback(new PlaceAdapter.PlaceAdapterOnItemClickListener() {
            @Override
            public void onChatClick(PlaceDetails items) {
                Intent intent = new Intent(getActivity(), PlaceDetailsActivity.class);
                intent.putExtra(AppConstants.Object, items);
                startActivity(intent);
            }

        });

    }

    @Override
    public void onEmptyPlaceList() {
        binding.recPlaces.setVisibility(View.GONE);
        binding.tvNoPlaceFound.setVisibility(View.VISIBLE);
    }
}
