package com.talkingstatues.ui.fragments.profile;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import com.talkingstatues.R;
import com.talkingstatues.databinding.FragmentProfileBinding;
import com.talkingstatues.di.component.ActivityComponent;
import com.talkingstatues.model.login.LoginDetails;

import com.talkingstatues.ui.base.BaseFragment;

import javax.inject.Inject;

public class ProfileFragment extends BaseFragment implements
        ProfileFragmentMvpView, View.OnClickListener {

    private static final String TAG = ProfileFragment.class.getSimpleName();

    @Inject
    ProfileFragmentMvpPresenter<ProfileFragmentMvpView> mPresenter;
    FragmentProfileBinding binding;
    LoginDetails loginDetails;
    private int MY_PERMISSIONS_MKAKE_CALL = 102;

    public static ProfileFragment newInstance() {
        Bundle args = new Bundle();
        ProfileFragment fragment = new ProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            mPresenter.onAttach(this);
        }
        return binding.getRoot();
    }

    @Override
    protected void setUp(View view) {
        binding.setClickListener(this);
        mPresenter.userDetailsApi();
    }

    @Override
    public void onResume() {
        super.onResume();
        setLoginDetails(mPresenter.getLoginDetails());
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

    @Override
    public void onClick(View v) {

    }





    @Override
    public void setLoginDetails(LoginDetails loginDetails) {
        this.loginDetails = loginDetails;
        binding.setLoginDetail(loginDetails);
    }

    private boolean checkPermission() {
        int camera = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE);
        if (camera == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, MY_PERMISSIONS_MKAKE_CALL);
        }

        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_PERMISSIONS_MKAKE_CALL && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            makeCall();
        }
    }


    private void makeCall() {
        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "+91123456789"));
        startActivity(intent);
    }

}
