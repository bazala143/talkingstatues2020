package com.talkingstatues.ui.fragments.main;



import com.talkingstatues.model.banner.BannerDetails;
import com.talkingstatues.model.placeList.PlaceDetails;
import com.talkingstatues.ui.base.MvpView;

import java.util.List;

public interface MainFragmentMvpView extends MvpView {

    void onBannerLoaded(List<BannerDetails> json);

    void onPlacesLoaded(List<PlaceDetails> json);

    void onEmptyPlaceList();
}
