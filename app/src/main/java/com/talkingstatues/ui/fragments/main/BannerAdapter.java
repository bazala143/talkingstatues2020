package com.talkingstatues.ui.fragments.main;


import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.talkingstatues.R;
import com.talkingstatues.databinding.HomeBannerItemsBinding;
import com.talkingstatues.model.banner.BannerDetails;
import com.talkingstatues.networking.APIUrl;
import com.talkingstatues.utilities.AppConstants;

import java.util.List;

public class BannerAdapter extends PagerAdapter {

    FragmentActivity context;
    List<BannerDetails> pager;
    OnImageClickListner onImageClickListner;
    String languageCode = AppConstants.LANGUAGE_ENGLISH;


    public BannerAdapter(FragmentActivity context, List<BannerDetails> pager) {
        this.context = context;
        this.pager = pager;
    }

    public void setCallBack(OnImageClickListner onImageClickListner) {
        this.onImageClickListner = onImageClickListner;

    }

    public void clearAll() {
        pager.clear();
        notifyDataSetChanged();
    }

    public void addAll(List<BannerDetails> bannerDetailsList) {
        pager.addAll(bannerDetailsList);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return pager.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view == o;
    }

    @NonNull
    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        HomeBannerItemsBinding binding = DataBindingUtil.inflate(context.getLayoutInflater(), R.layout.home_banner_items, container, false);
      /*  binding.setUrl(APIUrl.IMAGE_URL);
        binding.setData(pager.get(position));*/
        BannerDetails bannerDetails = pager.get(position);
        if (languageCode.equalsIgnoreCase(AppConstants.LANGUAGE_SPANISH)) {
            if (bannerDetails.getSpanishImageUrl() != null) {
                Glide.with(context).load(APIUrl.IMAGE_URL + bannerDetails.getSpanishImageUrl()).into(binding.ivBanner);
            } else {
                Glide.with(context).load(APIUrl.IMAGE_URL + bannerDetails.getImageUrl()).into(binding.ivBanner);
            }
        } else {
            Glide.with(context).load(APIUrl.IMAGE_URL + bannerDetails.getImageUrl()).into(binding.ivBanner);
        }
        binding.ivBanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onImageClickListner.OnImaegClick(pager.get(position));
            }
        });
        container.addView(binding.getRoot());
        return binding.getRoot();
    }

    @Override
    public void destroyItem(ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getItemPosition(Object object) {
        return super.getItemPosition(object);
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public interface OnImageClickListner {
        void OnImaegClick(BannerDetails bannerDetails);
    }
}
