package com.talkingstatues.ui.fragments.notifications;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.talkingstatues.R;
import com.talkingstatues.databinding.FragmentNotificationBinding;
import com.talkingstatues.di.component.ActivityComponent;
import com.talkingstatues.model.notification.NotificationDetails;
import com.talkingstatues.ui.base.BaseFragment;
import com.talkingstatues.utilities.AppConstants;
import com.talkingstatues.utilities.RecyclerViewPositionHelper;

import java.util.List;

import javax.inject.Inject;

public class NotificationFragment extends BaseFragment implements
        NotificationFragmentMvpView, View.OnClickListener {

    private static final String TAG = NotificationFragment.class.getSimpleName();

    @Inject
    NotificationFragmentMvpPresenter<NotificationFragmentMvpView> mPresenter;
    FragmentNotificationBinding binding;

    @Inject
    NotificationAdapter mAdapter;

    @Inject
    LinearLayoutManager mLayoutManager;

    RecyclerViewPositionHelper mRecyclerViewHelper;
    int startLimit = 0;
    int totalCount = 0;
    boolean isApiLoading = false;

    public static NotificationFragment newInstance() {
        Bundle args = new Bundle();
        NotificationFragment fragment = new NotificationFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_notification, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            mPresenter.onAttach(this);
        }
        return binding.getRoot();
    }

    @Override
    protected void setUp(View view) {
        binding.setClickListener(this);
        binding.toolbar.ivback.setVisibility(View.GONE);
        prepareNotificationAdapter();

        binding.layoutData.setVisibility(View.VISIBLE);
        binding.layoutEmptyData.setVisibility(View.GONE);

        mPresenter.onViewPrepared(startLimit);
    }

    @Override
    public void onClick(View view) {

    }

    private void prepareNotificationAdapter() {
        binding.recyclerView.setItemAnimator(new DefaultItemAnimator());
        binding.recyclerView.setLayoutManager(mLayoutManager);
        binding.recyclerView.setAdapter(mAdapter);
        mAdapter.setCallback(new NotificationAdapter.NotificationClickListener() {
            @Override
            public void onNotificationClick(NotificationDetails notificationDetails) {
                Intent intent = null;
                int nType = notificationDetails.getType();


                if (intent != null) {
                    intent.putExtra(AppConstants.FROM_SCREEN, AppConstants.NOTIFICATION_LIST);
                    startActivity(intent);
                }
            }
        });

        binding.recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {

                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy != 0) {
                    mRecyclerViewHelper = RecyclerViewPositionHelper.createHelper(recyclerView);

                    int visibleItemCount = mLayoutManager.getItemCount();
                    int lastVisible = mRecyclerViewHelper.findLastVisibleItemPosition();
                    boolean endHasBeenReached = lastVisible + 2 >= visibleItemCount;

                    if (!isApiLoading && visibleItemCount < totalCount && endHasBeenReached) {
                        mAdapter.setShowLoader(true);
                        startLimit += 10;
                        mPresenter.onViewPrepared(startLimit);
                    }
                }
            }
        });

        binding.layoutSwipeRefresh.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        startLimit = 0;
                        totalCount = 0;
                        isApiLoading = false;
                        mPresenter.onViewPrepared(startLimit);
                    }
                }
        );
    }

    @Override
    public void emptyData() {
        if (startLimit == 0) {
            binding.layoutData.setVisibility(View.GONE);
            binding.layoutEmptyData.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showWait() {
        isApiLoading = true;
    }

    @Override
    public void removeWait() {
        binding.layoutSwipeRefresh.setRefreshing(false);
        isApiLoading = false;
        mAdapter.setShowLoader(false);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void updateNotificationList(List<NotificationDetails> notificationList, Integer totalCount) {
        binding.layoutData.setVisibility(View.VISIBLE);
        binding.layoutEmptyData.setVisibility(View.GONE);

        if (totalCount != null)
            this.totalCount = totalCount;
        if (startLimit == 0) {
            mAdapter.clearList();
            mAdapter.notifyDataSetChanged();
        }
        mAdapter.addItems(notificationList);
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

}
