package com.talkingstatues.ui.fragments.main;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.talkingstatues.R;
import com.talkingstatues.databinding.LayoutPlacesItemsBinding;
import com.talkingstatues.model.banner.BannerDetails;
import com.talkingstatues.model.placeList.PlaceDetails;
import com.talkingstatues.networking.APIUrl;
import com.talkingstatues.ui.base.BaseViewHolder;

import java.util.ArrayList;
import java.util.List;

public class PlaceAdapter extends RecyclerView.Adapter<BaseViewHolder> {


    private List<PlaceDetails> mList = new ArrayList<>();
    private LayoutInflater layoutInflater;
    PlaceAdapterOnItemClickListener clickListener;


    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        LayoutPlacesItemsBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.layout_places_items, parent, false);
        return new ViewHolder(binding);

    }


    @Override
    public int getItemCount() {
        return mList.size();

    }

    public void addItems(List<PlaceDetails> mList) {
        this.mList.addAll(mList);
        notifyDataSetChanged();
    }

    public void clearList() {
        if (this.mList != null && this.mList.size() > 0) {
            this.mList.clear();
        }
        notifyDataSetChanged();
    }

    public void setCallback(PlaceAdapterOnItemClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public class ViewHolder extends BaseViewHolder {
        LayoutPlacesItemsBinding binding;

        public ViewHolder(View itemView) {
            super(itemView);
        }

        public ViewHolder(final LayoutPlacesItemsBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        @Override
        protected void clear() {

        }

        public void onBind(final int position) {
            super.onBind(position);
            final PlaceDetails bannerDetails = mList.get(position);
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.drawable.ic_place_holder);
            Glide.with(binding.getRoot().getContext())
                    .applyDefaultRequestOptions(requestOptions)
                    .load(APIUrl.IMAGE_URL + bannerDetails.getPlaceImage())
                    .into(binding.ivImage);
            binding.ivImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickListener.onChatClick(bannerDetails);
                }
            });


        }
    }


    public interface PlaceAdapterOnItemClickListener {
        void onChatClick(PlaceDetails items);
    }
}
