package com.talkingstatues.ui.fragments.notifications;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.talkingstatues.R;
import com.talkingstatues.data.prefs.AppPreferencesHelper;
import com.talkingstatues.model.notification.NotificationMaster;
import com.talkingstatues.networking.API_Params;
import com.talkingstatues.networking.NetworkError;
import com.talkingstatues.networking.NetworkService;
import com.talkingstatues.ui.base.BasePresenter;
import com.talkingstatues.utilities.CommonUtils;

import org.json.JSONException;
import org.json.JSONObject;

import javax.inject.Inject;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;


public class NotificationFragmentPresenter<V extends NotificationFragmentMvpView> extends BasePresenter<V>
        implements NotificationFragmentMvpPresenter<V> {
    @Inject
    NetworkService service;
    private static final String TAG = NotificationFragmentPresenter.class.getSimpleName();

    @Inject
    public NotificationFragmentPresenter(AppPreferencesHelper preferencesHelper, CompositeSubscription mSubscription) {
        super(preferencesHelper, mSubscription);

    }

    @Override
    public void onViewPrepared(int startLimit) {
        if (getMvpView().isNetworkConnected()) {
            if (startLimit == 0)
                getMvpView().showLoading();
            getMvpView().showWait();
            Subscription subscription = service.notificationList(getJsonParams(startLimit), new NetworkService.GetNotificationListCallbackCallback() {
                @Override
                public void onSuccess(NotificationMaster master) {
                    getMvpView().hideLoading();
                    getMvpView().removeWait();
                    if (master != null) {
                        CommonUtils.pLog("TAG", "Response : " + new Gson().toJson(master).toString());
                        if (master.getSuccess() == 1) {
                            if (master.getNotificationList() != null && master.getNotificationList().size() > 0) {
                                getMvpView().updateNotificationList(master.getNotificationList(), master.getTotalcount());
                            } else {
                                getMvpView().emptyData();
                            }
                        } else {
                            getMvpView().emptyData();
                        }
                    }
                }

                @Override
                public void onError(NetworkError networkError) {
                    getMvpView().hideLoading();
                    getMvpView().emptyData();
                    getMvpView().onError(networkError.getMessage() + "");
                }
            });
            getSubscription().add(subscription);
        } else {
            getMvpView().onError(R.string.utils__no_connection);
        }
    }

    private JsonObject getJsonParams(int startLimit) {
        JsonObject gsonObject = new JsonObject();
        try {
            JSONObject jsonObject = new JSONObject();
         //   jsonObject.put(API_Params.userId, getPreferencesHelper().getLoginModel().getCustomerID());
            jsonObject.put(API_Params.Start_Limit, startLimit);
            JsonParser parser = new JsonParser();
            gsonObject = (JsonObject) parser.parse(jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return gsonObject;
    }

    @Override
    public void onDetach() {
        if (getSubscription() != null && getSubscription().hasSubscriptions()) {
            getSubscription().clear();
        }
    }
}
