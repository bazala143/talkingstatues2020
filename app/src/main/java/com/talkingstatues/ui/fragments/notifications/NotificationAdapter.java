package com.talkingstatues.ui.fragments.notifications;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.talkingstatues.R;
import com.talkingstatues.databinding.LayoutNotificationItemBinding;
import com.talkingstatues.model.notification.NotificationDetails;
import com.talkingstatues.ui.base.BaseViewHolder;

import java.util.List;

public class NotificationAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    public static final int VIEW_TYPE_PROGRESS = 0;
    public static final int VIEW_TYPE_NORMAL = 1;

    private List<NotificationDetails> mList;
    private LayoutInflater layoutInflater;
    NotificationClickListener clickListener;
    boolean showLoader = false;
    private static final String TAG = NotificationAdapter.class.getSimpleName();

    public NotificationAdapter(List<NotificationDetails> mList) {
        this.mList = mList;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }

        if (viewType == VIEW_TYPE_NORMAL) {
            LayoutNotificationItemBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.layout_notification_item, parent, false);
            return new ViewHolder(binding);
        }

        return  null;

    }
    @Override
    public int getItemViewType(int position) {
        if (mList != null && mList.size() > 0) {
            if ((position == mList.size() - 1) && showLoader) {
                return VIEW_TYPE_PROGRESS;
            }
        }
        return VIEW_TYPE_NORMAL;

    }

    public void setShowLoader(boolean status) {
        showLoader = status;
    }

    @Override
    public int getItemCount() {
        if (mList != null && mList.size() > 0) {
            return mList.size();
        } else {
            return 0;
        }
    }

    public void addItems(List<NotificationDetails> mList) {
        this.mList.addAll(mList);
        notifyDataSetChanged();
    }

    public void clearList() {
        if (this.mList != null && this.mList.size() > 0) {
            this.mList.clear();
        }
    }

    public void setCallback(NotificationClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public class ViewHolder extends BaseViewHolder {
        LayoutNotificationItemBinding binding;

        public ViewHolder(View itemView) {
            super(itemView);
        }

        public ViewHolder(final LayoutNotificationItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        @Override
        protected void clear() {

        }

        public void onBind(final int position) {
            super.onBind(position);
            final NotificationDetails notificationDetails = mList.get(position);
            binding.setData(notificationDetails);
            binding.layoutMain.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickListener.onNotificationClick(notificationDetails);
                }
            });

        }
    }

   /* public class ProgressViewHolder extends BaseViewHolder {
        private LayoutProgressbarBinding binding;

        public ProgressViewHolder(LayoutProgressbarBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        @Override
        protected void clear() {

        }
    }*/

    public interface NotificationClickListener {
        void onNotificationClick(NotificationDetails notificationDetails);
    }
}
