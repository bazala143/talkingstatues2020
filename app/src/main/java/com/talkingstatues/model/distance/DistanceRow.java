package com.talkingstatues.model.distance;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class DistanceRow  implements Serializable {
    @SerializedName("elements")
    @Expose
    private List<DistanceElement> elements = null;

    public List<DistanceElement> getElements() {
        return elements;
    }

    public void setElements(List<DistanceElement> elements) {
        this.elements = elements;
    }
}
