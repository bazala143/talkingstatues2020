package com.talkingstatues.model.distance;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DistanceElement implements Serializable {
    @SerializedName("distance")
    @Expose
    private DistanceDetails distance;
    @SerializedName("duration")
    @Expose
    private DistanceDuration duration;
    @SerializedName("duration_in_traffic")
    @Expose
    private DistanceDurationInTrafic durationInTraffic;
    @SerializedName("status")
    @Expose
    private String status;

    public DistanceDetails getDistance() {
        return distance;
    }

    public void setDistance(DistanceDetails distance) {
        this.distance = distance;
    }

    public DistanceDuration getDuration() {
        return duration;
    }

    public void setDuration(DistanceDuration duration) {
        this.duration = duration;
    }

    public DistanceDurationInTrafic getDurationInTraffic() {
        return durationInTraffic;
    }

    public void setDurationInTraffic(DistanceDurationInTrafic durationInTraffic) {
        this.durationInTraffic = durationInTraffic;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
