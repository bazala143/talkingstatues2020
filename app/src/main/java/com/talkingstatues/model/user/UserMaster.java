package com.talkingstatues.model.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UserMaster {

    @SerializedName("success")
    @Expose
    private Integer success;
    @SerializedName("results")
    @Expose
    private List<UserDetail> userList = null;

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public List<UserDetail> getUserList() {
        return userList;
    }

    public void setUserList(List<UserDetail> userList) {
        this.userList = userList;
    }
}