package com.talkingstatues.model.directionApi;

public class Steps {
    private LocationBean start_location;
    private LocationBean end_location;
    private OverviewPolyLine polyline;

    public LocationBean getStart_location() {
        return start_location;
    }

    public LocationBean getEnd_location() {
        return end_location;
    }

    public OverviewPolyLine getPolyline() {
        return polyline;
    }
}