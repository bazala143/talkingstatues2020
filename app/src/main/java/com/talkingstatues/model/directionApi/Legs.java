package com.talkingstatues.model.directionApi;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Legs {
    private List<Steps> steps;
    @SerializedName("distance")
    @Expose
    private TotalDistance distance;

    public List<Steps> getSteps() {
        return steps;
    }

    public TotalDistance getDistance() {
        return distance;
    }

    public void setDistance(TotalDistance distance) {
        this.distance = distance;
    }
}