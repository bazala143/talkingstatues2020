package com.talkingstatues.model.directionApi;

public class LocationBean {
    private double lat;
    private double lng;

    public double getLat() {
        return lat;
    }

    public double getLng() {
        return lng;
    }
}