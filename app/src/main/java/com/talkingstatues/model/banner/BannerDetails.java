package com.talkingstatues.model.banner;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class BannerDetails implements Serializable {

    @SerializedName("top_logo_id")
    @Expose
    private String topLogoId;
    @SerializedName("image_url")
    @Expose
    private String imageUrl;
    @SerializedName("spanish_image_url")
    @Expose
    private String spanishImageUrl;
    @SerializedName("place_id")
    @Expose
    private String placeId;
    @SerializedName("statue_id")
    @Expose
    private String statueId;
    @SerializedName("template_id")
    @Expose
    private String templateId;
    @SerializedName("top_slidd_url")
    @Expose
    private String topSliddUrl;
    @SerializedName("city_id")
    @Expose
    private String cityId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("template")
    @Expose
    private String template;

    public String getTopLogoId() {
        return topLogoId;
    }

    public void setTopLogoId(String topLogoId) {
        this.topLogoId = topLogoId;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getSpanishImageUrl() {
        return spanishImageUrl;
    }

    public void setSpanishImageUrl(String spanishImageUrl) {
        this.spanishImageUrl = spanishImageUrl;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public String getStatueId() {
        return statueId;
    }

    public void setStatueId(String statueId) {
        this.statueId = statueId;
    }

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public String getTopSliddUrl() {
        return topSliddUrl;
    }

    public void setTopSliddUrl(String topSliddUrl) {
        this.topSliddUrl = topSliddUrl;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

}
