package com.talkingstatues.model.faqList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FaqDetails {

    @SerializedName("faq_id")
    @Expose
    private String faqId;
    @SerializedName("question")
    @Expose
    private String question;
    @SerializedName("spanish_question")
    @Expose
    private String spanishQuestion;
    @SerializedName("answer")
    @Expose
    private String answer;
    @SerializedName("spanish_answer")
    @Expose
    private String spanishAnswer;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public String getFaqId() {
        return faqId;
    }

    public void setFaqId(String faqId) {
        this.faqId = faqId;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getSpanishQuestion() {
        return spanishQuestion;
    }

    public void setSpanishQuestion(String spanishQuestion) {
        this.spanishQuestion = spanishQuestion;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getSpanishAnswer() {
        return spanishAnswer;
    }

    public void setSpanishAnswer(String spanishAnswer) {
        this.spanishAnswer = spanishAnswer;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

}
