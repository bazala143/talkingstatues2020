package com.talkingstatues.model.faqList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.talkingstatues.model.placeList.PlaceDetails;

import java.util.List;

public class FaqMaster {
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("json")
    @Expose
    private List<FaqDetails> json = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<FaqDetails> getJson() {
        return json;
    }

    public void setJson(List<FaqDetails> json) {
        this.json = json;
    }
}
