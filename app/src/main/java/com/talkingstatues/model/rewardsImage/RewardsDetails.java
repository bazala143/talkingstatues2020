package com.talkingstatues.model.rewardsImage;

public class RewardsDetails {
    private int imgId;

    public RewardsDetails(int imgId) {
        this.imgId = imgId;

    }

    public int getImgId() {
        return imgId;
    }

    public void setImgId(int imgId) {
        this.imgId = imgId;
    }


}