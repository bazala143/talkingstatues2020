package com.talkingstatues.model.notification;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NotificationMaster {

    @SerializedName("success")
    @Expose
    private Integer success;
    @SerializedName("totalcount")
    @Expose
    private Integer totalcount;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("result")
    @Expose
    private List<NotificationDetails> notificationList = null;

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public Integer getTotalcount() {
        return totalcount;
    }

    public void setTotalcount(Integer totalcount) {
        this.totalcount = totalcount;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<NotificationDetails> getNotificationList() {
        return notificationList;
    }

    public void setNotificationList(List<NotificationDetails> notificationList) {
        this.notificationList = notificationList;
    }
}