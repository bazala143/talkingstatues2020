package com.talkingstatues.model.selfie;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SelfieDetails {

    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("statue_id")
    @Expose
    private Object statueId;
    @SerializedName("image_url")
    @Expose
    private String imageUrl;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Object getStatueId() {
        return statueId;
    }

    public void setStatueId(Object statueId) {
        this.statueId = statueId;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
