package com.talkingstatues.model.statueList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.talkingstatues.model.distance.DistanceElement;

import java.io.Serializable;

public class StatueDetails implements Serializable {

    @SerializedName("statue_id")
    @Expose
    private String statueId;
    @SerializedName("statue_name")
    @Expose
    private String statueName;
    @SerializedName("spanish_statue_name")
    @Expose
    private String spanishStatueName;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("template_id")
    @Expose
    private String templateId;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("audio_url")
    @Expose
    private String audioUrl;
    @SerializedName("spanish_audio_url")
    @Expose
    private String spanishAudioUrl;
    @SerializedName("range_radius")
    @Expose
    private String rangeRadius;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("web_address")
    @Expose
    private String webAddress;
    @SerializedName("qr_code")
    @Expose
    private String qrCode;
    @SerializedName("spanish_qr_code")
    @Expose
    private String spanishQrCode;
    @SerializedName("qr_code_on")
    @Expose
    private String qrCodeOn;
    @SerializedName("gps_on")
    @Expose
    private String gpsOn;
    @SerializedName("statue_active")
    @Expose
    private String statueActive;
    @SerializedName("keywords")
    @Expose
    private String keywords;
    @SerializedName("spanish_keywords")
    @Expose
    private String spanishKeywords;
    @SerializedName("label")
    @Expose
    private String label;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("city_id")
    @Expose
    private String cityId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("city_latitude")
    @Expose
    private String cityLatitude;
    @SerializedName("city_longitude")
    @Expose
    private String cityLongitude;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("is_visited")
    @Expose
    private Boolean isVisited;

    public String getSpanishStatueName() {
        return spanishStatueName;
    }

    public void setSpanishStatueName(String spanishStatueName) {
        this.spanishStatueName = spanishStatueName;
    }

    public String getSpanishAudioUrl() {
        return spanishAudioUrl;
    }

    public void setSpanishAudioUrl(String spanishAudioUrl) {
        this.spanishAudioUrl = spanishAudioUrl;
    }

    public String getSpanishQrCode() {
        return spanishQrCode;
    }

    public void setSpanishQrCode(String spanishQrCode) {
        this.spanishQrCode = spanishQrCode;
    }

    public String getSpanishKeywords() {
        return spanishKeywords;
    }

    public void setSpanishKeywords(String spanishKeywords) {
        this.spanishKeywords = spanishKeywords;
    }

    @SerializedName("template")
    @Expose
    private StatueTemplate template;
    private DistanceElement distanceElement;
    private  String inZone;
    private  double distance;

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public String getInZone() {
        return inZone;
    }

    public void setInZone(String inZone) {
        this.inZone = inZone;
    }

    public Boolean getVisited() {
        return isVisited;
    }

    public void setVisited(Boolean visited) {
        isVisited = visited;
    }

    public DistanceElement getDistanceElement() {
        return distanceElement;
    }

    public void setDistanceElement(DistanceElement distanceElement) {
        this.distanceElement = distanceElement;
    }

    public String getStatueId() {
        return statueId;
    }

    public void setStatueId(String statueId) {
        this.statueId = statueId;
    }

    public String getStatueName() {
        return statueName;
    }

    public void setStatueName(String statueName) {
        this.statueName = statueName;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getAudioUrl() {
        return audioUrl;
    }

    public void setAudioUrl(String audioUrl) {
        this.audioUrl = audioUrl;
    }

    public String getRangeRadius() {
        return rangeRadius;
    }

    public void setRangeRadius(String rangeRadius) {
        this.rangeRadius = rangeRadius;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getWebAddress() {
        return webAddress;
    }

    public void setWebAddress(String webAddress) {
        this.webAddress = webAddress;
    }

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    public String getQrCodeOn() {
        return qrCodeOn;
    }

    public void setQrCodeOn(String qrCodeOn) {
        this.qrCodeOn = qrCodeOn;
    }

    public String getGpsOn() {
        return gpsOn;
    }

    public void setGpsOn(String gpsOn) {
        this.gpsOn = gpsOn;
    }

    public String getStatueActive() {
        return statueActive;
    }

    public void setStatueActive(String statueActive) {
        this.statueActive = statueActive;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCityLatitude() {
        return cityLatitude;
    }

    public void setCityLatitude(String cityLatitude) {
        this.cityLatitude = cityLatitude;
    }

    public String getCityLongitude() {
        return cityLongitude;
    }

    public void setCityLongitude(String cityLongitude) {
        this.cityLongitude = cityLongitude;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Boolean getIsVisited() {
        return isVisited;
    }

    public void setIsVisited(Boolean isVisited) {
        this.isVisited = isVisited;
    }

    public StatueTemplate getTemplate() {
        return template;
    }

    public void setTemplate(StatueTemplate template) {
        this.template = template;
    }


}
