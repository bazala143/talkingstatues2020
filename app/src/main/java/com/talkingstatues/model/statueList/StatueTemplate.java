package com.talkingstatues.model.statueList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class StatueTemplate implements Serializable {

    @SerializedName("template_id")
    @Expose
    private String templateId;
    @SerializedName("templates_name")
    @Expose
    private String templatesName;
    @SerializedName("spanish_templates_name")
    @Expose
    private String spanishTemplatesName;
    @SerializedName("templates_description")
    @Expose
    private String templatesDescription;
    @SerializedName("spanish_templates_description")
    @Expose
    private String spanishTemplatesDescription;
    @SerializedName("templates_image")
    @Expose
    private String templatesImage;
    @SerializedName("audio_url")
    @Expose
    private String audioUrl;
    @SerializedName("spanish_audio_url")
    @Expose
    private String spanishAudioUrl;
    @SerializedName("video_url")
    @Expose
    private String videoUrl;
    @SerializedName("spanish_video_url")
    @Expose
    private String spanishVideoUrl;
    @SerializedName("video_description")
    @Expose
    private String videoDescription;
    @SerializedName("spanish_video_description")
    @Expose
    private String spanishVideoDescription;
    @SerializedName("templates_image2")
    @Expose
    private String templatesImage2;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public void setSpanishTemplatesName(String spanishTemplatesName) {
        this.spanishTemplatesName = spanishTemplatesName;
    }

    public void setSpanishTemplatesDescription(String spanishTemplatesDescription) {
        this.spanishTemplatesDescription = spanishTemplatesDescription;
    }

    public void setSpanishAudioUrl(String spanishAudioUrl) {
        this.spanishAudioUrl = spanishAudioUrl;
    }

    public void setSpanishVideoUrl(String spanishVideoUrl) {
        this.spanishVideoUrl = spanishVideoUrl;
    }

    public void setSpanishVideoDescription(String spanishVideoDescription) {
        this.spanishVideoDescription = spanishVideoDescription;
    }

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public String getTemplatesName() {
        return templatesName;
    }

    public void setTemplatesName(String templatesName) {
        this.templatesName = templatesName;
    }

    public String getSpanishTemplatesName() {
        return spanishTemplatesName;
    }



    public String getTemplatesDescription() {
        return templatesDescription;
    }

    public void setTemplatesDescription(String templatesDescription) {
        this.templatesDescription = templatesDescription;
    }

    public String getSpanishTemplatesDescription() {
        return spanishTemplatesDescription;
    }



    public String getTemplatesImage() {
        return templatesImage;
    }

    public void setTemplatesImage(String templatesImage) {
        this.templatesImage = templatesImage;
    }

    public String getAudioUrl() {
        return audioUrl;
    }

    public void setAudioUrl(String audioUrl) {
        this.audioUrl = audioUrl;
    }

    public String getSpanishAudioUrl() {
        return spanishAudioUrl;
    }



    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getSpanishVideoUrl() {
        return spanishVideoUrl;
    }



    public String getVideoDescription() {
        return videoDescription;
    }

    public void setVideoDescription(String videoDescription) {
        this.videoDescription = videoDescription;
    }

    public String getSpanishVideoDescription() {
        return spanishVideoDescription;
    }



    public String getTemplatesImage2() {
        return templatesImage2;
    }

    public void setTemplatesImage2(String templatesImage2) {
        this.templatesImage2 = templatesImage2;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }
}
