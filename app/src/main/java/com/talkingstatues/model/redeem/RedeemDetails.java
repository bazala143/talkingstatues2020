package com.talkingstatues.model.redeem;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RedeemDetails {

    @SerializedName("reward_id")
    @Expose
    private String rewardId;
    @SerializedName("reward_name")
    @Expose
    private String rewardName;
    @SerializedName("reward_image")
    @Expose
    private String rewardImage;
    @SerializedName("reward_city")
    @Expose
    private String rewardCity;
    @SerializedName("top_text")
    @Expose
    private String topText;
    @SerializedName("bottom_text")
    @Expose
    private String bottomText;
    @SerializedName("reward_url")
    @Expose
    private String rewardUrl;
    @SerializedName("valid_period")
    @Expose
    private String validPeriod;
    @SerializedName("active_status")
    @Expose
    private String activeStatus;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("serial_number")
    @Expose
    private String serialNumber;

    public String getRewardId() {
        return rewardId;
    }

    public void setRewardId(String rewardId) {
        this.rewardId = rewardId;
    }

    public String getRewardName() {
        return rewardName;
    }

    public void setRewardName(String rewardName) {
        this.rewardName = rewardName;
    }

    public String getRewardImage() {
        return rewardImage;
    }

    public void setRewardImage(String rewardImage) {
        this.rewardImage = rewardImage;
    }

    public String getRewardCity() {
        return rewardCity;
    }

    public void setRewardCity(String rewardCity) {
        this.rewardCity = rewardCity;
    }

    public String getTopText() {
        return topText;
    }

    public void setTopText(String topText) {
        this.topText = topText;
    }

    public String getBottomText() {
        return bottomText;
    }

    public void setBottomText(String bottomText) {
        this.bottomText = bottomText;
    }

    public String getRewardUrl() {
        return rewardUrl;
    }

    public void setRewardUrl(String rewardUrl) {
        this.rewardUrl = rewardUrl;
    }

    public String getValidPeriod() {
        return validPeriod;
    }

    public void setValidPeriod(String validPeriod) {
        this.validPeriod = validPeriod;
    }

    public String getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(String activeStatus) {
        this.activeStatus = activeStatus;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }
}
