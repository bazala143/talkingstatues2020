package com.talkingstatues.model.redeem;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RedeemMaster {
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("json")
    @Expose
    private RedeemDetails json;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public RedeemDetails getJson() {
        return json;
    }

    public void setJson(RedeemDetails json) {
        this.json = json;
    }

}
