package com.talkingstatues.model.logoDetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LogoDetails {

    @SerializedName("sponsor_logo_id")
    @Expose
    private String sponsorLogoId;
    @SerializedName("image_url")
    @Expose
    private String imageUrl;
    @SerializedName("spanish_image_url")
    @Expose
    private Object spanishImageUrl;
    @SerializedName("sponsor_logo_web_url")
    @Expose
    private String sponsorLogoWebUrl;
    @SerializedName("city_id")
    @Expose
    private String cityId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    private  String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSponsorLogoId() {
        return sponsorLogoId;
    }

    public void setSponsorLogoId(String sponsorLogoId) {
        this.sponsorLogoId = sponsorLogoId;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Object getSpanishImageUrl() {
        return spanishImageUrl;
    }

    public void setSpanishImageUrl(Object spanishImageUrl) {
        this.spanishImageUrl = spanishImageUrl;
    }

    public String getSponsorLogoWebUrl() {
        return sponsorLogoWebUrl;
    }

    public void setSponsorLogoWebUrl(String sponsorLogoWebUrl) {
        this.sponsorLogoWebUrl = sponsorLogoWebUrl;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

}
