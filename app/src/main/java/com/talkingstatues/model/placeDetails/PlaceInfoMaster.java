package com.talkingstatues.model.placeDetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.talkingstatues.model.login.LoginDetails;

public class PlaceInfoMaster {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("json")
    @Expose
    private PlaceInfoDetails placeInfoDetails;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public PlaceInfoDetails getPlaceInfoDetails() {
        return placeInfoDetails;
    }

    public void setPlaceInfoDetails(PlaceInfoDetails placeInfoDetails) {
        this.placeInfoDetails = placeInfoDetails;
    }
}
