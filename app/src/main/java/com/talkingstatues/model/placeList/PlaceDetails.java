package com.talkingstatues.model.placeList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PlaceDetails implements Serializable {

    @SerializedName("place_id")
    @Expose
    private String placeId;
    @SerializedName("place_name")
    @Expose
    private String placeName;
    @SerializedName("spanish_place_name")
    @Expose
    private String spanishPlaceName;
    @SerializedName("city_id")
    @Expose
    private String cityId;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("open_time")
    @Expose
    private String openTime;
    @SerializedName("close_time")
    @Expose
    private String closeTime;
    @SerializedName("show_hours")
    @Expose
    private String showHours;
    @SerializedName("opening_weekdays")
    @Expose
    private String openingWeekdays;
    @SerializedName("details")
    @Expose
    private String details;
    @SerializedName("spanish_details")
    @Expose
    private String spanishDetails;
    @SerializedName("place_image")
    @Expose
    private String placeImage;
    @SerializedName("place_web_url")
    @Expose
    private String placeWebUrl;
    @SerializedName("place_address")
    @Expose
    private String placeAddress;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    public String getSpanishPlaceName() {
        return spanishPlaceName;
    }

    public void setSpanishPlaceName(String spanishPlaceName) {
        this.spanishPlaceName = spanishPlaceName;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getOpenTime() {
        return openTime;
    }

    public void setOpenTime(String openTime) {
        this.openTime = openTime;
    }

    public String getCloseTime() {
        return closeTime;
    }

    public void setCloseTime(String closeTime) {
        this.closeTime = closeTime;
    }

    public String getShowHours() {
        return showHours;
    }

    public void setShowHours(String showHours) {
        this.showHours = showHours;
    }

    public String getOpeningWeekdays() {
        return openingWeekdays;
    }

    public void setOpeningWeekdays(String openingWeekdays) {
        this.openingWeekdays = openingWeekdays;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getSpanishDetails() {
        return spanishDetails;
    }

    public void setSpanishDetails(String spanishDetails) {
        this.spanishDetails = spanishDetails;
    }

    public String getPlaceImage() {
        return placeImage;
    }

    public void setPlaceImage(String placeImage) {
        this.placeImage = placeImage;
    }

    public String getPlaceWebUrl() {
        return placeWebUrl;
    }

    public void setPlaceWebUrl(String placeWebUrl) {
        this.placeWebUrl = placeWebUrl;
    }

    public String getPlaceAddress() {
        return placeAddress;
    }

    public void setPlaceAddress(String placeAddress) {
        this.placeAddress = placeAddress;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

}
