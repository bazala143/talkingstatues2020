package com.talkingstatues.model.getReward;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetRewardDetails {

    @SerializedName("reward_id")
    @Expose
    private String rewardId;
    @SerializedName("reward_name")
    @Expose
    private String rewardName;
    @SerializedName("spanish_reward_name")
    @Expose
    private String spanishRewardName;
    @SerializedName("top_text")
    @Expose
    private String topText;
    @SerializedName("spanish_top_text")
    @Expose
    private String spanishTopText;
    @SerializedName("reward_image")
    @Expose
    private String rewardImage;
    @SerializedName("spanish_reward_image")
    @Expose
    private String spanishRewardImage;
    @SerializedName("reward_city")
    @Expose
    private String rewardCity;
    @SerializedName("bottom_text")
    @Expose
    private String bottomText;
    @SerializedName("spanish_bottom_text")
    @Expose
    private String spanishBottomText;
    @SerializedName("reward_url")
    @Expose
    private String rewardUrl;
    @SerializedName("valid_period")
    @Expose
    private String validPeriod;
    @SerializedName("active_status")
    @Expose
    private String activeStatus;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public String getRewardId() {
        return rewardId;
    }

    public void setRewardId(String rewardId) {
        this.rewardId = rewardId;
    }

    public String getRewardName() {
        return rewardName;
    }

    public void setRewardName(String rewardName) {
        this.rewardName = rewardName;
    }

    public String getSpanishRewardName() {
        return spanishRewardName;
    }

    public void setSpanishRewardName(String spanishRewardName) {
        this.spanishRewardName = spanishRewardName;
    }

    public String getTopText() {
        return topText;
    }

    public void setTopText(String topText) {
        this.topText = topText;
    }

    public String getSpanishTopText() {
        return spanishTopText;
    }

    public void setSpanishTopText(String spanishTopText) {
        this.spanishTopText = spanishTopText;
    }

    public String getRewardImage() {
        return rewardImage;
    }

    public void setRewardImage(String rewardImage) {
        this.rewardImage = rewardImage;
    }

    public String getSpanishRewardImage() {
        return spanishRewardImage;
    }

    public void setSpanishRewardImage(String spanishRewardImage) {
        this.spanishRewardImage = spanishRewardImage;
    }

    public String getRewardCity() {
        return rewardCity;
    }

    public void setRewardCity(String rewardCity) {
        this.rewardCity = rewardCity;
    }

    public String getBottomText() {
        return bottomText;
    }

    public void setBottomText(String bottomText) {
        this.bottomText = bottomText;
    }

    public String getSpanishBottomText() {
        return spanishBottomText;
    }

    public void setSpanishBottomText(String spanishBottomText) {
        this.spanishBottomText = spanishBottomText;
    }

    public String getRewardUrl() {
        return rewardUrl;
    }

    public void setRewardUrl(String rewardUrl) {
        this.rewardUrl = rewardUrl;
    }

    public String getValidPeriod() {
        return validPeriod;
    }

    public void setValidPeriod(String validPeriod) {
        this.validPeriod = validPeriod;
    }

    public String getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(String activeStatus) {
        this.activeStatus = activeStatus;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }


}
