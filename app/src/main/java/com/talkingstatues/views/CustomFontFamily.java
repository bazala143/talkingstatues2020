package com.talkingstatues.views;

import android.graphics.Typeface;

import com.talkingstatues.MvpApp;
import com.talkingstatues.utilities.CommonUtils;

import java.util.HashMap;


/**
 * Created by Aprod LLC. on 7/26/2018.
 */
public class CustomFontFamily {
    static CustomFontFamily customFontFamily;
    HashMap<String, String> fontMap = new HashMap<>();
    public static CustomFontFamily getInstance() {
        if (customFontFamily == null)
            customFontFamily = new CustomFontFamily();
        return customFontFamily;
    }
    public void addFont(String alias, String fontName) {
        fontMap.put(alias, fontName);
    }
    public Typeface getFont(String alias) {
        String fontFilename = fontMap.get(alias);
        if (fontFilename == null) {
            CommonUtils.pLog("", "Font not available with name " + alias);
            return null;
        } else {
            Typeface typeface = Typeface.createFromAsset(MvpApp.getContext().getAssets(), fontFilename);
            return typeface;
        }
    }

}
