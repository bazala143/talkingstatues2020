/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.talkingstatues;

import android.app.Application;
import android.content.Context;

import com.crashlytics.android.Crashlytics;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.talkingstatues.R;
import com.talkingstatues.di.component.ApplicationComponent;
import com.talkingstatues.di.component.DaggerApplicationComponent;
import com.talkingstatues.di.module.ApplicationModule;
import com.talkingstatues.views.CustomFontFamily;

import io.fabric.sdk.android.Fabric;


/**
 * Created by Paras Andani on 27/01/17.
 */

public class MvpApp extends Application {
    private static Context context;
    private ApplicationComponent mApplicationComponent;
    CustomFontFamily customFontFamily;

    @Override
    public void onCreate() {
        super.onCreate();
        MvpApp.context = this;
        Fabric.with(this, new Crashlytics());
        mApplicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this)).build();
        mApplicationComponent.inject(this);
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);


        customFontFamily = CustomFontFamily.getInstance();
        customFontFamily.addFont("app_font", getResources().getString(R.string.font));
        customFontFamily.addFont("app_font_bold", getResources().getString(R.string.font_bold));
    }

    public ApplicationComponent getComponent() {
        return mApplicationComponent;
    }


    // Needed to replace the component with a test specific one
    public void setComponent(ApplicationComponent applicationComponent) {
        mApplicationComponent = applicationComponent;
    }

    public static Context getContext() {
        return context;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }
}
