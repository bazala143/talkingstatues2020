package com.talkingstatues.broadcastReceiver;

import android.Manifest;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Places;

import com.talkingstatues.data.prefs.AppPreferencesHelper;

import com.talkingstatues.di.component.DaggerServiceComponent;
import com.talkingstatues.networking.NetworkModule;
import com.talkingstatues.networking.NetworkService;
import com.talkingstatues.utilities.AppConstants;
import java.io.File;

import javax.inject.Inject;



public class AlarmReceiver extends BroadcastReceiver implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    Context context;
    @Inject
    NetworkService service;
    AppPreferencesHelper sharedPref;
    boolean isFirst;
    long currTimeStamp;
    String logString;
    private static final String TAG = AlarmReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.e(TAG, "Background service called");
        this.context = context;
        File cacheFile = new File(context.getCacheDir(), "responses");
        DaggerServiceComponent.builder()
                .networkModule(new NetworkModule(cacheFile))
                .build()
                .injectAlarmReceiver(this);
        sharedPref = new AppPreferencesHelper(context, AppConstants.PREF_NAME);
        Intent alarmIntent = new Intent(context, AlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, alarmIntent, 0);
        AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        manager.cancel(pendingIntent);
        manager.setInexactRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + 8000, 8000, pendingIntent);
        // For our recurring task, we'll just display a message
        //Log.e("sendlocation", "Every 5 second it will appear in Log Console");
        isFirst = true;
        createLocationRequest();
        getLocation();
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10 * 1000);
        mLocationRequest.setFastestInterval(5 * 1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    private void getLocation() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
        } else {
            mGoogleApiClient = new GoogleApiClient.Builder(context)
                    .addApi(LocationServices.API)
                    .addApi(Places.GEO_DATA_API)
                    .addApi(Places.PLACE_DETECTION_API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        requestLocationUpdate();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.e(TAG, "Google api client Suspended");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e(TAG, "Google api client Failed");
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            if (location.hasAccuracy()) {
                Log.e(TAG, "Location Accuracy : " + location.getAccuracy());
            }
            if (location.hasAccuracy() && location.getAccuracy() < AppConstants.ACCURACY_LIMIT && isFirst) {
                Log.e(TAG, "Location is Perfect");
                sendToServer(location.getLatitude(), location.getLongitude());
                isFirst = false;
                if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
                    mGoogleApiClient.disconnect();
                }
            }
        }
    }

    private void requestLocationUpdate() {
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
// Register the listener with the Location Manager to receive location updates
        if (ContextCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            try {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            } catch (Exception e) {
                Log.e(TAG, "Location Update Error : " + e.getMessage());
            }

        }
    }

    private void sendToServer(double latitude, double longitude) {

    }


}