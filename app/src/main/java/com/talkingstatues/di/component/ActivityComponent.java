/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.talkingstatues.di.component;

import com.talkingstatues.di.PerActivity;
import com.talkingstatues.di.module.ActivityModule;
import com.talkingstatues.networking.NetworkModule;
import com.talkingstatues.ui.activities.aboutUs.AboutUsActivity;
import com.talkingstatues.ui.activities.calling.CallingActivity;
import com.talkingstatues.ui.activities.callinnNew.CallingNewActivity;
import com.talkingstatues.ui.activities.camera.CameraActivity;
import com.talkingstatues.ui.activities.editProfile.EditProfileActivity;
import com.talkingstatues.ui.activities.faqActivity.FaqActivity;
import com.talkingstatues.ui.activities.login.LoginActivity;
import com.talkingstatues.ui.activities.placeDetailsActivity.PlaceDetailsActivity;
import com.talkingstatues.ui.activities.redeemReward.RedeemRewardActivity;
import com.talkingstatues.ui.activities.register.RegisterActivity;
import com.talkingstatues.ui.activities.rewardsActivity.RewardsActvity;
import com.talkingstatues.ui.activities.selectLanguage.LanguageActivity;
import com.talkingstatues.ui.activities.splash.SplashActivity;
import com.talkingstatues.ui.activities.statueDetails.StatueDetailsActivity;
import com.talkingstatues.ui.activities.statuesList.StatuesListActvity;
import com.talkingstatues.ui.activities.statuesMap.StatuesMapActivity;
import com.talkingstatues.ui.activities.main.MainActvity;
import com.talkingstatues.ui.activities.webviewActivity.WebViewActivity;
import com.talkingstatues.ui.fragments.main.MainFragment;
import com.talkingstatues.ui.fragments.notifications.NotificationFragment;
import com.talkingstatues.ui.fragments.profile.ProfileFragment;

import dagger.Component;

/**
 * Created by Paras Andani on 27/01/17.
 */

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = {ActivityModule.class, NetworkModule.class,})
public interface ActivityComponent {

    /*Activities*/
    /*------------------------------------------------------*/
    void inject(SplashActivity activity);

    void inject(LoginActivity activity);


    void inject(RegisterActivity activity);

    void inject(MainActvity activity);

    void inject(EditProfileActivity activity);

    void inject(StatuesMapActivity activity);

    void inject(StatuesListActvity activity);

    void inject(RewardsActvity activity);

    void inject(PlaceDetailsActivity activity);

    void inject(StatueDetailsActivity activity);

    void inject(FaqActivity activity);

    void inject(WebViewActivity activity);

    void inject(AboutUsActivity activity);

    void inject(CameraActivity activity);

    void inject(CallingActivity activity);

    void inject(CallingNewActivity activity);
    void inject(RedeemRewardActivity activity);
    void inject(LanguageActivity activity);


    /*Fragments*/
    /*---------------------------------------------------*/
    void inject(MainFragment fragment);

    void inject(NotificationFragment fragment);

    void inject(ProfileFragment fragment);

}
