package com.talkingstatues.di.component;

import com.talkingstatues.broadcastReceiver.AlarmReceiver;
import com.talkingstatues.di.PerActivity;
import com.talkingstatues.networking.NetworkModule;

import dagger.Component;

@PerActivity
@Component(modules = NetworkModule.class)
public interface ServiceComponent {
    void injectAlarmReceiver(AlarmReceiver alarmReceiver);
}
