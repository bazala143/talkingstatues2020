/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.talkingstatues.di.module;

import android.app.Activity;
import android.content.Context;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.talkingstatues.data.prefs.AppPreferencesHelper;
import com.talkingstatues.data.prefs.PreferencesHelper;
import com.talkingstatues.di.ActivityContext;
import com.talkingstatues.di.PerActivity;
import com.talkingstatues.di.PreferenceInfo;
import com.talkingstatues.model.faqList.FaqDetails;
import com.talkingstatues.model.notification.NotificationDetails;
import com.talkingstatues.model.rewardsImage.RewardsDetails;
import com.talkingstatues.model.statueList.StatueDetails;
import com.talkingstatues.ui.activities.aboutUs.AboutUsMvpPresenter;
import com.talkingstatues.ui.activities.aboutUs.AboutUsMvpView;
import com.talkingstatues.ui.activities.aboutUs.AboutUsPresenter;
import com.talkingstatues.ui.activities.calling.CallingMvpPresenter;
import com.talkingstatues.ui.activities.calling.CallingMvpView;
import com.talkingstatues.ui.activities.calling.CallingPresenter;
import com.talkingstatues.ui.activities.callinnNew.CallingNewMvpPresenter;
import com.talkingstatues.ui.activities.callinnNew.CallingNewMvpView;
import com.talkingstatues.ui.activities.callinnNew.CallingNewPresenter;
import com.talkingstatues.ui.activities.camera.CameraMvpPresenter;
import com.talkingstatues.ui.activities.camera.CameraMvpView;
import com.talkingstatues.ui.activities.camera.CameraPresenter;
import com.talkingstatues.ui.activities.editProfile.EditProfileMvpPresenter;
import com.talkingstatues.ui.activities.editProfile.EditProfileMvpView;
import com.talkingstatues.ui.activities.editProfile.EditProfilePresenter;
import com.talkingstatues.ui.activities.faqActivity.FaqAdapter;
import com.talkingstatues.ui.activities.faqActivity.FaqMvpPresenter;
import com.talkingstatues.ui.activities.faqActivity.FaqMvpView;
import com.talkingstatues.ui.activities.faqActivity.FaqPresenter;
import com.talkingstatues.ui.activities.login.LoginMvpPresenter;
import com.talkingstatues.ui.activities.login.LoginMvpView;
import com.talkingstatues.ui.activities.login.LoginPresenter;
import com.talkingstatues.ui.activities.main.LogosAdapter;
import com.talkingstatues.ui.activities.placeDetailsActivity.PlaceDetailsMvpPresenter;
import com.talkingstatues.ui.activities.placeDetailsActivity.PlaceDetailsMvpView;
import com.talkingstatues.ui.activities.placeDetailsActivity.PlaceDetailsPresenter;
import com.talkingstatues.ui.activities.redeemReward.RedeemRewardMvpPresenter;
import com.talkingstatues.ui.activities.redeemReward.RedeemRewardMvpView;
import com.talkingstatues.ui.activities.redeemReward.RedeemRewardPresenter;
import com.talkingstatues.ui.activities.register.RegisterMvpPresenter;
import com.talkingstatues.ui.activities.register.RegisterMvpView;
import com.talkingstatues.ui.activities.register.RegisterPresenter;
import com.talkingstatues.ui.activities.rewardsActivity.RewardsAdapter;
import com.talkingstatues.ui.activities.rewardsActivity.RewardsMvpPresenter;
import com.talkingstatues.ui.activities.rewardsActivity.RewardsMvpView;
import com.talkingstatues.ui.activities.rewardsActivity.RewardsPresenter;
import com.talkingstatues.ui.activities.selectLanguage.LanguageMvpPresenter;
import com.talkingstatues.ui.activities.selectLanguage.LanguageMvpView;
import com.talkingstatues.ui.activities.selectLanguage.LanguagePresenter;
import com.talkingstatues.ui.activities.splash.SplashMvpPresenter;
import com.talkingstatues.ui.activities.splash.SplashMvpView;
import com.talkingstatues.ui.activities.splash.SplashPresenter;
import com.talkingstatues.ui.activities.statueDetails.StatueDetailsMvpPresenter;
import com.talkingstatues.ui.activities.statueDetails.StatueDetailsMvpView;
import com.talkingstatues.ui.activities.statueDetails.StatueDetailsPresenter;
import com.talkingstatues.ui.activities.statuesList.StatuesListAdapter;
import com.talkingstatues.ui.activities.statuesList.StatuesListMvpPresenter;
import com.talkingstatues.ui.activities.statuesList.StatuesListMvpView;
import com.talkingstatues.ui.activities.statuesList.StatuesListPresenter;
import com.talkingstatues.ui.activities.statuesMap.StatuesMapMvpPresenter;
import com.talkingstatues.ui.activities.statuesMap.StatuesMapMvpView;
import com.talkingstatues.ui.activities.statuesMap.StatuesMapPresenter;
import com.talkingstatues.ui.activities.main.DrawerAdapter;
import com.talkingstatues.ui.activities.main.MainMvpPresenter;
import com.talkingstatues.ui.activities.main.MainMvpView;
import com.talkingstatues.ui.activities.main.MainPresenter;
import com.talkingstatues.ui.activities.webviewActivity.WebViewMvpPresenter;
import com.talkingstatues.ui.activities.webviewActivity.WebViewMvpView;
import com.talkingstatues.ui.activities.webviewActivity.WebViewPresenter;
import com.talkingstatues.ui.fragments.main.MainFragmentMvpPresenter;
import com.talkingstatues.ui.fragments.main.MainFragmentMvpView;
import com.talkingstatues.ui.fragments.main.MainFragmentPresenter;
import com.talkingstatues.ui.fragments.main.PlaceAdapter;
import com.talkingstatues.ui.fragments.notifications.NotificationAdapter;
import com.talkingstatues.ui.fragments.notifications.NotificationFragmentMvpPresenter;
import com.talkingstatues.ui.fragments.notifications.NotificationFragmentMvpView;
import com.talkingstatues.ui.fragments.notifications.NotificationFragmentPresenter;
import com.talkingstatues.ui.fragments.profile.ProfileFragmentMvpPresenter;
import com.talkingstatues.ui.fragments.profile.ProfileFragmentMvpView;
import com.talkingstatues.ui.fragments.profile.ProfileFragmentPresenter;
import com.talkingstatues.utilities.AppConstants;

import java.util.ArrayList;

import dagger.Module;
import dagger.Provides;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Paras Andani on 27/01/17.
 */

@Module
public class ActivityModule {

    private AppCompatActivity mActivity;
    private Activity activity;

    public ActivityModule(AppCompatActivity activity) {
        this.mActivity = activity;
    }

    @Provides
    @ActivityContext
    Context provideContext() {
        return mActivity;
    }

    @Provides
    AppCompatActivity provideActivity() {
        return mActivity;
    }

    @Provides
    CompositeSubscription getSubscriptions() {
        return new CompositeSubscription();
    }

    @Provides
    @PreferenceInfo
    String providePreferenceName() {
        return AppConstants.PREF_NAME;
    }

    @Provides
    @PerActivity
    PreferencesHelper providePreferencesHelper(AppPreferencesHelper appPreferencesHelper) {
        return appPreferencesHelper;
    }

    @Provides
    @PerActivity
    SplashMvpPresenter<SplashMvpView> provideSplashPresenter(
            SplashPresenter<SplashMvpView> presenter) {
        return presenter;
    }


    @Provides
    @PerActivity
    StatueDetailsMvpPresenter<StatueDetailsMvpView> statueDetailsMvpPresenter(
            StatueDetailsPresenter<StatueDetailsMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    LoginMvpPresenter<LoginMvpView> provideLoginPresenter(
            LoginPresenter<LoginMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    CallingMvpPresenter<CallingMvpView> provideCallingPresenter(
            CallingPresenter<CallingMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    CallingNewMvpPresenter<CallingNewMvpView> provideCallingNewPresenter(
            CallingNewPresenter<CallingNewMvpView> presenter) {
        return presenter;
    }


    @Provides
    @PerActivity
    RegisterMvpPresenter<RegisterMvpView> registerMvpPresenter(
            RegisterPresenter<RegisterMvpView> presenter) {
        return presenter;
    }


    @Provides
    @PerActivity
    MainMvpPresenter<MainMvpView> userHomeMvpPresenter(
            MainPresenter<MainMvpView> presenter) {
        return presenter;
    }


    @Provides
    MainFragmentMvpPresenter<MainFragmentMvpView> mainFragmentMvpPresenter(
            MainFragmentPresenter<MainFragmentMvpView> presenter) {
        return presenter;
    }


    @Provides
    NotificationFragmentMvpPresenter<NotificationFragmentMvpView> notificationFragmentMvpPresenter(
            NotificationFragmentPresenter<NotificationFragmentMvpView> presenter) {
        return presenter;
    }

    @Provides
    ProfileFragmentMvpPresenter<ProfileFragmentMvpView> profileFragmentMvpPresenter(
            ProfileFragmentPresenter<ProfileFragmentMvpView> presenter) {
        return presenter;
    }

    @Provides
    EditProfileMvpPresenter<EditProfileMvpView> editProfileMvpPresenter(
            EditProfilePresenter<EditProfileMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    StatuesMapMvpPresenter<StatuesMapMvpView> statuesMapMvpPresenter(
            StatuesMapPresenter< StatuesMapMvpView> presenter) {
        return presenter;
    }

    @Provides
    LinearLayoutManager provideLinearLayoutManager(AppCompatActivity activity) {
        return new LinearLayoutManager(activity);
    }

    @Provides
    GridLayoutManager provideGridLayoutManager(AppCompatActivity activity) {
        return new GridLayoutManager(activity, 2);
    }

    @Provides
    NotificationAdapter provideNotificationAdapter() {
        return new NotificationAdapter(new ArrayList<NotificationDetails>());
    }

    @Provides
    PlaceAdapter placeAdapter() {
        return new PlaceAdapter();
    }
    @Provides
    DrawerAdapter drawerAdapter() {
        return new DrawerAdapter();
    }

    @Provides
    LogosAdapter logosAdapter() {
        return new LogosAdapter();
    }

    @Provides
    @PerActivity
    StatuesListMvpPresenter<StatuesListMvpView> statuesListMvpPresenter(
            StatuesListPresenter<StatuesListMvpView> presenter) {
        return presenter;
    }

    @Provides
    StatuesListAdapter statuesListAdapter() {
        return new StatuesListAdapter(activity,new ArrayList<StatueDetails>());
    }

    @Provides
    @PerActivity
    RewardsMvpPresenter<RewardsMvpView> rewardsMvpPresenter(
            RewardsPresenter<RewardsMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    PlaceDetailsMvpPresenter<PlaceDetailsMvpView> placeDetailsPresenter(
            PlaceDetailsPresenter<PlaceDetailsMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    CameraMvpPresenter<CameraMvpView> cameraMvpPresenter(
            CameraPresenter<CameraMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    FaqMvpPresenter<FaqMvpView> faqListMvpPresenter(
            FaqPresenter<FaqMvpView> presenter) {
        return presenter;
    }

    @Provides
    FaqAdapter faqAdapter() {
        return new FaqAdapter(activity,new ArrayList<FaqDetails>());
    }


    @Provides
    @PerActivity
    WebViewMvpPresenter<WebViewMvpView> webViewMvpPresenter(
            WebViewPresenter<WebViewMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    AboutUsMvpPresenter<AboutUsMvpView> aboutUsPresenter(
            AboutUsPresenter<AboutUsMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    RedeemRewardMvpPresenter<RedeemRewardMvpView> redeemRewardPresenter(
            RedeemRewardPresenter<RedeemRewardMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    LanguageMvpPresenter<LanguageMvpView> languagePresenter(
            LanguagePresenter<LanguageMvpView> presenter) {
        return presenter;
    }

}
