package com.talkingstatues.utilities;

import android.widget.ImageView;

import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

public class AdapterBinding {

    @BindingAdapter("android:src")
    public static void loadImage(ImageView view, String imageUrl) {
        Glide.with(view.getContext())
                .load(imageUrl)
                .into(view);
    }

    @BindingAdapter("android:drawable")
    public static void loadDrwable(ImageView view, int drawableId) {
        Glide.with(view.getContext())
                .load(drawableId)
                .into(view);
    }

    @BindingAdapter("adapter")
    public static void setAdapter(RecyclerView view, RecyclerView.Adapter recyclerAdapter) {
        view.setAdapter(recyclerAdapter);
    }
}
