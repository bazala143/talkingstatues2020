package com.talkingstatues.utilities;

public class RequestCodes {
    public static final int REQUEST_SPLASH_ACTIVITY = 301;
    public static final int REQUEST_MAIN_ACTIVITY = 302;
    public static final int REQUEST_LOGIN_ACTIVITY = 303;
    public static final int REQUEST_DAILY_TOUR_ACTIVITY = 304;
    public static final int REQUEST_VERIFY_OTP_ACTIVITY = 305;
    public static final int REQUEST_CHOOSE_MAP_LOCATION_ACTIVITY = 306;
    public static final int REQUEST_MAINTENANCE_REQUEST_ACTIVITY = 307;
    public static final int REQUEST_SERVICE_LIST_ACTIVITY = 308;
    public static final int REQUEST_REQUEST_ACTIVITY = 309;
    public static final int REQUEST_LOCATION_TO_LOCATION_ACTIVITY = 310;
    public static final int REQUEST_FIND_SHOP_ACTIVITY = 311;
}
