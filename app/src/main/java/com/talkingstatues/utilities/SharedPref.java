package com.talkingstatues.utilities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

public class SharedPref {

    public Activity mActivity = null;
    private static final String COMPARESESSION_PREFENCE_NAME = "a2z_app";
    private static final String DEVICE_TOKEN = "device_token";
    public static final String LOGIN_DETAILS = "login_details";
    public static final String IS_SOUND = "is_sound";

    private SharedPreferences sharedPref;
    private SharedPreferences.Editor editor;

    private static SharedPref sharedPreference = null;

    @SuppressLint("CommitPrefEdits")
    private SharedPref(Context activity) {
        sharedPref = activity.getSharedPreferences(
                COMPARESESSION_PREFENCE_NAME, 0);
        editor = sharedPref.edit();

    }

    public static SharedPref getInstance(Context context){
        if(sharedPreference == null)
        {
            sharedPreference = new SharedPref(context);
        }
        return sharedPreference;
    }

    @SuppressLint("CommitPrefEdits")
    public SharedPref(Activity mfragmentactivity) {
        sharedPref = mfragmentactivity.getSharedPreferences(
                COMPARESESSION_PREFENCE_NAME, 0);
        editor = sharedPref.edit();

    }

    public void deleteAllSharedPrefs() {
        editor.clear();
        editor.commit();
    }

    // Device token
    public String getDeviceToken() {
        return sharedPref.getString(DEVICE_TOKEN, null);
    }

    public void storeDeviceToken(String menu_content) {
        try {
            editor.putString(DEVICE_TOKEN, menu_content);
            editor.commit();
            editor.apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void resetDeviceToken() {
        try {
            editor.putString(DEVICE_TOKEN, null);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getLoginDetails() {
        return sharedPref.getString(LOGIN_DETAILS, null);
    }

    public void storeLoginDetails(String login) {
        try {
            editor.putString(LOGIN_DETAILS, login);
            editor.commit();
            editor.apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void resetLoginDetails() {
        try {
            editor.putString(LOGIN_DETAILS, null);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*public LoginDetails getLoginDetailsModel() {
        LoginDetails loginDetails;
        String response = getLoginDetails();
        if (!CommonUtils.isEmpty( response )) {
            final Gson gson = new Gson();
            Type collectionType = new TypeToken<LoginDetails>() {
            }.getType();
            loginDetails = gson.fromJson( response, collectionType );
            return loginDetails;
        }
        return null;
    }*/

    public boolean getIsPlaySound() {
        return sharedPref.getBoolean(IS_SOUND, false);
    }

    public void storeIsPlaySound(boolean param) {
        try {
            editor.putBoolean(IS_SOUND, param);
            editor.commit();
            editor.apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
