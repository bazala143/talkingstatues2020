package com.talkingstatues.utilities;

public class AppConstants {
    /*Stings*/
    public static final String PREF_NAME = "yakfeek_prefs";
    public static final String DEVICE_TYPE = "1";
    public  static  final  int MARKER_TYPE_BLACK = 1;
    public  static  final  int MARKER_TYPE_RED = 2;
    public  static  final  int MARKER_TYPE_GREEN= 3;
    public static final int ACCURACY_LIMIT = 1500;

    /*Languages*/
    public static final String LANGUAGE_SPANISH = "es";
    public static final String LANGUAGE_ENGLISH = "en";
    public static final String FROM_SCREEN = "FROM_SCREEN";
    public static final String STATUS_OK = "OK";
    public static final String PUSH_NOTIFICATION = "PUSH_NOTIFICATION";
    public static final String NOTIFICATION_LIST = "NOTIFICATION_LIST";
    public static String Object = "Object";
    public static String statueList = "statueList";
    public static String BannerDetails = "BannerDetails";
    public static String index ="index";
    public static String STATUE_LIST_ACTIVITY ="STATUE_LIST_ACTIVITY";
    public static String DASHBOARD_ACTIVITY ="DASHBOARD_ACTIVITY";
}
