package com.talkingstatues.networking;

import com.google.gson.JsonObject;
import com.talkingstatues.model.aboutUs.AboutUsMaster;
import com.talkingstatues.model.banner.BannerMaster;
import com.talkingstatues.model.city.CityMaster;
import com.talkingstatues.model.directionApi.DirectionResults;
import com.talkingstatues.model.faqList.FaqMaster;
import com.talkingstatues.model.getReward.GetRewardMaster;
import com.talkingstatues.model.login.LoginMaster;
import com.talkingstatues.model.logoDetails.LogoMaster;
import com.talkingstatues.model.notification.NotificationMaster;
import com.talkingstatues.model.placeDetails.PlaceInfoMaster;
import com.talkingstatues.model.placeList.PlaceMaster;
import com.talkingstatues.model.redeem.RedeemMaster;
import com.talkingstatues.model.rewardsImage.RewardMaster;
import com.talkingstatues.model.selfie.SelfieMaster;
import com.talkingstatues.model.statueDetail.StatueDetailMaster;
import com.talkingstatues.model.statueList.StatueMaster;
import com.talkingstatues.model.visitStatue.VisitStatueMaster;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;
import rx.Observable;

public interface WebServices {

    @POST(APIUrl.registration)
    Observable<LoginMaster> registerApi(@Body JsonObject jsonBody);

    @POST(APIUrl.login)
    Observable<LoginMaster> loginApi(@Body JsonObject jsonBody);


    @POST(APIUrl.updateProfile)
    Observable<LoginMaster> updateProfileApi(@Body JsonObject jsonBody);

    @POST(APIUrl.forgotPassword)
    Observable<LoginMaster> forgotPasswordApi(@Body JsonObject jsonBody);

    @POST(APIUrl.notificationList)
    Observable<NotificationMaster> notificationListApi(@Body JsonObject jsonBody);

    @POST(APIUrl.statueList)
    Observable<StatueMaster> getStatueList(@Body JsonObject jsonBody);

    @POST(APIUrl.rewardList)
    Observable<RewardMaster> getRewardList(@Body JsonObject jsonBody);

    @POST(APIUrl.GET_REWARDS)
    Observable<GetRewardMaster> getRewardDetails(@Body JsonObject jsonBody);

    @POST(APIUrl.placeList)
    Observable<PlaceMaster> getPlaceList(@Body JsonObject jsonBody);

    @POST(APIUrl.GET_FAQS)
    Observable<FaqMaster> getfaqList();

    @POST(APIUrl.bannerList)
    Observable<BannerMaster> getbannerList();

    @POST(APIUrl.GET_CITIES)
    Observable<CityMaster> getCityList(@Body JsonObject jsonBody);

    @POST(APIUrl.GET_ABOUT_US)
    Observable<AboutUsMaster> getAbousUs();

    @POST(APIUrl.GET_PLACE_INFO)
    Observable<PlaceInfoMaster> getPlaceInfo(@Body JsonObject jsonBody);

    @POST(APIUrl.VISIT_STATUE)
    Observable<VisitStatueMaster> visitStatue(@Body JsonObject jsonBody);

    @POST(APIUrl.REDEEM_REWARD)
    Observable<RedeemMaster> redeemReward(@Body JsonObject jsonBody);

    @POST(APIUrl.GET_SPONSER_LOGOS)
    Observable<LogoMaster> getSponserLogos(@Body JsonObject jsonBody);

    @POST(APIUrl.LOGIN_WITH_FACEBOOK)
    Observable<LoginMaster> loginWithFacebook(@Body JsonObject jsonBody);

    @Multipart
    @POST(APIUrl.SET_SELFIE)
    Observable<SelfieMaster> setSelfi(@Part(API_Params.user_id) int userId, @Part MultipartBody.Part image);

    @GET("/maps/api/directions/json")
    Call<DirectionResults> getPolyLineJson(@Query("origin") String origin, @Query("destination") String destination, @Query("key") String key);

    @POST(APIUrl.GET_STATUE_DETAILS)
    Observable<StatueDetailMaster> getStatueDetails(@Body JsonObject jsonBody);


}
