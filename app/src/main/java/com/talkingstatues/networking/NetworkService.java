package com.talkingstatues.networking;

import android.util.Log;

import com.google.gson.JsonObject;
import com.talkingstatues.MvpApp;
import com.talkingstatues.data.prefs.AppPreferencesHelper;

import com.talkingstatues.model.aboutUs.AboutUsMaster;
import com.talkingstatues.model.banner.BannerMaster;
import com.talkingstatues.model.city.CityMaster;
import com.talkingstatues.model.faqList.FaqMaster;
import com.talkingstatues.model.getReward.GetRewardMaster;
import com.talkingstatues.model.login.LoginMaster;
import com.talkingstatues.model.logoDetails.LogoMaster;
import com.talkingstatues.model.notification.NotificationMaster;

import com.talkingstatues.model.placeDetails.PlaceInfoMaster;
import com.talkingstatues.model.placeList.PlaceMaster;
import com.talkingstatues.model.redeem.RedeemMaster;
import com.talkingstatues.model.rewardsImage.RewardMaster;
import com.talkingstatues.model.selfie.SelfieMaster;
import com.talkingstatues.model.statueDetail.StatueDetailMaster;
import com.talkingstatues.model.statueList.StatueMaster;
import com.talkingstatues.model.visitStatue.VisitStatueMaster;
import com.talkingstatues.utilities.AppConstants;
import com.talkingstatues.utilities.CommonUtils;

import okhttp3.MultipartBody;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by ennur on 6/25/16.
 */
public class NetworkService {
    private final WebServices webServices;
    private static final String TAG = NetworkService.class.getSimpleName();


    public NetworkService(WebServices webServices) {
        this.webServices = webServices;
    }

    /*----------------------------------------Login-----------------------------------------------*/
    public Subscription getClientLogin(final JsonObject jsonObject, final GetLoginCallback callback) {
        jsonObject.addProperty(API_Params.language_code,CommonUtils.LanguageCode);

        Log.e("Login Params",jsonObject.toString());

        return webServices.loginApi(jsonObject)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends LoginMaster>>() {
                    @Override
                    public Observable<? extends LoginMaster> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<LoginMaster>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onError(new NetworkError(e));
                        CommonUtils.pLog(TAG, "Network service Error : " + e.getMessage().toString());
                    }

                    @Override
                    public void onNext(LoginMaster loginMaster) {
                        callback.onSuccess(loginMaster);

                    }
                });
    }

    public interface GetLoginCallback {
        void onSuccess(LoginMaster loginMaster);

        void onError(NetworkError networkError);
    }

    /*  login with facebook*/

    public Subscription loginWithfacebook(final JsonObject jsonObject, final GetLoginCallback callback) {
        jsonObject.addProperty(API_Params.language_code,CommonUtils.LanguageCode);

        return webServices.loginWithFacebook(jsonObject)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends LoginMaster>>() {
                    @Override
                    public Observable<? extends LoginMaster> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<LoginMaster>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onError(new NetworkError(e));
                        CommonUtils.pLog(TAG, "Network service Error : " + e.getMessage().toString());
                    }

                    @Override
                    public void onNext(LoginMaster loginMaster) {
                        callback.onSuccess(loginMaster);

                    }
                });
    }

    /*----------------------------------------Register-----------------------------------------------*/
    public Subscription register(final JsonObject jsonObject, final GetLoginCallback callback) {
        jsonObject.addProperty(API_Params.language_code,CommonUtils.LanguageCode);


        return webServices.registerApi(jsonObject)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends LoginMaster>>() {
                    @Override
                    public Observable<? extends LoginMaster> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<LoginMaster>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onError(new NetworkError(e));
                        CommonUtils.pLog(TAG, "Network service Error : " + e.getMessage().toString());
                    }

                    @Override
                    public void onNext(LoginMaster loginMaster) {
                        callback.onSuccess(loginMaster);

                    }
                });
    }


    /*----------------------------------Forgot Password--------------------------------------*/
    public Subscription forgotPasswordApi(final JsonObject jsonObject, final GetForgotPasswordCallback callback) {
        jsonObject.addProperty(API_Params.language_code,CommonUtils.LanguageCode);


        return webServices.forgotPasswordApi(jsonObject)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends LoginMaster>>() {
                    @Override
                    public Observable<? extends LoginMaster> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<LoginMaster>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onError(new NetworkError(e));

                    }

                    @Override
                    public void onNext(LoginMaster master) {
                        callback.onSuccess(master);

                    }
                });
    }

    public interface GetForgotPasswordCallback {
        void onSuccess(LoginMaster master);

        void onError(NetworkError networkError);
    }

    /*----------------------------------Notification List-------------------------------------*/
    public Subscription notificationList(final JsonObject jsonObject, final GetNotificationListCallbackCallback callback) {
        jsonObject.addProperty(API_Params.language_code,CommonUtils.LanguageCode);


        return webServices.notificationListApi(jsonObject)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends NotificationMaster>>() {
                    @Override
                    public Observable<? extends NotificationMaster> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<NotificationMaster>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onError(new NetworkError(e));

                    }

                    @Override
                    public void onNext(NotificationMaster master) {
                        callback.onSuccess(master);

                    }
                });
    }

    public interface GetNotificationListCallbackCallback {
        void onSuccess(NotificationMaster master);

        void onError(NetworkError networkError);
    }

    /*----------------------------------Statue List-------------------------------------*/
    public Subscription statueList(final JsonObject jsonObject, final GetStatueListCallbackCallback callback) {
        jsonObject.addProperty(API_Params.language_code,CommonUtils.LanguageCode);

        return webServices.getStatueList(jsonObject)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends StatueMaster>>() {
                    @Override
                    public Observable<? extends StatueMaster> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<StatueMaster>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onError(new NetworkError(e));
                        CommonUtils.pLog(TAG, "Network service Error : " + e.getMessage().toString());


                    }

                    @Override
                    public void onNext(StatueMaster master) {
                        callback.onSuccess(master);

                    }
                });
    }

    public interface GetStatueListCallbackCallback {
        void onSuccess(StatueMaster master);

        void onError(NetworkError networkError);
    }

    /*---------------------------------RewardList-------------------------------------*/
    public Subscription rewardList(final JsonObject jsonObject, final GetRewardListCallbackCallback callback) {
        jsonObject.addProperty(API_Params.language_code,CommonUtils.LanguageCode);

        return webServices.getRewardList(jsonObject)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends RewardMaster>>() {
                    @Override
                    public Observable<? extends RewardMaster> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<RewardMaster>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onError(new NetworkError(e));

                    }

                    @Override
                    public void onNext(RewardMaster master) {
                        callback.onSuccess(master);

                    }
                });
    }

    public interface GetRewardListCallbackCallback {
        void onSuccess(RewardMaster master);

        void onError(NetworkError networkError);
    }

    /*---------------------------------Near place List-------------------------------------*/
    public Subscription getPlaceList(final JsonObject jsonObject, final GetNearPlaceListCallbackCallback callback) {
        jsonObject.addProperty(API_Params.language_code,CommonUtils.LanguageCode);

        return webServices.getPlaceList(jsonObject)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends PlaceMaster>>() {
                    @Override
                    public Observable<? extends PlaceMaster> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<PlaceMaster>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onError(new NetworkError(e));

                    }

                    @Override
                    public void onNext(PlaceMaster master) {
                        callback.onSuccess(master);

                    }
                });
    }

    public interface GetNearPlaceListCallbackCallback {
        void onSuccess(PlaceMaster master);

        void onError(NetworkError networkError);


    }

    /*--------------------------------Faq List-------------------------------------*/
    public Subscription faqList(final GetFaqListCallback callback) {

        return webServices.getfaqList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends FaqMaster>>() {
                    @Override
                    public Observable<? extends FaqMaster> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<FaqMaster>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onError(new NetworkError(e));

                    }

                    @Override
                    public void onNext(FaqMaster master) {
                        callback.onSuccess(master);

                    }
                });
    }

    public interface GetFaqListCallback {
        void onSuccess(FaqMaster master);

        void onError(NetworkError networkError);
    }

    /*------------------------------- Banner List-------------------------------------*/
    public Subscription bannerList(final GetBannerListCallback callback) {
        return webServices.getbannerList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends BannerMaster>>() {
                    @Override
                    public Observable<? extends BannerMaster> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<BannerMaster>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onError(new NetworkError(e));
                    }

                    @Override
                    public void onNext(BannerMaster master) {
                        callback.onSuccess(master);

                    }
                });
    }

    public interface GetBannerListCallback {
        void onSuccess(BannerMaster master);

        void onError(NetworkError networkError);
    }
    /* get city list*/

    public Subscription getCityList(final GetCityListCallback callback) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty(API_Params.language_code,CommonUtils.LanguageCode);
        return webServices.getCityList(jsonObject)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends CityMaster>>() {
                    @Override
                    public Observable<? extends CityMaster> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<CityMaster>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onError(new NetworkError(e));

                    }

                    @Override
                    public void onNext(CityMaster loginMaster) {
                        callback.onSuccess(loginMaster);

                    }
                });
    }

    public interface GetCityListCallback {
        void onSuccess(CityMaster cityMaster);

        void onError(NetworkError networkError);
    }

    /* update ptofile */

    public Subscription updateProfile(final JsonObject jsonObject, final GetLoginCallback callback) {
        jsonObject.addProperty(API_Params.language_code,CommonUtils.LanguageCode);

        return webServices.updateProfileApi(jsonObject)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends LoginMaster>>() {
                    @Override
                    public Observable<? extends LoginMaster> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<LoginMaster>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onError(new NetworkError(e));
                        CommonUtils.pLog(TAG, "Network service Error : " + e.getMessage().toString());
                    }

                    @Override
                    public void onNext(LoginMaster loginMaster) {
                        callback.onSuccess(loginMaster);

                    }
                });
    }

    /* get about us */

    public Subscription getAbousUs(final GetAboutUsCallback callback) {
        return webServices.getAbousUs()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends AboutUsMaster>>() {
                    @Override
                    public Observable<? extends AboutUsMaster> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<AboutUsMaster>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onError(new NetworkError(e));
                        CommonUtils.pLog(TAG, "Network service Error : " + e.getMessage().toString());
                    }

                    @Override
                    public void onNext(AboutUsMaster loginMaster) {
                        callback.onSuccess(loginMaster);

                    }
                });
    }

    public interface GetAboutUsCallback {
        void onSuccess(AboutUsMaster cityMaster);

        void onError(NetworkError networkError);
    }

    /* get place info */

    public Subscription getPlaceInfo(final JsonObject jsonObject, final GetPlaceInfoCallback callback) {
        jsonObject.addProperty(API_Params.language_code,CommonUtils.LanguageCode);
        return webServices.getPlaceInfo(jsonObject)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends PlaceInfoMaster>>() {
                    @Override
                    public Observable<? extends PlaceInfoMaster> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<PlaceInfoMaster>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onError(new NetworkError(e));
                        CommonUtils.pLog(TAG, "Network service Error : " + e.getMessage().toString());
                    }

                    @Override
                    public void onNext(PlaceInfoMaster loginMaster) {
                        callback.onSuccess(loginMaster);

                    }
                });
    }

    public interface GetPlaceInfoCallback {
        void onSuccess(PlaceInfoMaster cityMaster);

        void onError(NetworkError networkError);
    }

    /* visit statue */

    public Subscription getVisitStatue(final JsonObject jsonObject, final GetVisitStatueCallback callback) {
        jsonObject.addProperty(API_Params.language_code,CommonUtils.LanguageCode);

        return webServices.visitStatue(jsonObject)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends VisitStatueMaster>>() {
                    @Override
                    public Observable<? extends VisitStatueMaster> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<VisitStatueMaster>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onError(new NetworkError(e));
                        CommonUtils.pLog(TAG, "Network service Error : " + e.getMessage().toString());
                    }

                    @Override
                    public void onNext(VisitStatueMaster loginMaster) {
                        callback.onSuccess(loginMaster);

                    }
                });
    }

    public interface GetVisitStatueCallback {
        void onSuccess(VisitStatueMaster cityMaster);

        void onError(NetworkError networkError);
    }

    /* set selfie*/

    /*----------------------------------------Update Profile-----------------------------------------------*/
    public Subscription setSelfieApi(final int userId, MultipartBody.Part body, final GetSetSelfieCallback callback) {


        return webServices.setSelfi(userId, body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends SelfieMaster>>() {
                    @Override
                    public Observable<? extends SelfieMaster> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<SelfieMaster>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onError(new NetworkError(e));

                    }

                    @Override
                    public void onNext(SelfieMaster loginMaster) {
                        callback.onSuccess(loginMaster);

                    }
                });
    }

    public interface GetSetSelfieCallback {
        void onSuccess(SelfieMaster loginMaster);

        void onError(NetworkError networkError);
    }

    /* redeem reward */

    public Subscription redeemReward(final JsonObject jsonObject, final GetRedeemRewardCallback callback) {
        jsonObject.addProperty(API_Params.language_code,CommonUtils.LanguageCode);

        return webServices.redeemReward(jsonObject)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends RedeemMaster>>() {
                    @Override
                    public Observable<? extends RedeemMaster> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<RedeemMaster>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onError(new NetworkError(e));
                        CommonUtils.pLog(TAG, "Network service Error : " + e.getMessage().toString());
                    }

                    @Override
                    public void onNext(RedeemMaster loginMaster) {
                        callback.onSuccess(loginMaster);

                    }
                });
    }

    public interface GetRedeemRewardCallback {
        void onSuccess(RedeemMaster cityMaster);

        void onError(NetworkError networkError);
    }

    /* get sponser logos */

    public Subscription getSponserLogos(final JsonObject jsonObject, final GetSponserLogosCallback callback) {
        return webServices.getSponserLogos(jsonObject)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends LogoMaster>>() {
                    @Override
                    public Observable<? extends LogoMaster> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<LogoMaster>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onError(new NetworkError(e));
                        CommonUtils.pLog(TAG, "Network service Error : " + e.getMessage().toString());
                    }

                    @Override
                    public void onNext(LogoMaster loginMaster) {
                        callback.onSuccess(loginMaster);

                    }
                });
    }

    public interface GetSponserLogosCallback {
        void onSuccess(LogoMaster cityMaster);

        void onError(NetworkError networkError);
    }

    /* get statue details */

    public Subscription getStatueDetails(final JsonObject jsonObject, final GetStatueDetailsCallback callback) {
        jsonObject.addProperty(API_Params.language_code,CommonUtils.LanguageCode);
        return webServices.getStatueDetails(jsonObject)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends StatueDetailMaster>>() {
                    @Override
                    public Observable<? extends StatueDetailMaster> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<StatueDetailMaster>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onError(new NetworkError(e));
                        CommonUtils.pLog(TAG, "Network service Error : " + e.getMessage().toString());
                    }

                    @Override
                    public void onNext(StatueDetailMaster loginMaster) {
                        callback.onSuccess(loginMaster);

                    }
                });
    }

    public interface GetStatueDetailsCallback {
        void onSuccess(StatueDetailMaster cityMaster);

        void onError(NetworkError networkError);
    }

    /* redeem reward */

    public Subscription getRewardDetails(final JsonObject jsonObject, final GetRewardDetailsCallback callback) {
        jsonObject.addProperty(API_Params.language_code,CommonUtils.LanguageCode);

        return webServices.getRewardDetails(jsonObject)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends GetRewardMaster>>() {
                    @Override
                    public Observable<? extends GetRewardMaster> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<GetRewardMaster>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onError(new NetworkError(e));
                        CommonUtils.pLog(TAG, "Network service Error : " + e.getMessage().toString());
                    }

                    @Override
                    public void onNext(GetRewardMaster loginMaster) {
                        callback.onSuccess(loginMaster);

                    }
                });
    }

    public interface GetRewardDetailsCallback {
        void onSuccess(GetRewardMaster cityMaster);
        void onError(NetworkError networkError);
    }

}