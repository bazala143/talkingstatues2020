package com.talkingstatues.networking;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.talkingstatues.utilities.CommonUtils;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okio.Buffer;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;


/**
 * Created by admin on 8/31/2016.
 */
public class ApiHandler {

    private static WebServices apiService;


    public static WebServices getApiService() {

        if (apiService == null) {

            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .addInterceptor(new Interceptor() {
                        @Override
                        public okhttp3.Response intercept(Chain chain) throws IOException {

                            Request original = chain.request();

                            // Customize the request
                            Request request = original.newBuilder()
                                    .header("Content-Type", "application/json")
                                    .header("Connection", "close")
                                    .removeHeader("Pragma")
                                    /*.header("Cache-Control", String.format("max-age=%d",
                                            31536000))*/
                                    .build();

                            CommonUtils.pLog("URL : ", request.url().toString() + "");
                            try {
                                final Buffer buffer = new Buffer();
                                request.body().writeTo(buffer);
                                CommonUtils.pLog("Parameter : ", buffer.readUtf8() + "");
                            } catch (final Exception e) {
                                CommonUtils.pLog("Parameter : ", "did not work");
                            }

                            okhttp3.Response response = chain.proceed(request);
                            response.cacheResponse();
                            CommonUtils.pLog("Http Response : ", response.toString());
                            // Customize or return the response
                            return response;
                        }
                    })
                    .build();

            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(APIUrl.BASE_URL)
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())

                    .build();

            apiService = retrofit.create(WebServices.class);
            return apiService;
        } else {
            return apiService;
        }
    }


}
