package com.talkingstatues.networking;

public class APIUrl {
    /*Local*/
    // public static final String BASE_URL = "http://192.168.1.25/yakfeek_new/index.php/api/";

    /*Live*/
    public static final String BASE_URL = "http://cms-ts.com/api/user/";
    public static final String IMAGE_URL = "http://cms-ts.com/public/images/";
    public static final String AUDIO_URL = "http://cms-ts.com/public/audio/";
    public static final String GET_CITIES = "get-cities";
    public static final String registration = "sign-up";
    public static final String login = "sign-in";
    public static final String updateProfile = "update-profile";
    public static final String forgotPassword = "forget-password";
    public static final String notificationList = "notificationList";
    public static final String statueList = "get-statues";
    public static final String rewardList = "rewardList";
    public static final String GET_FAQS = "get-faqs";
    public static final String GET_PLACE_INFO = "get-places-info";
    public static final String VISIT_STATUE = "visit-statue";
    public static final String REDEEM_REWARD = "redeem-award";
    public static final String bannerList = "get-all-slides";
    public static final String placeList = "get-places";
    public static final String GET_ABOUT_US = "get-aboutus";
    public static final String SET_SELFIE = "set-selfie";
    public static final String GET_STATUE_DETAILS = "get-statue-details";
    public static final String GET_SPONSER_LOGOS = "get-sponsor-logos";
    public static final String GET_REWARDS = "get-rewards";
    public static final String LOGIN_WITH_FACEBOOK = "sign-in-facebook";
    public static String dummyUrl = "http://harmistechnology.com/contactus";
    public static final String userDetails = "customerDetail";
    public static final String webSiteUrl = "http://www.talkingstatues.com/";
    public static String contactUsUrl = "http://www.talkingstatues.com/contact.html";
    public static final String googleApiKey = "AIzaSyBPG4-84HogJOKU2ajbab2a2p4fTTDlqSg";
    public static final String needHelp = "http://www.newyorktalkingstatues.com/how-does-it-work.html";
}
